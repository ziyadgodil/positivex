﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRApp.MultiLang {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppRes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppRes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("HRApp.MultiLang.AppRes", typeof(AppRes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Action Plan.
        /// </summary>
        public static string ActionPlan {
            get {
                return ResourceManager.GetString("ActionPlan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activities.
        /// </summary>
        public static string Activities {
            get {
                return ResourceManager.GetString("Activities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity.
        /// </summary>
        public static string Activity {
            get {
                return ResourceManager.GetString("Activity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Strength.
        /// </summary>
        public static string AddStrength {
            get {
                return ResourceManager.GetString("AddStrength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        public static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Analyze.
        /// </summary>
        public static string Analyze {
            get {
                return ResourceManager.GetString("Analyze", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Balcony Basement Strengths-Handout2.
        /// </summary>
        public static string BalconyBasementStrengthsHandout2 {
            get {
                return ResourceManager.GetString("BalconyBasementStrengthsHandout2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Best of me:.
        /// </summary>
        public static string Bestofme {
            get {
                return ResourceManager.GetString("Bestofme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Password.
        /// </summary>
        public static string ChangePassword {
            get {
                return ResourceManager.GetString("ChangePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Coach Uploads.
        /// </summary>
        public static string CoachUploads {
            get {
                return ResourceManager.GetString("CoachUploads", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Definitions.
        /// </summary>
        public static string Definitions {
            get {
                return ResourceManager.GetString("Definitions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Distribution Grid.
        /// </summary>
        public static string DistributionGrid {
            get {
                return ResourceManager.GetString("DistributionGrid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Domain.
        /// </summary>
        public static string Domain {
            get {
                return ResourceManager.GetString("Domain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        public static string Done {
            get {
                return ResourceManager.GetString("Done", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Download PDF : 1.
        /// </summary>
        public static string DownloadPDF1 {
            get {
                return ResourceManager.GetString("DownloadPDF1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Download PDF : 2.
        /// </summary>
        public static string DownloadPDF2 {
            get {
                return ResourceManager.GetString("DownloadPDF2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Download PDF : 3.
        /// </summary>
        public static string DownloadPDF3 {
            get {
                return ResourceManager.GetString("DownloadPDF3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E.
        /// </summary>
        public static string E {
            get {
                return ResourceManager.GetString("E", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Coach.
        /// </summary>
        public static string EmailCoach {
            get {
                return ResourceManager.GetString("EmailCoach", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Executing.
        /// </summary>
        public static string Executing {
            get {
                return ResourceManager.GetString("Executing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In Network.
        /// </summary>
        public static string HomeInNetworkLabel {
            get {
                return ResourceManager.GetString("HomeInNetworkLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Out Network.
        /// </summary>
        public static string HomeOutNetworkLabel {
            get {
                return ResourceManager.GetString("HomeOutNetworkLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I.
        /// </summary>
        public static string I {
            get {
                return ResourceManager.GetString("I", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Influencing.
        /// </summary>
        public static string Influencing {
            get {
                return ResourceManager.GetString("Influencing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Know your strengths.
        /// </summary>
        public static string Knowyourstrengths {
            get {
                return ResourceManager.GetString("Knowyourstrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Live your strengths.
        /// </summary>
        public static string Liveyourstrengths {
            get {
                return ResourceManager.GetString("Liveyourstrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logout.
        /// </summary>
        public static string Logout {
            get {
                return ResourceManager.GetString("Logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Love your life.
        /// </summary>
        public static string Loveyourlife {
            get {
                return ResourceManager.GetString("Loveyourlife", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage/View Network.
        /// </summary>
        public static string ManageViewNetwork {
            get {
                return ResourceManager.GetString("ManageViewNetwork", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Strength Domains.
        /// </summary>
        public static string MyStrengthDomains {
            get {
                return ResourceManager.GetString("MyStrengthDomains", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Strengths.
        /// </summary>
        public static string MyStrengths {
            get {
                return ResourceManager.GetString("MyStrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Strengths Days.
        /// </summary>
        public static string MyStrengthsDays {
            get {
                return ResourceManager.GetString("MyStrengthsDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 360 Network.
        /// </summary>
        public static string n360Network {
            get {
                return ResourceManager.GetString("n360Network", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to name-it-aim-it-claim-it.
        /// </summary>
        public static string nameitaimitclaimit {
            get {
                return ResourceManager.GetString("nameitaimitclaimit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notifications.
        /// </summary>
        public static string Notifications {
            get {
                return ResourceManager.GetString("Notifications", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please add AIM to leverage your strengths.
        /// </summary>
        public static string PleaseaddAIMtoleverageyourstrengths {
            get {
                return ResourceManager.GetString("PleaseaddAIMtoleverageyourstrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to + Add.
        /// </summary>
        public static string plusadd {
            get {
                return ResourceManager.GetString("plusadd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to + Best of me:.
        /// </summary>
        public static string plusBestofme {
            get {
                return ResourceManager.GetString("plusBestofme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to + Filter:.
        /// </summary>
        public static string plusFilter {
            get {
                return ResourceManager.GetString("plusFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to + What I need from you:.
        /// </summary>
        public static string plusWhatIneedfromyou {
            get {
                return ResourceManager.GetString("plusWhatIneedfromyou", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to + What you can count on me to do consistently:.
        /// </summary>
        public static string plusWhatyoucancountonmetodoconsistently {
            get {
                return ResourceManager.GetString("plusWhatyoucancountonmetodoconsistently", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to + Worst of me:.
        /// </summary>
        public static string plusWorstofme {
            get {
                return ResourceManager.GetString("plusWorstofme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to POWER OF TWO Handout_4 Questions.
        /// </summary>
        public static string POWEROFTWOHandout_4Questions {
            get {
                return ResourceManager.GetString("POWEROFTWOHandout_4Questions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Privacy Policy.
        /// </summary>
        public static string PrivacyPolicy {
            get {
                return ResourceManager.GetString("PrivacyPolicy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        public static string Profile {
            get {
                return ResourceManager.GetString("Profile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Explore 360.
        /// </summary>
        public static string Profile360 {
            get {
                return ResourceManager.GetString("Profile360", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to R.
        /// </summary>
        public static string R {
            get {
                return ResourceManager.GetString("R", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating.
        /// </summary>
        public static string Rating {
            get {
                return ResourceManager.GetString("Rating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refresh.
        /// </summary>
        public static string Refresh {
            get {
                return ResourceManager.GetString("Refresh", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Relationship Building.
        /// </summary>
        public static string RelationshipBuilding {
            get {
                return ResourceManager.GetString("RelationshipBuilding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset.
        /// </summary>
        public static string Reset {
            get {
                return ResourceManager.GetString("Reset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to S.
        /// </summary>
        public static string S {
            get {
                return ResourceManager.GetString("S", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Strengths.
        /// </summary>
        public static string SaveStrengths {
            get {
                return ResourceManager.GetString("SaveStrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search out of network.
        /// </summary>
        public static string Searchoutofnetwork {
            get {
                return ResourceManager.GetString("Searchoutofnetwork", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Connection.
        /// </summary>
        public static string SelectConnection {
            get {
                return ResourceManager.GetString("SelectConnection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected Colleague(s) Domains of Strengths.
        /// </summary>
        public static string SelectedColleaguesDomainsofStrengths {
            get {
                return ResourceManager.GetString("SelectedColleaguesDomainsofStrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected Colleague(s) Strengths.
        /// </summary>
        public static string SelectedColleaguesStrengths {
            get {
                return ResourceManager.GetString("SelectedColleaguesStrengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send Invitation.
        /// </summary>
        public static string SendInvitation {
            get {
                return ResourceManager.GetString("SendInvitation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Share with manager.
        /// </summary>
        public static string Sharewithmanager {
            get {
                return ResourceManager.GetString("Sharewithmanager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shout.
        /// </summary>
        public static string Shout {
            get {
                return ResourceManager.GetString("Shout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shout Out.
        /// </summary>
        public static string ShoutOut {
            get {
                return ResourceManager.GetString("ShoutOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shout outs Given.
        /// </summary>
        public static string Shoutoutsgiven {
            get {
                return ResourceManager.GetString("Shoutoutsgiven", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shout outs Received.
        /// </summary>
        public static string ShoutoutsReceived {
            get {
                return ResourceManager.GetString("ShoutoutsReceived", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shout Received.
        /// </summary>
        public static string ShoutReceived {
            get {
                return ResourceManager.GetString("ShoutReceived", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shouts:.
        /// </summary>
        public static string Shouts_ {
            get {
                return ResourceManager.GetString("Shouts:", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is a detail page. To get the &apos;triple&apos; line icon on each platform add a icon to each platform and update the &apos;Master&apos; page with an Icon that references it..
        /// </summary>
        public static string SliderDetail {
            get {
                return ResourceManager.GetString("SliderDetail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strategic Thinking.
        /// </summary>
        public static string StrategiesThinking {
            get {
                return ResourceManager.GetString("StrategiesThinking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strength NAME.
        /// </summary>
        public static string StrengthNAME {
            get {
                return ResourceManager.GetString("StrengthNAME", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strengths:.
        /// </summary>
        public static string Strengths {
            get {
                return ResourceManager.GetString("Strengths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strengths:.
        /// </summary>
        public static string Strengths_ {
            get {
                return ResourceManager.GetString("Strengths:", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strengths to AIM.
        /// </summary>
        public static string StrengthstoAIM {
            get {
                return ResourceManager.GetString("StrengthstoAIM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Talent.
        /// </summary>
        public static string Talent {
            get {
                return ResourceManager.GetString("Talent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Term &amp; Condition.
        /// </summary>
        public static string TandC {
            get {
                return ResourceManager.GetString("TandC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Their Domains of Leadership.
        /// </summary>
        public static string TheirDomainsofLeadership {
            get {
                return ResourceManager.GetString("TheirDomainsofLeadership", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Their Shout Outs Received.
        /// </summary>
        public static string TheirShoutOutsReceived {
            get {
                return ResourceManager.GetString("TheirShoutOutsReceived", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        public static string Update {
            get {
                return ResourceManager.GetString("Update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upload your report.
        /// </summary>
        public static string UploadYourReport {
            get {
                return ResourceManager.GetString("UploadYourReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username.
        /// </summary>
        public static string Username {
            get {
                return ResourceManager.GetString("Username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to What I need from you:.
        /// </summary>
        public static string WhatIneedfromyou {
            get {
                return ResourceManager.GetString("WhatIneedfromyou", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to What you can count on me to do consistently:.
        /// </summary>
        public static string Whatyoucancountonmetodoconsistently {
            get {
                return ResourceManager.GetString("Whatyoucancountonmetodoconsistently", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Worst of me:.
        /// </summary>
        public static string Worstofme {
            get {
                return ResourceManager.GetString("Worstofme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You&apos;ve Got This!.
        /// </summary>
        public static string You_veGotThis {
            get {
                return ResourceManager.GetString("You_veGotThis", resourceCulture);
            }
        }
    }
}
