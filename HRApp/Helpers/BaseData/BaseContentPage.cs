﻿using System;
using HRApp.Helpers.Resources;
using Naxam.Controls.Forms;
using Xamarin.Forms;

namespace HRApp.Helpers.BaseData
{
    //public class PopupBasePage<T> : MainPopupBasePage where T : BaseViewModel, new()
    //{
    //    protected T _viewModel;

    //    public T ViewModel
    //    {
    //        get
    //        {
    //            return _viewModel ?? (_viewModel = new T());
    //        }
    //    }

    //    ~PopupBasePage()
    //    {
    //        _viewModel = null;
    //    }

    //    public PopupBasePage()
    //    {
    //        BindingContext = ViewModel;
    //    }

    //    protected override void OnAppearing()
    //    {
    //        ViewModel.OnAppearing();
    //        base.OnAppearing();
    //    }

    //    protected override void OnDisappearing()
    //    {
    //        ViewModel.OnDisappearing();
    //        base.OnDisappearing();
    //    }

    //}

    public enum PopupShowDirection
    {
        TopToBottom,
        BottomToTop,
        RightToLeft,
        LeftToRight
    }

    //public class MainPopupBasePage : ContentPage
    //{
    //    public PopupShowDirection ShowDirection { get; set; } = PopupShowDirection.RightToLeft;
    //    public bool IsFullScreen { get; set; } = true;

    //    public Color BarTextColor
    //    {
    //        get;
    //        set;
    //    }

    //    public Color BarBackgroundColor
    //    {
    //        get;
    //        set;
    //    }

    //    public MainPopupBasePage()
    //    {
    //        BarBackgroundColor = Color.White;
    //        BarTextColor = Colors.NavigationBarBackgroundColor;
    //        BackgroundColor = Color.Transparent;

    //        if (Parent is NavigationPage)
    //        {
    //            var nav = (NavigationPage)Parent;
    //            nav.BarBackgroundColor = BarBackgroundColor;
    //            nav.BarTextColor = BarTextColor;
    //        }
    //    }

    //    public bool HasInitialized
    //    {
    //        get;
    //        private set;
    //    }

    //    protected virtual void OnLoaded() { }

    //    protected virtual void Initialize() { }


    //    protected override void OnAppearing()
    //    {
    //        if (!HasInitialized)
    //        {
    //            HasInitialized = true;
    //            OnLoaded();
    //        }
    //        base.OnAppearing();
    //    }

    //    protected override void OnDisappearing()
    //    {
    //        base.OnDisappearing();
    //    }

    //}

    public class BaseContentPage<T> : MainBaseContentPage where T : BaseViewModel, new()
    {
        protected T _viewModel;

        public T ViewModel
        {
            get
            {
                return _viewModel ?? (_viewModel = new T());
            }
        }



        ~BaseContentPage()
        {
            _viewModel = null;
        }

        public BaseContentPage()
        {
            BindingContext = ViewModel;
        }

        protected override void OnAppearing()
        {
            ViewModel.OnAppearing();
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            ViewModel.OnDisappearing();
            base.OnDisappearing();
        }
    }

    public class MainBaseContentPage : ContentPage
    {
        public Color BarTextColor
        {
            get;
            set;
        }

        public Color BarBackgroundColor
        {
            get;
            set;
        }

        public MainBaseContentPage()
        {
            BarBackgroundColor = Color.White;
            BarTextColor = Colors.NavigationBarBackgroundColor;
            BackgroundColor = Color.White;

            if (Parent is NavigationPage)
            {
                var nav = (NavigationPage)Parent;
                nav.BarBackgroundColor = BarBackgroundColor;
                nav.BarTextColor = BarTextColor;
            }
        }

        public bool HasInitialized
        {
            get;
            private set;
        }

        protected virtual void OnLoaded()
        {
        }

        protected virtual void Initialize()
        {
        }


        protected override void OnAppearing()
        {
            if (!HasInitialized)
            {
                HasInitialized = true;
                OnLoaded();
            }
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

    }

    public class BaseTabbedPage<T> : TopTabbedPage where T : BaseViewModel, new()
    {
        protected T _viewModel;

        public T ViewModel
        {
            get
            {
                return _viewModel ?? (_viewModel = new T());
            }
        }



        ~BaseTabbedPage()
        {
            _viewModel = null;
        }

        public BaseTabbedPage()
        {
            BindingContext = ViewModel;
        }

        protected override void OnAppearing()
        {
            ViewModel.OnAppearing();
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            ViewModel.OnDisappearing();
            base.OnDisappearing();
        }
    }
}
