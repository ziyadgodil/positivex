﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace HRApp.Helpers.BaseData
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public static string UserToken
        {
            get => AppSettings.GetValueOrDefault(nameof(UserToken), string.Empty);

            set => AppSettings.AddOrUpdateValue(nameof(UserToken), value);
        }

        public static string UserName
        {
            get => AppSettings.GetValueOrDefault(nameof(UserName), string.Empty);

            set => AppSettings.AddOrUpdateValue(nameof(UserName), value);
        }

        public static string UserPassword
        {
            get => AppSettings.GetValueOrDefault(nameof(UserPassword), string.Empty);

            set => AppSettings.AddOrUpdateValue(nameof(UserPassword), value);
        }
    }
}
