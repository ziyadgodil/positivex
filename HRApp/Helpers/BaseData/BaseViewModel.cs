﻿using System;
using Acr.UserDialogs;

namespace HRApp.Helpers.BaseData
{
    public class BaseViewModel : BaseNotify
    {
        bool _isBusy;
        public event EventHandler IsBusyChanged;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                if (SetPropertyChanged(ref _isBusy, value))
                {
                    if (IsBusyChanged != null)
                        IsBusyChanged(this, new EventArgs());
                }
            }
        }

        public virtual void OnAppearing() { }

        public virtual void OnDisappearing() { }

        public class Busy : IDisposable
        {
            readonly object _sync = new object();
            readonly BaseViewModel _viewModel;
            readonly bool _showProgressView;
            static IUserDialogs Dialogs;
            public Busy(BaseViewModel viewModel, bool showProgressView, string displayMessage = null)
            {
                try
                {
                    _viewModel = viewModel;
                    lock (_sync)
                    {
                        _viewModel.IsBusy = true;
                        _showProgressView = showProgressView;
                        if (_showProgressView)
                        {
                            if (string.IsNullOrEmpty(displayMessage))
                            {
                                displayMessage = "Loading..";
                            }
                            Dialogs = UserDialogs.Instance;
                            Dialogs.Loading(displayMessage);
                            //App.Instance.Hud.DisplayProgress(displayMessage);
                        }

                    }
                }
                catch (Exception ex)
                {
                    //Analytics.TrackEvent("1 BaseViewModel Busy() ");
                    //ex.Track();
                }
            }

            public void Dispose()
            {
                try
                {
                    lock (_sync)
                    {
                        _viewModel.IsBusy = false;
                        if (_showProgressView)
                        {
                            Dialogs.HideLoading();
                            //App.Instance.Hud.Dismiss();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Analytics.TrackEvent("1 BaseViewModel Dispose()  ");
                    //ex.Track();
                }
            }
        }
    }
}
