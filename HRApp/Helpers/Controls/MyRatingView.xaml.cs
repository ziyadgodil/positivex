﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HRApp.Helpers.Controls
{
    public partial class MyRatingView : ContentView
    {
        public MyRatingView()
        {
            InitializeComponent();
        }


        public static readonly BindableProperty RatingProperty = BindableProperty.Create(
                                                         propertyName: "Rating",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(MyRatingView),
                                                         defaultValue: string.Empty,
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: RatingPropertyChanged);

        public string Rating
        {
            get { return base.GetValue(RatingProperty).ToString(); }
            set { base.SetValue(RatingProperty, value); }
        }

        private static void RatingPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (MyRatingView)bindable;


            //reset
            control.FirstBtn.BackgroundColor = Color.White;
            control.SecondBtn.BackgroundColor = Color.White;
            control.ThiredBtn.BackgroundColor = Color.White;
            control.FourthBtn.BackgroundColor = Color.White;
            control.FifthBtn.BackgroundColor = Color.White;


            if (newValue == null || string.IsNullOrEmpty(newValue.ToString()))
                return;

            int Rating = Convert.ToInt32(newValue.ToString());
            if (Rating == 5)
            {
                control.FifthBtn.BackgroundColor = Color.Gray;
            }
            if (Rating >= 4)
            {
                control.FourthBtn.BackgroundColor = Color.Gray;
            }
            if (Rating >= 3)
            {
                control.ThiredBtn.BackgroundColor = Color.Gray;
            }
            if (Rating >= 2)
            {
                control.SecondBtn.BackgroundColor = Color.Gray;
            }
            if (Rating >= 1)
            {
                control.FirstBtn.BackgroundColor = Color.Gray;
            }

            //control.title.Text = newValue.ToString();
        }

        void SelectRating(object sender, EventArgs args)
        {
            var btn = (sender as Button);
            Rating = btn.CommandParameter as string;
        }
    }
}
