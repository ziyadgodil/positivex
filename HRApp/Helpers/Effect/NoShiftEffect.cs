﻿using System;
using Xamarin.Forms;
namespace HRApp.Helpers.Effect
{
    public class NoShiftEffect : RoutingEffect
    {
        public NoShiftEffect() : base("Finn.NoShiftEffect")
        {
        }
    }
}
