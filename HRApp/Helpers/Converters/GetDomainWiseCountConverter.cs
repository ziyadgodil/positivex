﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using HRApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Helpers.Converters
{
    public class GetDomainWiseCountConverter : IValueConverter, IMarkupExtension
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var listStrengths = (ObservableCollection<StrengthModel>)value;
                var DomainId = parameter as string;

                return listStrengths.Count(x => x.DomainID.ToString() == DomainId);

                //if (DomainId == "1")
                //    return listStrengths.Count(x=>x.DomainID.ToString() == DomainId);
                //else if (DomainId == "2")
                //    return Colors.InfluencingDomain;
                //else if (DomainId == "3")
                //    return Colors.RelationshipDomain;
                //else if (DomainId == "4")
                //    return Colors.StrategicDomain;
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
