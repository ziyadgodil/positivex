﻿using System;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;

namespace RamseyCrookall.Helpers.Converters
{
    public class HRApp : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var currencyCodeLabel = parameter as Label;
            string currencyCode = "";
            if (currencyCodeLabel != null)
            {
                currencyCode = currencyCodeLabel.Text;
            }

            if (value != null)
            {
                decimal dec;
                if (value is string)
                    decimal.TryParse(value as string, out dec);
                else
                    decimal.TryParse(((double)value).ToString(), out dec);

                return FormatCurrency(dec, currencyCode);
            }
            else if (!string.IsNullOrEmpty(currencyCode))
            {
                return FormatCurrency(0, currencyCode);
            }
            else
                return "0";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public string FormatCurrency(decimal amount, string currencyCode)
        {
            var culture = (from c in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                           let r = new RegionInfo(c.LCID)
                           where r != null
                           && r.ISOCurrencySymbol.ToUpper() == currencyCode.ToUpper()
                           select c).FirstOrDefault();

            if (culture == null)
                return amount.ToString("0.00");

            return string.Format(culture, "{0:C}", amount);
        }
    }
}
