﻿using System;
using System.Globalization;
using HRApp.Helpers.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Helpers.Converters
{
    public class DomainStrengthsColorConverter : IValueConverter, IMarkupExtension
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int DomainId = (int) value;

                if (DomainId == 1)
                    return Colors.ExecutingDomain;
                else if (DomainId == 2)
                    return Colors.InfluencingDomain;
                else if (DomainId == 3)
                    return Colors.RelationshipDomain;
                else if (DomainId == 4)
                    return Colors.StrategicDomain;
            }
            return Colors.ExecutingDomain;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
