﻿using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Helpers.Converters
{
    public class RatingValueConverter : IValueConverter, IMarkupExtension
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var RatingValue = parameter as string;
            if (value == null)
            {
                return false;
            }
            else
            {
                int res;
                int.TryParse(RatingValue, out res);
                return (int)value <= res;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
