﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HRApp.Helpers.Resources
{
    public static class ActionBarConfig
    {
        public static readonly TextAlignment CenterAlign = Device.RuntimePlatform == Device.iOS ? TextAlignment.Center : TextAlignment.Start;
        public static readonly int ColumnSpan = Device.RuntimePlatform == Device.iOS ? 2 : 1;
        public static readonly double FontSize = Device.RuntimePlatform == Device.iOS ? 20 : 22;
        public static readonly LayoutOptions vertical = Device.RuntimePlatform == Device.iOS ? LayoutOptions.End : LayoutOptions.Center;
        public static readonly LayoutOptions NotificationBadge = Device.RuntimePlatform == Device.iOS ? LayoutOptions.Center : LayoutOptions.Start;
    }
}
