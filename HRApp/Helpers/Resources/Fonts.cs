﻿using System;
using Xamarin.Forms;

namespace HRApp.Helpers.Resources
{
    public static class Fonts
    {
        public static readonly string GothamBook = Device.RuntimePlatform == Device.iOS ? "Gotham-Book" : "Gotham-Book.otf#Open Sans";
        public static readonly string GothamBookItalic = Device.RuntimePlatform == Device.iOS ? "Gotham-BookItalic" : "Gotham-BookItalic.otf#Open Sans";
        public static readonly string GothamBlack= Device.RuntimePlatform == Device.iOS ? "Gotham-Black" : "Gotham-Black.otf#Open Sans";
    }
}