﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRApp.Helpers.Resources
{
    class FontSize
    {
        public static readonly double LargeFont = 20;
        public static readonly double MediumFont = 16;
        public static readonly double SmallFont = 13;
        public static readonly double MicroFont = 11;
        public static readonly double ExtraMicroFont = 9;
    }
}
