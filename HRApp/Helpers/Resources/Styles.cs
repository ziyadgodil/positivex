﻿using System;
using Xamarin.Forms;

namespace HRApp.Helpers.Resources
{
    public class Styles
    {
        [Obsolete]
        public static Style SmallFont = new Style(typeof(Label))
        {
            Setters = {
                new Setter { Property = Label.TextColorProperty, Value = Color.Black },
                new Setter { Property = Label.FontSizeProperty, Value = 14 },
                new Setter { Property = Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap},
                new Setter { Property = Label.FontFamilyProperty, Value = "Calibri"},
                new Setter { Property = Label.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                new Setter { Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center}
            }
        };
    }
}
