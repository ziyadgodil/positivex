﻿using System;
using Xamarin.Forms;

namespace HRApp.Helpers.Resources
{
    public static class Colors
    {
        public static readonly Color NavigationBarBackgroundColor = Color.FromHex("#00ff00");
        public static readonly Color NegativeColor = Color.FromHex("#C90000");
        public static readonly Color PositiveColor = Color.FromHex("#006738");
        public static readonly Color ExecutingDomain = Color.FromRgb(73,41,91);
        public static readonly Color InfluencingDomain = Color.FromRgb(208,118,36);
        public static readonly Color RelationshipDomain = Color.FromRgb(25,58,97);
        public static readonly Color StrategicDomain = Color.FromRgb(136,0,2);

        //Theme Color Style starts
        public static readonly Color ActionBarAqua = Color.FromHex("#4CB5C4");
        public static readonly Color TextColorGrey = Color.FromHex("#727376");
        public static readonly Color NotificationAlertColor = Color.FromHex("#727376");
        public static readonly Color TabPageInactiveMenuItem = Color.FromHex("#727376");
        public static readonly Color TabPageSelectedIcon = Color.FromHex("#4CB5C4");
        public static readonly Color ButtonBackgroundColorAqua = Color.FromHex("#4CB5C4");
        public static readonly Color TextColorAqua = Color.FromHex("#4CB5C4");
        public static readonly Color FrameBorderColorAqua = Color.FromHex("#4CB5C4");
        public static readonly Color FrameBorderColorGray = Color.FromHex("#727376");
        public static readonly Color LinkColor = Color.Blue;

    }
}
