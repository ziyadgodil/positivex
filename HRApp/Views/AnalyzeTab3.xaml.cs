﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using HRApp.Helpers.BaseData;
using HRApp.Helpers.Resources;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class AnalyzeTab3Xaml : BaseContentPage<AnalyzeTab3ViewModel> { }

    public partial class AnalyzeTab3 : AnalyzeTab3Xaml
    {
        public AnalyzeTab3()
        {
            InitializeComponent();


            FilterUserList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && FilterUserList.ItemsSource != null)
                {
                    try
                    {
                        FilterUserList.HeightRequest = (ViewModel.ListAllFilterUsers.Count) * FilterUserList.RowHeight + 60;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };

        }
    }
}
