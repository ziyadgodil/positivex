﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Naxam.Controls.Forms;
using Xamarin.Forms;
using HRApp.Helpers.Resources;

namespace HRApp.Views
{
    //public class AnalyzeTabPageXaml : BaseTabbedPage<UserProfilePageViewModel> { }

    public partial class AnalyzeTabPage : TopTabbedPage
    {
        public AnalyzeTabPage(AnalysisPageViewModel APVM)
        {
            InitializeComponent();
            this.BindingContext = APVM;
            this.Title = "Analysis";
            this.BarTextColor = Color.FromHex("#ffffff");
            this.BarBackgroundColor = Colors.ButtonBackgroundColorAqua;
            this.BarIndicatorColor = Color.FromHex("#006738");
            this.BarTextColor = Color.White;
            this.SelectedTabColor = Color.Transparent;
            this.SwipeEnabled = false;

            //First tab
            var page = new AnalyzeTab1();
            page.ViewModel.ListFilterUsers = APVM.ListFilterUsers;
            var analyzeTab1 = new Xamarin.Forms.NavigationPage(page);
            analyzeTab1.BarBackgroundColor = Colors.ButtonBackgroundColorAqua;
            analyzeTab1.BarTextColor = Color.White;
            analyzeTab1.Title = "Page1";
            this.Children.Add(analyzeTab1);


            //Second tab
            var analyzeTab2 = new Xamarin.Forms.NavigationPage(new AnalyzeTab2());
            analyzeTab2.BindingContext = this.BindingContext;

            analyzeTab2.BarTextColor = Color.White;
            analyzeTab2.Title = "Page2";
            this.Children.Add(analyzeTab2);

            //Third tab

            var page3 = new AnalyzeTab3();
            page3.ViewModel.ListAllFilterUsers = APVM.ListFilterUsers;
            var analyzeTab3 = new Xamarin.Forms.NavigationPage(page3);
            analyzeTab3.BindingContext = this.BindingContext;

            analyzeTab3.BarTextColor = Color.White;
            analyzeTab3.Title = "Page3";
            this.Children.Add(analyzeTab3);
        }
        //protected override void OnAppearing()
        //{
        //    base.OnAppearing();
        //    MessagingCenter.Send<string>("1", "DisableGesture");
        //}

    }
}
