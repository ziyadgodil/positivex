﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HRApp.Helpers.BaseData;
using HRApp.Helpers.Resources;
using HRApp.Models;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{

    public class AnalyzeTab1Xaml : BaseContentPage<AnalyzeTab1ViewModel> { }

    public partial class AnalyzeTab1 : AnalyzeTab1Xaml
    {
        public AnalyzeTab1()
        {
            InitializeComponent();
        }
    }
}
