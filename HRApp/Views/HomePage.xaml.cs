﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRApp.Helpers.BaseData;
using HRApp.Services;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class HomePageXaml : BaseContentPage<HomePageViewModel> { }

    public partial class HomePage : HomePageXaml
    {
        public HomePage()
        {
            InitializeComponent();
            //PieChart.Model = Simplemodel();
            StrengthsList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && StrengthsList.ItemsSource != null)
                {
                    try
                    {
                        if(ViewModel.HomeModel.Strengths == null || ViewModel.HomeModel.Strengths.Count==0)
                        {
                            StrengthsList.HeightRequest = 0;
                            return;
                        }
                        //var ViewModel = ((PaymentViewModel)this.BindingContext);
                        //ViewModel.calculateInvoiceHeight();
                        StrengthsList.HeightRequest = ViewModel.HomeModel.Strengths.Count * StrengthsList.RowHeight;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };


            MessagingCenter.Subscribe<MessagingCenterModel, string>(this, "NotificationCount", (sender, args) =>
            {
                DependencyService.Get<IToolbarItemBadgeService>().SetBadge(this, ToolbarItems[0], args, Color.Red, Color.White);

                //SetNotificationCount(args);
            });
        }

        //public PlotModel Simplemodel()
        //{
        //    var modelP1 = new PlotModel { Title = "" };

        //    dynamic seriesP1 = new PieSeries { StrokeThickness = 1.0, OutsideLabelFormat = "{2:0} %", AngleSpan = 360, StartAngle = 90, InnerDiameter = 0.8 };

        //    seriesP1.Slices.Add(new PieSlice("", 500) { IsExploded = false, Fill = OxyColor.FromRgb(157,15,25) });
        //    seriesP1.Slices.Add(new PieSlice("", 500) { IsExploded = true, Fill = OxyColor.FromRgb(230, 141, 57) });

        //    modelP1.Series.Add(seriesP1);

        //    return modelP1;

        //}
    }
}
