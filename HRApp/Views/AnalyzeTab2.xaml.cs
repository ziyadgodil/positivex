﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HRApp.Helpers.BaseData;
using HRApp.Helpers.Resources;
using HRApp.Models;
using HRApp.ViewModels;
using Syncfusion.SfChart.XForms;
using Xamarin.Forms;

namespace HRApp.Views
{
    //public class AnalyzeTab2Xaml : BaseContentPage<AnalysisPageViewModel> { }

    public partial class AnalyzeTab2 : ContentPage
    {
        public AnalyzeTab2()
        {
            InitializeComponent();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var users = ((AnalysisPageViewModel)this.BindingContext).ListFilterUsers;

            var ListReceivedStrengthsDetail = ((AnalysisPageViewModel)this.BindingContext).ListReceivedStrengthsDetail;

            var StrengthsReceivedChart = new ObservableCollection<ShoutChartModel>();
            var DomainReceivedChart = new ObservableCollection<ShoutChartModel>();


            // Strengths Chart

            var Strengthsresults = ListReceivedStrengthsDetail
                .Select(e => e.StrengthsDetail)
                .GroupBy(d => d.StrengthID)
                .OrderBy(x => x.Key);

            foreach (var item2 in Strengthsresults)
            {
                if (!StrengthsReceivedChart.Any(x => x.StrengthID == item2.Key))
                {
                    StrengthsReceivedChart.Add(new ShoutChartModel() { StrengthID = item2.Key, DomainID = item2.First().DomainID, StrengthName = item2.First().StrengthName, Count = item2.Count() });
                }
                else
                {
                    StrengthsReceivedChart.Where(x => x.StrengthID == item2.Key).ToList().ForEach(x => x.Count += item2.Count());
                }
            }

            StrengthChart.ItemsSource = StrengthsReceivedChart.OrderBy(x => x.StrengthID);


            var StrengthSeriesBrushes = new ObservableCollection<Color>();

            foreach (var item in StrengthsReceivedChart)
            {
                if (item.DomainID == 1)
                {
                    StrengthSeriesBrushes.Add(Colors.ExecutingDomain);
                }
                if (item.DomainID == 2)
                {
                    StrengthSeriesBrushes.Add(Colors.InfluencingDomain);
                }
                if (item.DomainID == 3)
                {
                    StrengthSeriesBrushes.Add(Colors.RelationshipDomain);
                }
                if (item.DomainID == 4)
                {
                    StrengthSeriesBrushes.Add(Colors.StrategicDomain);
                }
            }

            StrengthBrushes.CustomBrushes = StrengthSeriesBrushes;



            //Domain Chart

            var Domainresults = ListReceivedStrengthsDetail
            .Select(e => e.StrengthsDetail)
            .GroupBy(d => d.DomainID)
            .OrderBy(x => x.Key);


            foreach (var item2 in Domainresults)
            {
                if (!DomainReceivedChart.Any(x => x.DomainID == item2.Key))
                {
                    DomainReceivedChart.Add(new ShoutChartModel() { DomainID = item2.Key, StrengthName = item2.First().DomainName, Count = item2.Count() });

                }
                else
                {
                    DomainReceivedChart.Where(x => x.DomainID == item2.Key).ToList().ForEach(x => x.Count += item2.Count());
                }
            }

            DomainChart.ItemsSource = DomainReceivedChart.OrderBy(x => x.DomainID);

            var DomainSeriesBrushes = new ObservableCollection<Color>();
            if (DomainReceivedChart.Any(x => x.DomainID == 1))
            {
                DomainSeriesBrushes.Add(Colors.ExecutingDomain);
            }
            if (DomainReceivedChart.Any(x => x.DomainID == 2))
            {
                DomainSeriesBrushes.Add(Colors.InfluencingDomain);
            }
            if (DomainReceivedChart.Any(x => x.DomainID == 3))
            {
                DomainSeriesBrushes.Add(Colors.RelationshipDomain);
            }
            if (DomainReceivedChart.Any(x => x.DomainID == 4))
            {
                DomainSeriesBrushes.Add(Colors.StrategicDomain);
            }

            DomainBrushes.CustomBrushes = DomainSeriesBrushes;


            //foreach (var item in users)
            //{
            //    var result = await App.HRServiceManager.GetOtherUserAllGivenAndReceivedStrengths(item.UserID);
            //    if(result.data==null)
            //        continue;

            //    //var Strengthsresults = result.data.received
            //    //.Select(e => e.StrengthsDetail)
            //    //.GroupBy(d => d.StrengthID)
            //    //.OrderBy(x=>x.Key);

            //    var Domainresults = result.data.received
            //    .Select(e => e.StrengthsDetail)
            //    .GroupBy(d => d.DomainID)
            //    .OrderBy(x => x.Key);



            //    //foreach (var item2 in Strengthsresults)
            //    //{
            //    //    if (!StrengthsReceivedChart.Any(x => x.StrengthID == item2.Key))
            //    //    {
            //    //        StrengthsReceivedChart.Add(new ShoutChartModel() { StrengthID = item2.Key, StrengthName = item2.First().StrengthName, Count = item2.Count() });
            //    //    }
            //    //    else
            //    //    {
            //    //        StrengthsReceivedChart.Where(x => x.StrengthID == item2.Key).ToList().ForEach(x => x.Count += item2.Count());
            //    //    }
            //    //}

            //    foreach (var item2 in Domainresults)
            //    {
            //        if (!DomainReceivedChart.Any(x => x.StrengthID == item2.Key))
            //        {
            //            DomainReceivedChart.Add(new ShoutChartModel() { StrengthID = item2.Key, StrengthName = item2.First().DomainName, Count = item2.Count() });

            //        }
            //        else
            //        {
            //            DomainReceivedChart.Where(x => x.StrengthID == item2.Key).ToList().ForEach(x => x.Count += item2.Count());
            //        }
            //    }

            //}

            //ColumnSeries columnSeries1 = new ColumnSeries()
            //{
            //    ItemsSource = StrengthsReceivedChart,
            //    XBindingPath = "StrengthName",
            //    YBindingPath = "Count"
            //};
            //StrengthChart.Series.Add(columnSeries1);

        }
    }

    public class ShoutChartModel
    {
        public int StrengthID { get; set; }
        public int DomainID { get; set; }
        public string StrengthName { get; set; }
        public int Count { get; set; }
    }
}
