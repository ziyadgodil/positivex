﻿using System;
using System.Threading.Tasks;
using HRApp.Helpers.BaseData;
using HRApp.Views.SubPages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.MasterMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SliderMenuPageMaster : ContentPage
    {
        public ListView ListView;

        public SliderMenuPageMaster()
        {
            InitializeComponent();
            LoadDataAsync();
            //BindingContext = new SliderMenuPageMasterViewModel();
            //ListView = MenuItemsListView;


            MessagingCenter.Subscribe<MessagingCenterModel, string>(this, "SendShout", (sender, arg) =>
            {
                SendShoutPagenPopup(arg);
            });

            MessagingCenter.Subscribe<MessagingCenterModel>(this, "ProfileImageUpdate", (sender) =>
            {
                LoadDataAsync();
            });
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        public async Task LoadDataAsync()
        {
            var res = await App.HRLocalDataManager.GetUserData();
            if (res != null)
            {
                try
                {
                    lblName.Text = res.FullName;
                    lblEmail.Text = res.Email;
                    if (res.Profile == "DefaultProfile" || string.IsNullOrEmpty(res.Profile))
                    {
                        ProfileImage.Source = ImageSource.FromFile("DefaultProfile");
                    }
                    else
                    {
                        ProfileImage.Source = ImageSource.FromUri(new Uri(res.Profile));
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        void OnLogOutTapped(object sender, EventArgs args)
        {
            Settings.UserName = "";
            Settings.UserPassword = "";
            Settings.UserToken = "";
            App.Instance.MainPage = new LoginPage();
        }

        void OnEditProfileTapped(object sender, EventArgs args)
        {
            ((SliderMenuPage)App.Instance.MainPage).IsPresented = false;
            ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(new UserProfilePage());
        }

        void OnUser360Tapped(object sender, EventArgs args)
        {
            ((SliderMenuPage)App.Instance.MainPage).IsPresented = false;
            ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(new User360Page());
        }

        void OnChangePasswordTapped(object sender, EventArgs args)
        {
            ((SliderMenuPage)App.Instance.MainPage).IsPresented = false;
            ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(new ChangePasswordPage());
        }

        private async void SendShoutPagenPopup(string userId)
        {
            try
            {
                var selectedUser = (await App.HRServiceManager.GetUserByID(userId))?.data;
                if (selectedUser != null && !(((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).CurrentPage is ShoutUserPage))
                { 
                    var page = new ShoutUserPage();
                    page.ViewModel.SelectedUser = selectedUser;
                    await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}