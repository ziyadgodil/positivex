﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRApp.Views.MasterMenu
{
    public class SliderMenuPageMenuItem
    {
        public SliderMenuPageMenuItem()
        {
            TargetType = typeof(SliderMenuPageDetail);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}