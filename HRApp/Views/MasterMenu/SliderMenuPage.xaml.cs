﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.MasterMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SliderMenuPage : MasterDetailPage
    {
        public SliderMenuPage()
        {
            InitializeComponent();
            //ToolbarItem cart = new ToolbarItem()
            //{
            //    Text = "Test",
            //    Icon = "User"
            //};
            //ToolbarItems.Add(cart);
            //MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "DisableGesture", (sender) =>
            {
                if (sender == "0")
                {
                    IsGestureEnabled = false;
                }
                else
                {
                    IsGestureEnabled = true;
                }
            });
        }

        //private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    var item = e.SelectedItem as SliderMenuPageMenuItem;
        //    if (item == null)
        //        return;

        //    var page = (Page)Activator.CreateInstance(item.TargetType);
        //    page.Title = item.Title;

        //    Detail = new NavigationPage(page);
        //    IsPresented = false;

        //    MasterPage.ListView.SelectedItem = null;
        //}
    }
}
