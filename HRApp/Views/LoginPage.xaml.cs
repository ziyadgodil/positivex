﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class LoginPageXaml : BaseContentPage<LoginPageViewModel> { }

    public partial class LoginPage : LoginPageXaml
    {
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}
