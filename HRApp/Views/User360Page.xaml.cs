﻿using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class User360PageXaml : BaseContentPage<User360PageViewModel> { }
    public partial class User360Page : User360PageXaml
    {
        public User360Page()
        {
            InitializeComponent();
            Connection360Shouts.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && Connection360Shouts.ItemsSource != null)
                {
                    try
                    {
                        //var ViewModel = ((PaymentViewModel)this.BindingContext);
                        //ViewModel.calculateInvoiceHeight();

                        if (ViewModel.ConnectionShouts.Count > 0)
                        {
                            //Connection360Shouts.IsVisible = true;
                            ConnectionFrame.IsVisible = true;
                        }
                        else
                        {
                            //Connection360Shouts.IsVisible = false;
                            ConnectionFrame.IsVisible = false;
                        }

                        //StrengthsList.HeightRequest = ViewModel.UserDetail.Strengths.Count * StrengthsList.ItemSize;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };
        } 
    }
}