﻿using System;
using System.Collections.Generic;
using HRApp.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.Views.PopupViews
{
    public partial class SelectAimStrengthsPopup : PopupPage
    {
        public EventHandler<AimDetail> SelectedStrength;
        public AimDetail aimDetail { get; set; }
        public SelectAimStrengthsPopup(AimDetail _aimDetail)
        {
            InitializeComponent();
            aimDetail = _aimDetail;
            lblStrength.Text = aimDetail.StrengthName;
        }

        void OnSaveClicked(object sender, EventArgs args)
        {
            if (!string.IsNullOrEmpty(ActionPlan.Text))
            {
                aimDetail.ActionPlan = ActionPlan.Text;
                SelectedStrength?.Invoke(sender, aimDetail);
            }
            else
            {
                //DisplayAlert("Alert", "Please enter text in action plan.","OK");
                PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Please enter text in action plan.", false));
            }
        }

        void OnCancleClicked(object sender, EventArgs args)
        {
            SelectedStrength?.Invoke(sender, null);
        }
        
    }
}
