﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using HRApp.Models;
using HRApp.Views.MasterMenu;
using HRApp.Views.SubPages;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.PopupViews
{
    public partial class AddNetworkPopup : PopupPage
    {

        public EventHandler<ObservableCollection<UserModel>> AddedUsers;

        public ObservableCollection<UserModel> _userConnectionList { get; set; }
        public ObservableCollection<UserModel> UserConnectionList
        {
            get { return _userConnectionList; }
            set
            {
                _userConnectionList = value;
                LoadData();

            }
        }

        private ObservableCollection<UserModel> _networkuserslist { get; set; }
        public ObservableCollection<UserModel> Networkuserslist 
        {
            get { return _networkuserslist; }
            set { _networkuserslist = value; LoadData(); }
        }

        public AddNetworkPopup()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            InNetworkListUI.ItemsSource = Networkuserslist;
            OutNetworkList.ItemsSource = UserConnectionList;
        }


        private async void OnCheckedUserTapped(object sender, EventArgs args)
        {
            var selectedItem = (UserModel)((TappedEventArgs)args).Parameter;
            //var image = sender as Image;
            //var selectedItem = image.BindingContext as UserModel;
            selectedItem.IsCheckedUser = !selectedItem.IsCheckedUser;
            if (selectedItem.IsCheckedUser)
            {
                AlertPopup alert = new AlertPopup("Alert", "Are you sure you want to add this user?", true);
                await PopupNavigation.Instance.PushAsync(alert);
                alert.AcceptAlertPopup += (object sender1, bool e1) =>
                {
                    if (e1 == true)
                    {
                        AddUserToNetwork(selectedItem);
                    }
                    else
                    {
                        selectedItem.IsCheckedUser = false;
                    }
                };
            }
            else if (!selectedItem.IsCheckedUser) 
            {
                AlertPopup alert = new AlertPopup("Alert", "Do you want to remove this user?", true);
                await PopupNavigation.Instance.PushAsync(alert);
                alert.AcceptAlertPopup += (object sender1, bool e1) =>
                {
                    if (e1 == true)
                    {
                        RemoveUserfromNetwork(selectedItem);
                    }
                    else
                    {
                        selectedItem.IsCheckedUser = true;
                    }
                };
            }
        }

        public async void AddUserToNetwork(UserModel userModel)  
        {
            using (UserDialogs.Instance.Loading()) 
            {
                UserNetworkInputModel userNetworkInput = new UserNetworkInputModel();
                userNetworkInput.NetworkID = userModel.UserID;
                var res = await App.HRServiceManager.AddUserNetwork(userNetworkInput);
                if (res != null && res.status) 
                {
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "User added to network.", false));
                    Networkuserslist.Add(userModel);
                    OutNetworkList.IsVisible = false;
                    InNetworkListUI.IsVisible = true;
                }
            }
        }

        public async void RemoveUserfromNetwork(UserModel userModel)
        {
            using (UserDialogs.Instance.Loading())
            {
                UserNetworkInputModel userNetworkInput = new UserNetworkInputModel();
                userNetworkInput.NetworkID = userModel.UserID;
                var res = await App.HRServiceManager.RemoveUserfromNetwork(userNetworkInput);
                if (res != null && res.status)
                {
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "User removed from network.", false));
                    UserConnectionList.Where(x => x.UserID == userModel.UserID).ToList().ForEach(u => u.IsUserAvailable = false);
                    userModel.IsUserAvailable = false;
                    var removalitem = Networkuserslist.FirstOrDefault(x => x.UserID == userModel.UserID);
                    Networkuserslist.Remove(removalitem);
                    InNetworkListUI.HeightRequest = Networkuserslist.Count * 60;
                }
            }
        }
        private async void OnUserTapped(object sender, EventArgs args)
        {

            var selectedItem = (UserModel)(((TappedEventArgs)args).Parameter);
            
            await App.Instance.MainPage.Navigation.PopAllPopupAsync();

            var page = new ProfilePage();
            page.ViewModel.UserDetail = selectedItem;
            await((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }


        private async void OnShoutTapped(object sender, EventArgs args)
        {

            var selectedItem = (UserModel)(((TappedEventArgs)args).Parameter);

            await App.Instance.MainPage.Navigation.PopAllPopupAsync();

            var page = new ShoutUserPage();
            page.ViewModel.SelectedUser = selectedItem;
            await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }
        

        void OnDoneClicked(object sender, EventArgs args)
        {
            //ObservableCollection<UserModel> selectedUser = new ObservableCollection<UserModel>(UserConnectionList.Where(x => x.IsCheckedUser));

            //if (selectedUser.Count() > 0)
            //    AddedUsers?.Invoke(sender, selectedUser);
            //else
            //    //DisplayAlert("Alert", "Please select at least one user.", "Ok");
            //     PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Please select at least one user.", false));
             PopupNavigation.Instance.PopAllAsync();
        }

        private void SearchEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterUser(e.NewTextValue);
        }
        public void FilterUser(string SearchText)
        {
            if (SearchText != "")
            {
                InNetworkListUI.IsVisible = false;
                ObservableCollection<UserModel> Searchlist = new ObservableCollection<UserModel>();
                Searchlist = new ObservableCollection<UserModel>(UserConnectionList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
                OutNetworkList.ItemsSource = Searchlist;
                OutNetworkList.IsVisible = true;
            }
            else 
            {
                OutNetworkList.IsVisible = false;
                InNetworkListUI.IsVisible = true;
                InNetworkListUI.ItemsSource = Networkuserslist;
            }
            //ObservableCollection<UserModel> Searchlist = new ObservableCollection<UserModel>();
            //ObservableCollection<UserModel> OutNetworkSearchlist = new ObservableCollection<UserModel>();
            
            //OutNetworkSearchlist = new ObservableCollection<UserModel>(UserConnectionList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            //InNetworkListUI.ItemsSource = Searchlist;
            //OutNetworkList.ItemsSource = OutNetworkSearchlist;
        }
        private async void OnCloseButton(object sender, EventArgs args)
        {
            await PopupNavigation.Instance.PopAllAsync();
        }
    }
}