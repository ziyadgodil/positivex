﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.UserDialogs;
using HRApp.Models;
using HRApp.Services;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.PopupViews
{
    public partial class SearchOutofNetworkPopup : PopupPage
    {
        public string Type { get; set; }
        public SearchOutofNetworkPopup()
        {
            InitializeComponent();
            this.CloseWhenBackgroundIsClicked = false;
        }
        private async void OnCloseButton(object sender, EventArgs args)
        {
            await PopupNavigation.Instance.PopAsync();
        }
        private async void btnSendInvitation_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(UserInviteEmail.Text) && InviteNotificationTypePicker.SelectedItem != null)
            {
                if (Regex.Match(UserInviteEmail.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
                {
                    using (UserDialogs.Instance.Loading("Inviting..."))
                    {
                        InviteUserInputModel inviteUserInput = new InviteUserInputModel();
                        inviteUserInput.Type = Type;
                        inviteUserInput.Email = UserInviteEmail.Text;
                        inviteUserInput.NotificationType = InviteNotificationTypePicker.SelectedItem.ToString();
                        var res = await App.HRServiceManager.InviteUser(inviteUserInput);
                        if (res != null && res.status)
                        {
                            //await DisplayAlert("Result", "User Invited Successfully", "Ok");
                            await PopupNavigation.Instance.PopAsync();
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "User Invited Successfully", false));
                            
                        }
                        else if (res.status == false)
                        {
                            //await DisplayAlert("Error", "This user is already Invited!", "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", ServiceConfiguration.CommonErrorMessage, false));
                        }
                    }
                }
                else
                {
                    //await DisplayAlert("Error", "Please enter valid email address!", "Ok");
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Please enter valid email address!", false));
                }
            }
            else
            {
                //await DisplayAlert("Error", "Please enter valid details first!", "OK");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Please enter all details.", false));
            }
        }
    }
}