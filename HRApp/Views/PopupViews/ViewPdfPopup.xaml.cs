﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.PopupViews
{
    public partial class ViewPdfPopup : PopupPage
    {
        public string WebViewSource { get; set; }
        public ViewPdfPopup(string URL)
        {
            InitializeComponent();
            if (Device.RuntimePlatform == Device.Android)
                PdfWebView.Source = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + URL;
            else
                PdfWebView.Source = URL;
        }

        private async void OnCloseButtonTapped(object sender, EventArgs e)
        {
           await PopupNavigation.Instance.PopAsync();
        }
    }
}