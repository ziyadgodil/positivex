﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRApp.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.PopupViews
{
    public partial class AddUserPopup : PopupPage
    {

        public event EventHandler AddedFilter;

        public string ConnectionType { get; set; }

        public ObservableCollection<UserModel> _userConnectionList { get; set; }

        public ObservableCollection<UserModel> UserConnectionList
        {
            get { return _userConnectionList; }
            set
            {
                _userConnectionList = value;
                LoadData();

            }
        }

        public ObservableCollection<UserModel> _outuserConnectionList { get; set; }

        public ObservableCollection<UserModel> OutUserConnectionList
        {
            get { return _outuserConnectionList; }
            set
            {
                _outuserConnectionList = value;
                LoadData();

            }
        }
        public AddUserPopup()
        {
            InitializeComponent();
            //LoadData();
            this.CloseWhenBackgroundIsClicked = false;
            
        }

        public void LoadData()
        {
            ListofInConnection.ItemsSource = UserConnectionList;
            ListofOutConnection.ItemsSource = OutUserConnectionList;
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            
            var Parameter = (UserModel)(((TappedEventArgs)e).Parameter);
            Parameter.IsCheckedUser = !Parameter.IsCheckedUser;
            if (Parameter.IsCheckedUser)
            {
                //bool confirm = await DisplayAlert("Alert", "Are you sure you want to add this user?", "Ok", "Cancel");
                AlertPopup alert = new AlertPopup("Alert", "Are you sure you want to add this user?", true);
                await PopupNavigation.Instance.PushAsync(alert);
                alert.AcceptAlertPopup += (object sender1, bool e1) => 
                {
                    if (e1 == true) 
                    {
                        AddConnection360(Parameter);
                    }
                    else
                    {
                        Parameter.IsCheckedUser = false;
                    }
                };  
            }
            else if(Parameter.IsCheckedUser == false)
            {
                //bool confirm = await DisplayAlert("Alert", "Are you sure you want to remove this user?", "Ok", "Cancel");
                AlertPopup alert = new AlertPopup("Alert", "Are you sure you want to remove this user?", true);
                await PopupNavigation.Instance.PushAsync(alert);
                alert.AcceptAlertPopup += (object sender1, bool e1) => 
                {
                    if (e1 == true)
                    {
                        RemoveConnection(Parameter);
                    }
                    else
                    {
                        Parameter.IsCheckedUser = true;
                    }
                };
                
            }
        }

        void OnSaveClicked(object sender, EventArgs args)
        {
            //ObservableCollection<UserModel> selectedUser = new ObservableCollection<UserModel>(UserConnectionList.Where(x => x.IsCheckedUser));
            //if (selectedUser.Count() > 0)
            //    AddedFilter?.Invoke(sender, selectedUser);
            //else
            //    DisplayAlert("Alert", "Please select at least one user.", "Ok");
            AddedFilter?.Invoke(sender, args);
            PopupNavigation.Instance.PopAsync();
        }
        private async void OnCloseButton(object sender, EventArgs args)
        {
            AddedFilter?.Invoke(sender, args);
            await PopupNavigation.Instance.PopAsync();
        }   
        private void User_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterUser(e.NewTextValue);
        }

        public void FilterUser(string SearchText)
        {
            if (SearchText != "")
            {
                ListofInConnection.IsVisible = false;
                ListofOutConnection.IsVisible = true;
                ObservableCollection<UserModel> Searchlist = new ObservableCollection<UserModel>();
                Searchlist = new ObservableCollection<UserModel>(OutUserConnectionList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower()) || x.Email.Contains(SearchText.ToLower())));
                ListofOutConnection.ItemsSource = Searchlist;
            }
            else 
            {
                ListofInConnection.IsVisible = true;
                ListofOutConnection.IsVisible = false;
                ListofInConnection.ItemsSource = UserConnectionList;
            }

            //ObservableCollection<UserModel> InNetworkSearchList = new ObservableCollection<UserModel>();
            //InNetworkSearchList = new ObservableCollection<UserModel>(UserConnectionList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower()) || x.Email.Contains(SearchText.ToLower())));
            //ListofInConnection.ItemsSource = InNetworkSearchList;
        }


        private void NotificationTypePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var usermodel = (UserModel)(picker.BindingContext);
            //var parameter =(UserModel)(((TappedEventArgs)e).Parameter);
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                usermodel.NotificationType = (string)picker.ItemsSource[selectedIndex];
                //if (usermodel.IsCheckedUser == true) 
                //{
                //    //AddConnection360(usermodel);
                //}
            }
        }
        private void NotificationTypePicker_Unfocused(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var usermodel = (UserModel)(picker.BindingContext);
            if (usermodel.IsCheckedUser == true)
            {
                AddConnection360(usermodel);
            }
        }
        private async void OnInviteUserImgTapped(object sender, EventArgs e)
        {
            SearchOutofNetworkPopup searchOutofNetworkPopup = new SearchOutofNetworkPopup();
            searchOutofNetworkPopup.Type = ConnectionType;
            await PopupNavigation.Instance.PushAsync(searchOutofNetworkPopup);
        }

        public async void AddConnection360(UserModel userModel) 
        {
            if (userModel.InviteUser == 1)
            {
                InviteUserInputModel inviteUserInput = new InviteUserInputModel();
                inviteUserInput.Type = ConnectionType;
                inviteUserInput.Email = userModel.Email;
                inviteUserInput.NotificationType = userModel.NotificationType;
                var res = await App.HRServiceManager.InviteUser(inviteUserInput);
                if(res!= null && !res.status)         
                {
                    //userModel.UpcomingNotificationDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            else if (userModel.InviteUser == 0) 
            {
                Connection360InputModel connectionInputModel = new Connection360InputModel();
                connectionInputModel.ConnectionID = userModel.UserID;
                connectionInputModel.ConnectionType = ConnectionType;
                connectionInputModel.NotificationType = userModel.NotificationType;
                var res = await App.HRServiceManager.AddUserConnection(connectionInputModel);
                if (res != null && res.status && res.data != null)
                {
                    userModel.Connection360ID = res.data.ConnectionID;
                    DateTime CurrentDate = DateTime.Now;
                    if (userModel.NotificationType == "Weekly")
                        CurrentDate = CurrentDate.AddDays(07);
                    else if (userModel.NotificationType == "Bi-Weekly")
                        CurrentDate = CurrentDate.AddDays(15);
                    else if (userModel.NotificationType == "Monthly")
                        CurrentDate = CurrentDate.AddMonths(1);
                    userModel.UpcomingNotificationDate = CurrentDate;
                }
            }
        }

        public async void RemoveConnection(UserModel userModel) 
        {
            RemoveConnection360Model removeConnection360 = new RemoveConnection360Model();
            removeConnection360.ID = userModel.Connection360ID;
            removeConnection360.InviteUser = userModel.InviteUser;
            await App.HRServiceManager.RemoveConnection360(removeConnection360);
        }
    }
}