﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using HRApp.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.PopupViews
{
    public partial class AimNotePopup : PopupPage
    {
        public int AimID { get; set; }
        public AimNotePopup()
        {
            InitializeComponent();
        }

        private async void BtnSend_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(AimNoteEditor.Text))
            {
                using (UserDialogs.Instance.Loading())
                {
                    ShareWithManagerInputModel managerInputModel = new ShareWithManagerInputModel();
                    managerInputModel.ID = AimID;
                    managerInputModel.AimNote = AimNoteEditor.Text;
                    var res = await App.HRServiceManager.ShareWithManager(managerInputModel);
                    if (res != null && res.status)
                    {
                        await PopupNavigation.Instance.PopAsync();
                        await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", res.msg, false));
                    }
                    else if (res != null && !res.status)
                    {
                        await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", res.msg, false));
                    }
                }
            }
            else 
            {
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Please enter valid note!", false));
            }
        }

        private async void OnCloseButtonTapped(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

    }
}