﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.Views.PopupViews
{
    public partial class AnalysisFilterPopup : PopupPage
    {
        public EventHandler<ObservableCollection<UserModel>> AddedFilter;
        public AnalysisFilterPopup()
        {
            InitializeComponent();
            LoadData();

        }
        public ObservableCollection<UserModel> UserList { get; set; }
        public async void LoadData()
        {
            using (UserDialogs.Instance.Loading("Loading...")) 
            {
                var AllNetworkUsers = await App.HRServiceManager.GetAllNetworkUsers();
                var currentuser = AllNetworkUsers.data.items.FirstOrDefault(x => x.UserID == Settings.UserToken);
                if (currentuser != null) 
                {
                    AllNetworkUsers.data.items.Remove(currentuser);
                    AllNetworkUsers.data.items.Insert(0, currentuser);
                }
                UserList = new ObservableCollection<UserModel>(AllNetworkUsers.data.items);
                lstAllAnalysisUser.ItemsSource = UserList;
            }

            //var data = await App.HRLocalDataManager.GetAllNetworkUsers();
            // UserList = data.Select(s => s.UserDetail)
            //.GroupBy(p => p.UserID)
            //.Select(g => g.FirstOrDefault())
            //.ToList();
            //UserList = await App.HRLocalDataManager.GetAllNetworkUsers();
            //UserList = new ObservableCollection<UserModel>(AllNetworkUsers.data.items.Where(x => x != null && x.UserID != Settings.UserToken));
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {


            //var image =  sender as Image;
            var parameter = (UserModel)((TappedEventArgs)e).Parameter;
            //var selectedItem = image.BindingContext as UserModel;
            parameter.IsCheckedUser = !parameter.IsCheckedUser;
            //if (selectedItem == null) return;

        }

        void OnSaveClicked(object sender, EventArgs args)
        {

            ObservableCollection<UserModel> selectedUser = new ObservableCollection<UserModel>(UserList.Where(x=>x.IsCheckedUser));

            if (selectedUser.Count() > 0)
                AddedFilter?.Invoke(sender, selectedUser);
            else
                //DisplayAlert("Alert", "Please select at least 1 user.", "Ok");
                PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Please select at least 1 user.", false));

        }
    }
}
