﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HRApp.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.Views.PopupViews
{
    public partial class AddStrengthsPopup : PopupPage
    {
        public EventHandler<CustomStrengthModel> AddedStrengths;
        public ObservableCollection<StrengthModel> UserStrengths { get; set; }
        public AddStrengthsPopup()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }
        public async void LoadData()
        {
            //var res = await App.HRLocalDataManager.GetUserData();
            var StrengthData = await App.HRLocalDataManager.GetAllStrengths();
            if (UserStrengths != null)
                StrengthPicker.ItemsSource = StrengthData.Where(p => !UserStrengths.Any(p2 => p2.StrengthID == p.StrengthID))
                    .OrderBy(s => s.StrengthName).ToList();
            else
                StrengthPicker.ItemsSource = StrengthData.OrderBy(d => d.StrengthName).ToList();
            StrengthPicker.ItemDisplayBinding = new Binding("StrengthName");
            StrengthPicker.SelectedIndexChanged += OnPickerSelectedIndexChanged;
        }

        void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                var selectedItem = (StrengthModel)picker.ItemsSource[selectedIndex];
                lblDefination.Text = selectedItem.StrengthDetail;
                lblDomain.Text = "Domain: " + selectedItem.DomainName;
                DomainFrame.IsVisible = true;
                DefinationFrame.IsVisible = true;
                //(StrengthModel)StrengthPicker.SelectedItem
            }
        }

        void OnSaveClicked(object sender, EventArgs args)
        {
            if (StrengthPicker.SelectedItem != null)
            {
                StrengthModel strengthModel = (StrengthModel)StrengthPicker.SelectedItem;

                CustomStrengthModel customStrengthModel = new CustomStrengthModel();

                customStrengthModel.StrengthsId = strengthModel.StrengthID;
                customStrengthModel.Comment = txtComment.Text;

                AddedStrengths?.Invoke(sender, customStrengthModel);
            }
            else 
            {
                //App.Instance.MainPage.DisplayAlert("Error", "Please select strength first!", "Ok");
                PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Please select strength first!", false));
            }
            
        }
    }
    public class CustomStrengthModel
    {
        public int StrengthsId { get; set; }
        public string Comment { get; set; }
    }
}
