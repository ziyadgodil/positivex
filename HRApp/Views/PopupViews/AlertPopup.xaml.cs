﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms.Xaml;

namespace HRApp.Views.PopupViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertPopup : PopupPage
    {
        public event EventHandler<bool> AcceptAlertPopup;

        public AlertPopup(String title, String body, bool hascancel)
        {
            InitializeComponent();
            Title.Text = title;
            Body.Text = body;
            if (!hascancel)
            {
                OnlyOk.IsVisible = true;
                OkCancel.IsVisible = false;
            }
        }

        private void Cancel_Tapped(object sender, EventArgs e)
        {
            CloseAllPopup();
            AcceptAlertPopup?.Invoke(sender, false);
        }

        private void Ok_Tapped(object sender, EventArgs e)
        {
            CloseAllPopup();
            AcceptAlertPopup?.Invoke(sender, true);
        }

        protected override bool OnBackgroundClicked()
        {
            return false;
        }

        private async void CloseAllPopup()
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private void OnlyOk_Tapped(object sender, EventArgs e)
        {
            CloseAllPopup();
        }
    }
}