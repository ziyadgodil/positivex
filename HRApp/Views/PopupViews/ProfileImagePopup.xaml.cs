﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.Views.PopupViews
{
    public partial class ProfileImagePopup : PopupPage
    {
        public ProfileImagePopup(string source)
        {
            InitializeComponent();
            if (source == "DefaultProfile")
            {
                return;
            }
            else
            {
                try
                {
                    ProfileImage.Source = ImageSource.FromUri(new Uri(source));
                }
                catch (Exception ex) { }
            }
        }

        private async void OnCloseButton(object sender, EventArgs args)
        {
            await PopupNavigation.Instance.PopAllAsync();
        }
    }
}
