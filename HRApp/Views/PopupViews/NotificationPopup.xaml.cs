﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HRApp.Models;
using System.Collections.ObjectModel;
using HRApp.Views.MasterMenu;
using HRApp.Helpers.BaseData;
using Acr.UserDialogs;

namespace HRApp.Views.PopupViews
{
    public partial class NotificationPopup : PopupPage
    {
        private ObservableCollection<NotificationModel> _notificationList { get; set; }
        public ObservableCollection<NotificationModel> NotificationList
        {
            get { return _notificationList; }
            set { _notificationList = value; LoadData(); }
        }

        public event EventHandler<NotificationModel> UpdateNotification;
            
        public NotificationPopup()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            UserNotificationListView.ItemsSource = NotificationList;
        }

        private async void OnCloseButton(object sender, EventArgs args)
        {
            await PopupNavigation.Instance.PopAllAsync();
        }

        private void NotificationSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterUser(e.NewTextValue);
        }
        public void FilterUser(string SearchText)
        {
            ObservableCollection<NotificationModel> SearchNotificationList = new ObservableCollection<NotificationModel>();
            SearchNotificationList = new ObservableCollection<NotificationModel>(NotificationList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            UserNotificationListView.ItemsSource = SearchNotificationList;
        }

        private async void TappedOnShoutButton(object sender, EventArgs args)
        {
            var selectedItem = new NotificationModel();
            using (UserDialogs.Instance.Loading("Loading.."))
            {
                await App.Instance.MainPage.Navigation.PopAllPopupAsync();
                selectedItem = (NotificationModel)(((TappedEventArgs)args).Parameter);
                NotificationList.Remove(selectedItem);
                UserNotificationDeleteInput userNotificationDeleteInput = new UserNotificationDeleteInput();
                userNotificationDeleteInput.ID = selectedItem.NotificationID;
                var res = await App.HRServiceManager.RemoveNotification(userNotificationDeleteInput);
            }
            if (selectedItem.Type == "0")
            {
                MessagingCenter.Send(new MessagingCenterModel { }, "SendShout", selectedItem.UserID);
            }
            else if (selectedItem.Type == "1" || selectedItem.Type == "2")
            {
                UpdateNotification?.Invoke(sender, selectedItem);
                //await App.Instance.MainPage.DisplayAlert(selectedItem.StrengthName, selectedItem.ShoutComment, "OK");
                await PopupNavigation.Instance.PushAsync(new AlertPopup(selectedItem.StrengthName, selectedItem.ShoutComment, false));
            }
            
            //var page = new ShoutUserPage();
            //page.ViewModel.SelectedUser = selectedItem;
            //await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }
    }
}