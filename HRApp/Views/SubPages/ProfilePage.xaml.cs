﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views.SubPages
{
    public class ProfilePageXaml : BaseContentPage<ProfilePageViewModel> { }

    public partial class ProfilePage : ProfilePageXaml
    {
        public ProfilePage()
        {
            InitializeComponent();

            StrengthsList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && StrengthsList.ItemsSource != null)
                {
                    try
                    {
                        StrengthsList.HeightRequest = ViewModel.UserStrengths.Count * StrengthsList.RowHeight;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };
        }
    }
}
