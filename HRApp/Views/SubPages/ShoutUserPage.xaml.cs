﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class ShoutUserXaml : BaseContentPage<ShoutOutPageViewModel> { }

    public partial class ShoutUserPage : ShoutUserXaml
    {
        public ShoutUserPage()
        {
            InitializeComponent();
        }
    }
}
