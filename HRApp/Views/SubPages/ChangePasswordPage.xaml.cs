﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp.Views
{
    public class ChangePasswordPageXaml : BaseContentPage<ChangePasswordPageViewModel> { }
    public partial class ChangePasswordPage : ChangePasswordPageXaml
    {
        public ChangePasswordPage()
        {
            InitializeComponent();
        }
    }
}