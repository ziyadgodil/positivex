﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

using Xamarin.Forms;
using HRApp.Helpers.Resources;

namespace HRApp.Views.NavBar
{
    public partial class BottomTabPage : Xamarin.Forms.TabbedPage
    {
        [Obsolete]
        public BottomTabPage()
        {
            InitializeComponent();

            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetIsSmoothScrollEnabled(false);
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetBarSelectedItemColor(Colors.TabPageSelectedIcon);
            //On<Android>().SetBarSelectedItemColor(Color.FromHex("#30B031"));
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetBarItemColor(Colors.TabPageInactiveMenuItem);
            //On<Xamarin.Forms.PlatformConfiguration.Android>().SetIsSwipePagingEnabled(false);
            this.BarBackgroundColor = Color.White;


            base.CurrentPageChanged += CurrentPageHasChanged;

            var homePage = new Xamarin.Forms.NavigationPage(new HomePage());
            homePage.IconImageSource = "Home";
            homePage.BarTextColor = Color.White;
            homePage.Title = "Home";
            this.Children.Add(homePage);

            var Shout = new Xamarin.Forms.NavigationPage(new ShoutOutPage());
            Shout.IconImageSource = "Shout";
            Shout.BarTextColor = Color.White;
            Shout.Title = "Shout Out";
            this.Children.Add(Shout);

            var Analysis = new Xamarin.Forms.NavigationPage(new AnalysisPage());
            Analysis.IconImageSource = "Analysis";
            Analysis.BarTextColor = Color.White;
            Analysis.Title = "Analysis";
            this.Children.Add(Analysis);

            var Aim = new Xamarin.Forms.NavigationPage(new AIMPage());
            Aim.IconImageSource = "Aim";
            Aim.BarTextColor = Color.White;
            Aim.Title = "Aim";
            this.Children.Add(Aim);

            var Resource = new Xamarin.Forms.NavigationPage(new ResoucesPage());
            Resource.IconImageSource = "Resource";
            Resource.BarTextColor = Color.White;
            Resource.Title = "Resource";
            this.Children.Add(Resource);
        }

        protected async void CurrentPageHasChanged(object sender, EventArgs e)
        {
            //if (sender != null)
            //{
                //var tabbedPage = (BottomTabPage)sender;
                //if (((NavigationPage)tabbedPage.CurrentPage).CurrentPage is ShoutOutPage)
                //{   
                //}
                //else
                //{
                //}
            //}
            this.Title = CurrentPage.Title;
        }

        public void SwitchToAnalysis()
        {
            CurrentPage = this.Children[2];
        }
    }
}
