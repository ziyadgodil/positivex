﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class ShoutOutPageXaml : BaseContentPage<ShoutOutPageViewModel> { }

    public partial class ShoutOutPage : ShoutOutPageXaml
    {
        public ShoutOutPage()
        {
            InitializeComponent();

            SearchUserList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && SearchUserList.ItemsSource != null)
                {
                    try
                    {
                        if (ViewModel.NetworkUsers.Count > 10)
                            SearchUserList.HeightRequest = 10 * 40;
                        else if (ViewModel.NetworkUsers.Count == 0)
                            SearchUserList.HeightRequest = 0;
                        else
                            SearchUserList.HeightRequest = ViewModel.NetworkUsers.Count * 40;

                        UserFrame.HeightRequest = SearchUserList.HeightRequest;
                    }
                    catch (Exception ex)
                    {
                        SearchUserList.HeightRequest = 0;
                    }
                }
            };
        }



        public void UserSearchFocus(object sender, EventArgs args)
        {
            ViewModel.IsUserListVisible = true;
            txtsearch.Text = "";
            SearchUserList.SelectedItem = null;
            ViewModel.SelectedUserName = "Collegue Name";
            ViewModel.IsPickerEnable = false;
            if (ViewModel.CommentText != null)
                ViewModel.CommentText = "";
        }
        public void UserSearchUnfocused(object sender, EventArgs args)
        {
            ViewModel.IsUserListVisible = false;
        }

        private void OnDownArrowTapped(object sender, EventArgs e)
        {
            if(!picker.IsFocused)
              picker.Focus();
        }
    }
}
