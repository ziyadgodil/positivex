﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class AnalysisPageXaml : BaseContentPage<AnalysisPageViewModel> { }

    public partial class AnalysisPage : AnalysisPageXaml
    {
        public AnalysisPage()
        {
            InitializeComponent();
            
        }
    }
}
