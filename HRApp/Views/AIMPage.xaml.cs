﻿using System;
using System.Collections.Generic;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.ViewModels;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.Views
{
    public class AIMPageXaml : BaseContentPage<AIMPageViewModel> { }

    public partial class AIMPage : AIMPageXaml
    //public partial class AIMPage : ContentPage
    {
        public AIMPage()
        {
            InitializeComponent();

            aimList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && aimList.ItemsSource != null)
                {
                    try
                    {
                        //testing
                        aimList.HeightRequest = (ViewModel.AimList.Count) * aimList.RowHeight;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };

            StrengthList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && StrengthList.ItemsSource != null)
                {
                    try
                    {
                        if (ViewModel.AllStrengths.Count > 0)
                        {
                            StrengthList.HeightRequest = (ViewModel.AllStrengths.Count) * StrengthList.RowHeight;
                        }
                        else
                        {
                            StrengthList.HeightRequest = 1;
                        }
                        
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };

            AimDetailList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && AimDetailList.ItemsSource != null)
                {
                    try
                    {
                        //var ViewModel = ((PaymentViewModel)this.BindingContext);
                        //ViewModel.calculateInvoiceHeight();
                        AimDetailList.HeightRequest = (ViewModel.AddEditAimDetail.AimDetail.Count+1) * AimDetailList.RowHeight;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };


            ListAimActivity.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && ListAimActivity.ItemsSource != null)
                {
                    try
                    {
                        ListAimActivity.HeightRequest = (ViewModel.AddEditAimDetail.AimActivity.Count) * ListAimActivity.RowHeight;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };

            AimDetailList.ItemTapped += async (s, e) =>
            {
                var data = (AimDetail)e.Item;
                //await DisplayAlert(data.StrengthName, data.ActionPlan, "OK");
                await PopupNavigation.Instance.PushAsync(new AlertPopup(data.StrengthName, data.ActionPlan, false));
                AimDetailList.SelectedItem = null;
            };

        }

    }
}
