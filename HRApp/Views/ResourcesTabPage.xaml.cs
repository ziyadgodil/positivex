﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRApp.Helpers.BaseData;
using HRApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace HRApp.Views
{
    public class ResourcesTabPageXaml : BaseContentPage<ResourcesTabPageViewModel> { }
    public partial class ResourcesTabPage : ResourcesTabPageXaml
    {
        public ResourcesTabPage()
        {
            InitializeComponent();

            #region #LastCode#
            //var allpages = new Xamarin.Forms.NavigationPage(new ResourceTabDetail());
            ////allpages.BindingContext = this.BindingContext;
            //allpages.BarTextColor = Color.White;
            //allpages.Title = "All";
            //this.Children.Add(allpages);

            //var executing = new Xamarin.Forms.NavigationPage(new ResourceTabDetail());
            ////executing.BindingContext = this.BindingContext;
            //executing.BarTextColor = Color.White;
            //executing.Title = "Executing";
            //this.Children.Add(executing);

            //var influencing = new Xamarin.Forms.NavigationPage(new ResourceTabDetail());
            ////influencing.BindingContext = this.BindingContext;
            //influencing.BarTextColor = Color.White;
            //influencing.Title = "Influencing";
            //this.Children.Add(influencing);

            //var relation = new Xamarin.Forms.NavigationPage(new ResourceTabDetail());
            ////relation.BindingContext = this.BindingContext;
            //relation.BarTextColor = Color.White;
            //relation.Title = "Relationship Building";
            //this.Children.Add(relation);

            //var strategy = new Xamarin.Forms.NavigationPage(new ResourceTabDetail());
            ////strategy.BindingContext = this.BindingContext;
            //strategy.BarTextColor = Color.White;
            //strategy.Title = "Strategies";
            //this.Children.Add(strategy);
            #endregion
        }
    }
}