﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.ViewModels;
using System.Linq;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Extensions;
using Syncfusion.DataSource.Extensions;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms.Control.Helpers;
using System.Reflection;

namespace HRApp.Views
{
    public class UserProfilePageXaml : BaseContentPage<UserProfilePageViewModel> { }

    public partial class UserProfilePage : UserProfilePageXaml
    {
        public string OldValues { get; set; } = "";
        public UserProfilePage()
        {
            InitializeComponent();
            //StrengthsList.Loaded += ListView_Loaded;
            StrengthsList.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) =>
            {
                if ((e.PropertyName == "Renderer" || e.PropertyName == "ItemsSource") && StrengthsList.ItemsSource != null)
                {
                    try
                    {
                        if (ViewModel.UserDetail.Strengths.Count > 0)
                        {
                            StrengthsList.HeightRequest = ViewModel.UserDetail.Strengths.Count * StrengthsList.ItemSize;
                        }
                        else
                        {
                            StrengthsList.HeightRequest = 1;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };
            StrengthsList.ItemDragging += ListView_ItemDraggingAsync;


        }

        //private void ListView_Loaded(object sender, ListViewLoadedEventArgs e)
        //{
        //    var visualContainer = StrengthsList.GetVisualContainer();
        //    var totalextent = (double)visualContainer.GetType().GetRuntimeProperties().FirstOrDefault(container => container.Name == "TotalExtent").GetValue(visualContainer);
        //    var parent = new StackLayout();
        //    parent.LayoutChildren(this.Width, totalextent);
        //}

        //private void ChangeItemsSource_Clicked(object sender, EventArgs e)
        //{
        //    this.viewModel.add();
        //    var visualContainer = StrengthsList.GetVisualContainer();
        //    baseLayout.HeightRequest = visualContainer.Height;
        //}

        private async void ListView_ItemDraggingAsync(object sender, ItemDraggingEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                await Task.Delay(100);
                ChangeData(e);
                
            }
        }

        private void ChangeData(ItemDraggingEventArgs e)
        {
            ViewModel.UserDetail.Strengths.MoveTo(e.OldIndex, e.NewIndex);
            ViewModel.UserDetail.StrengthDetail.MoveTo(e.OldIndex, e.NewIndex);
            for (int i = 0; i < ViewModel.UserDetail.Strengths.Count; i++)
            {
                (ViewModel.UserDetail.Strengths[i] as StrengthModel).StrengthOrder = i+1;
                (ViewModel.UserDetail.Strengths[i] as StrengthModel).StrengthComment = ViewModel.UserDetail.StrengthDetail[i];
            }
        }

        //Best section
        private void BestEditor_Focused(object sender, FocusEventArgs e)
        {
            OldValues = ViewModel.UserDetail.Best;
        }
        private void BestEditor_Unfocused(object sender, FocusEventArgs e)
        {
            if (OldValues != BestEditor.Text.Trim()) 
            {
                ViewModel.UpdateBest();
            }
        }

        // Wrost section
        private void WrostEditor_Focused(object sender, FocusEventArgs e)
        {
            OldValues = ViewModel.UserDetail.Worst;
        }
        private void WrostEditor_Unfocused(object sender, FocusEventArgs e)
        {
            if (OldValues != WrostEditor.Text.Trim()) 
            {
                ViewModel.UpdateWrost();
            }
        }

        //Consistently section
        private void ConsistentlyEditor_Focused(object sender, FocusEventArgs e)
        {
            OldValues = ViewModel.UserDetail.Consistently;
        }
        private void ConsistentlyEditor_Unfocused(object sender, FocusEventArgs e)
        {
            if (OldValues != ConsistentlyEditor.Text.Trim())
            {
                ViewModel.UpdateConsistently();
            }
        }

        //Detail section
        private void DetailEditor_Focused(object sender, FocusEventArgs e)
        {
            OldValues = ViewModel.UserDetail.Detail;
        }
        private void DetailEditor_Unfocused(object sender, FocusEventArgs e)
        {
            if (OldValues != DetailEditor.Text.Trim())
            {
                ViewModel.UpdateDetail();
            }
        }

        async void OnEditStrengthsTapped(object sender, EventArgs args)
        {
            AddStrengthsPopup addStrengthsPopup = new AddStrengthsPopup();
            addStrengthsPopup.UserStrengths = ViewModel.UserDetail.Strengths;

            addStrengthsPopup.AddedStrengths += async (object sender1, CustomStrengthModel strengthModel) =>
            {
                await App.Instance.MainPage.Navigation.PopAllPopupAsync();
                if (strengthModel!=null)
                {
                    if (ViewModel.UserDetail.Strengths == null)
                        ViewModel.UserDetail.Strengths = new ObservableCollection<StrengthModel>();

                    if (ViewModel.UserDetail.StrengthDetail == null)
                        ViewModel.UserDetail.StrengthDetail = new ObservableCollection<string>();

                    var strength = await App.HRLocalDataManager.GetStrengthDetailById(strengthModel.StrengthsId);

                    strength.StrengthComment = strengthModel.Comment;

                    ViewModel.UserDetail.Strengths.Add(strength);

                    ViewModel.UserDetail.StrengthDetail.Add(strengthModel.Comment);

                    ChangeStrengthsOrder();

                    StrengthsList.HeightRequest = ViewModel.UserDetail.Strengths.Count * StrengthsList.ItemSize;

                }
            };

            await App.Instance.MainPage.Navigation.PushPopupAsync(addStrengthsPopup);
        }

        async void OnDeleteStrengthTapped(object sender, EventArgs args)
        {
            AlertPopup alertPopup = new AlertPopup("Alert", "Are you sure you want to delete this strength?",true);
            await PopupNavigation.Instance.PushAsync(alertPopup);
            alertPopup.AcceptAlertPopup += (object sender1, bool e1) => 
            {
                if (e1 == true) 
                {
                    try
                    {

                        var strength = (StrengthModel)((TappedEventArgs)args).Parameter;

                        var index = ViewModel.UserDetail.Strengths.Select((v, i) => new { Strength = v, index = i }).First(x => x.Strength.StrengthID == strength.StrengthID).index;

                        ViewModel.UserDetail.Strengths.Remove(ViewModel.UserDetail.Strengths.FirstOrDefault(x => x.StrengthID == strength.StrengthID));

                        ViewModel.UserDetail.StrengthDetail.RemoveAt(index);

                        ChangeStrengthsOrder();

                        if (ViewModel.UserDetail.Strengths.Count > 0)
                        {
                            StrengthsList.HeightRequest = ViewModel.UserDetail.Strengths.Count * StrengthsList.ItemSize;
                        }
                        else
                        {
                            StrengthsList.HeightRequest = 1;
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
            };
            //var confirm = await App.Instance.MainPage.DisplayAlert("Alert", "Are you sure you want to delete this strength?", "Ok", "Cancel");
            //if (confirm)
            //{
            //    try
            //    {

            //        var strength = (StrengthModel)((TappedEventArgs)args).Parameter;

            //        var index = ViewModel.UserDetail.Strengths.Select((v, i) => new { Strength = v, index = i }).First(x => x.Strength.StrengthID == strength.StrengthID).index;

            //        ViewModel.UserDetail.Strengths.Remove(ViewModel.UserDetail.Strengths.FirstOrDefault(x => x.StrengthID == strength.StrengthID));

            //        ViewModel.UserDetail.StrengthDetail.RemoveAt(index);

            //        ChangeStrengthsOrder();

            //        if (ViewModel.UserDetail.Strengths.Count > 0)
            //        {
            //            StrengthsList.HeightRequest = ViewModel.UserDetail.Strengths.Count * StrengthsList.ItemSize;
            //        }
            //        else
            //        {
            //            StrengthsList.HeightRequest = 1;
            //        }
                    
            //    }
            //    catch(Exception ex)
            //    {

            //    }
            //    //ViewModel.UserDetail.Strengths.Remove(strength);
            //}

        }

        public void ChangeStrengthsOrder()
        {
            try
            {
                var order = 1;
                foreach (var item in ViewModel.UserDetail.Strengths)
                {
                    item.StrengthOrder = order;
                    item.StrengthComment = ViewModel.UserDetail.StrengthDetail[order - 1];
                    order++;
                }
            }
            catch (Exception ex)
            {

            }
        }

        void OnFullNameTapped(object sender, EventArgs args)
        {
            if (LblFullName.IsVisible)
            {
                LblFullName.IsVisible = false;
                FulNameFrame.IsVisible = true;
                FullNameEditor.Focus();
            }
            else if (LblFullName.IsVisible == false)
            {
                ViewModel.UpdateFullName();
                FulNameFrame.IsVisible = false;
                LblFullName.IsVisible = true;
            }
        }
    }
}
