﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HRApp.Models
{
    public class AimModel : BaseModel
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public string AimTitle { get; set; }
        
        ObservableCollection<AimActivity> _aimActivity { get; set; }
        public ObservableCollection<AimActivity> AimActivity { get { return _aimActivity; } set { _aimActivity = value; SetPropertyChanged(nameof(AimActivity)); } }

        ObservableCollection<AimDetail> _aimDetail { get; set; }
        public ObservableCollection<AimDetail> AimDetail { get { return _aimDetail; } set { _aimDetail = value; SetPropertyChanged(nameof(AimDetail)); } }
        public string IsShareWithManager { get; set; }
    }

    public class AimActivity
    {
        public string Text { get; set; }
        public string TextDate { get; set; }
        public DateTime DisplayDate
        {
            get
            {
                if (!string.IsNullOrEmpty(TextDate))
                {
                    DateTime dateTime = DateTime.ParseExact(TextDate,  "M/d/yyyy h:mm:ss tt" , System.Globalization.CultureInfo.InvariantCulture);
                    return dateTime.ToLocalTime();
                }
                else
                {
                    return DateTime.Now.Date;
                }
            }
        }
    }

    public class AimDetail : BaseModel
    {
        public string StrengthId { get; set; }
        public string StrengthName { get; set; }
        public string ActionPlan { get; set; }
        string _rating { get; set; }
        public string Rating { get { return _rating; } set { _rating = value; SetPropertyChanged(nameof(Rating)); } }
    }

    public class RemoveAimInputModel
    {
        public int ID { get; set; }
    }

    public class ShareWithManagerInputModel
    {
        public int ID { get; set; }
        public string AimNote { get; set; }
    }
}
