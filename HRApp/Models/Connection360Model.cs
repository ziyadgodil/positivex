﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace HRApp.Models
{
    public class Connection360Model : BaseModel
    {
        public UserModel UserDetail { get; set; }

        public string ConnectionType { get; set; }

        public ObservableCollection<ConnectionShout> connectionShouts { get; set; }

        private string _notificationType { get; set; }
        public string NotificationType
        {
            get
            {
                return _notificationType;
            }
            set
            {
                _notificationType = value;
                if (UserDetail != null)
                {
                    UserDetail.NotificationType = value;
                }

                SetPropertyChanged(nameof(NotificationType));
                SetPropertyChanged(nameof(UserDetail));
            }
        }
        private int _connectionid { get; set; }
        public int ConnectionID 
        {
            get{ return _connectionid; }
            set 
            {
                _connectionid = value;
                if (UserDetail != null) 
                {
                    UserDetail.Connection360ID = value;
                }
                SetPropertyChanged(nameof(ConnectionID));
                SetPropertyChanged(nameof(UserDetail));
            }
        }

        private int _inviteUser { get; set; }
        public int InviteUser 
        {
            get { return _inviteUser; }
            set 
            {
                _inviteUser = value;
                if (UserDetail != null)
                {
                    UserDetail.InviteUser = value;
                }
                SetPropertyChanged(nameof(InviteUser));
                SetPropertyChanged(nameof(UserDetail));
            }
        }

        private string _updatedAt { get; set; }
        public string UpdatedAt 
        {
            get { return _updatedAt; }
            set { 
                _updatedAt = value;
                if (UserDetail != null) 
                {
                    //UserDetail.NextNotificationDate = value;

                    if (!string.IsNullOrEmpty(value))
                    {
                        UserDetail.UpcomingNotificationDate = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        if (NotificationType == "Weekly")
                        {
                            UserDetail.UpcomingNotificationDate = UserDetail.UpcomingNotificationDate.AddDays(07);
                        }
                        else if (NotificationType == "Bi-Weekly")
                        {
                            UserDetail.UpcomingNotificationDate = UserDetail.UpcomingNotificationDate.AddDays(15);
                        }
                        else if (NotificationType == "Monthly")
                        {
                            UserDetail.UpcomingNotificationDate = UserDetail.UpcomingNotificationDate.AddMonths(1);
                        }
                    }
                    else
                    {
                        UserDetail.UpcomingNotificationDate = DateTime.Now.Date;
                    }




                }
                SetPropertyChanged(nameof(UpdatedAt));
                SetPropertyChanged(nameof(UserDetail));
            }
        }
        
    }
    public class Connection360InputModel
    {
        public string ConnectionID { get; set; }
        public string ConnectionType { get; set; }
        public string NotificationType { get; set; }
    }

    public class RemoveConnection360Model 
    {
        public int ID { get; set; }
        public int InviteUser { get; set; }
    }

    public class ConnectionShout
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public string FullName { get; set; }
        public string NetworkID { get; set; }
        public string StrengthID { get; set; }
        public string Comment { get; set; }
        public string CreatedAt { get; set; }
        public DateTime DisplayDateTime
        { 
            get
            {
                if (!string.IsNullOrEmpty(CreatedAt))
                {
                    DateTime dateTime = DateTime.ParseExact(CreatedAt, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    return dateTime;
                }
                else
                {
                    return DateTime.Now.Date;
                }
            }
        }
        public string UpdatedAt { get; set; }
        public string StrengthName { get; set; }
    }
}
