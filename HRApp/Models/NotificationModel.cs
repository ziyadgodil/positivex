﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRApp.Models
{
    public class NotificationModel : BaseModel
    {
        public int NotificationID { get; set; }
        public int? ShoutID { get; set; }
        public string StrengthName { get; set;}
        public string ShoutComment { get; set; }
        public string Type { get; set; }

        public string NotificationCreatedAt { get; set; }
        public string UserID { get; set; }
        public string FullName { get; set; }

        string _detail { get; set; }
        public string Detail 
        {
            get { return _detail; }
            set 
            {
                SetPropertyChanged(nameof(Detail));
            }
        }

        string _profile { get; set; }
        public string Profile
        {
            get { return _profile; }
            set 
            {
                if (string.IsNullOrEmpty(value))
                {
                    _profile = "DefaultProfile";
                }
                else
                {
                    _profile = value;
                }
                SetPropertyChanged(nameof(Profile));
            }
        }

        public DateTime NotificationDate
        {
            get
            {
                if (!string.IsNullOrEmpty(NotificationCreatedAt))
                {
                    DateTime dateTime = DateTime.ParseExact(NotificationCreatedAt, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    dateTime = dateTime.ToLocalTime().AddHours(-1);
                    return dateTime;
                }
                else
                {
                    return DateTime.Now.Date;
                }
            }
        }
    }
}
