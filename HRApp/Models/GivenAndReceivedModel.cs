﻿using System;
using System.Collections.ObjectModel;

namespace HRApp.Models
{
    public class GivenAndReceivedModel
    {
        public ObservableCollection<GivenAndReceivedStrengthsDetail> given { get; set; }
        public ObservableCollection<GivenAndReceivedStrengthsDetail> received { get; set; }
    }
    public class GivenAndReceivedStrengthsDetail
    {
        public StrengthModel StrengthsDetail { get; set; }
        public UserModel UserDetail { get; set; }
        public string Comment { get; set; }
        public string CreatedAt { get; set; }

        public DateTime ShoutDate
        {
            get
            {
                if (!string.IsNullOrEmpty(CreatedAt))
                {
                    DateTime dateTime = DateTime.ParseExact(CreatedAt, "yyyy-M-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    return dateTime.ToLocalTime();
                }
                else
                {
                    return DateTime.Now.Date;
                }
            }
        }
    }
    
}
