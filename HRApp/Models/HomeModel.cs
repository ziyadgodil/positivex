﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace HRApp.Models
{
    public class HomeModel :BaseModel
    {
        [JsonProperty("days")]
        public int Days { get; set; }

        [JsonProperty("givenStrengths")]
        public int ShoutoutGiven { get; set; }

        [JsonProperty("receivedStrengths")]
        public int ShoutoutReceived { get; set; }

        [JsonProperty("currentMonthGivenStrengths")]
        public double CurrentMonthGivenShouts { get; set; }

        [JsonProperty("currentMonthAvgGivenStrengths")]
        public double CurrentMonthAvgGivenShouts { get; set; }

        [JsonProperty ("currentMonthMaxGivenStrengths")]
        public double CurrentMonthMaxGivenShouts { get; set; }

        [JsonProperty("currentMonthReceivedStrengths")]
        public double CurrentMonthReceivedShouts { get; set; }

        [JsonProperty("currentMonthAvgReceivedStrengths")]
        public double CurrentMonthAvgReceivedShouts { get; set; }

        [JsonProperty("currentMonthMaxReceivedStrengths")]
        public double CurrentMonthMaxReceivedShouts { get; set; }


        ObservableCollection<StrengthModel> _strengths { get; set; }
        [JsonProperty("Srengths")]
        public ObservableCollection<StrengthModel> Strengths { get { return _strengths; } set { _strengths = value; SetPropertyChanged(nameof(Strengths)); } }

        [JsonProperty("Activity")]
        public ObservableCollection<Activity> Activities { get; set; }

        [JsonProperty("Notification")]
        public ObservableCollection<NotificationModel> NotificationList { get; set; }

        public string ShoutsGivenColor 
        {
            get 
            {
                if (CurrentMonthGivenShouts > 0 && CurrentMonthAvgGivenShouts > 0)
                {
                    if (CurrentMonthGivenShouts < CurrentMonthAvgGivenShouts)
                    {
                        return "Red";
                    }
                    else
                    {
                        return "Green";
                    }
                }
                else 
                {
                    return "Red";
                }
            }
        }

        public string ShoutsReceivedColor
        {
            get
            {
                if (CurrentMonthReceivedShouts > 0 && CurrentMonthAvgReceivedShouts > 0)
                {
                    if (CurrentMonthReceivedShouts < CurrentMonthAvgReceivedShouts)
                    {
                        return "Red";
                    }
                    else
                    {
                        return "Green";
                    }
                }
                else 
                {
                    return "Red";
                }
            }
        }
    }

    public class Activity
    {
        public string text { get; set; }
        public string ActivityText { get; set; }
        public string ActivityDate { get; set; } = "2020-02-07 13:59:04"; 
        public string strengthName { get; set; }
        public string shoutComment { get; set; }
        public DateTime ActivityLocalDate 
        {
            get 
            {
                if (!string.IsNullOrEmpty(ActivityDate))
                {
                    DateTime dateTime = DateTime.ParseExact(ActivityDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    dateTime = dateTime.ToLocalTime().AddHours(-1);
                    return dateTime;
                }
                else
                {
                    return DateTime.Now.Date;
                }
            }
        }

        public string DisplayText 
        {
            get 
            {
                return ActivityText + ActivityLocalDate.ToString("MM-dd-yyyy");
            }
        }
    }
}
