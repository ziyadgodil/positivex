﻿using System;
using System.Collections.ObjectModel;

namespace HRApp.Models
{
    public class UserModel : BaseModel
    {
        public string UserID { get; set; }

        public string CoachID { get; set; }

        string _fullName { get; set; }
        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; SetPropertyChanged(nameof(FullName)); }
        }

        string _email { get; set; }
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                if (string.IsNullOrEmpty(FullName))
                {
                    FullName = value;
                }
                SetPropertyChanged(nameof(FullName));
                SetPropertyChanged(nameof(Email));
            }
        }

        string _profile { get; set; }
        public string Profile
        {
            get { return _profile; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _profile = "DefaultProfile";
                }
                else
                {
                    _profile = value;
                }
                SetPropertyChanged(nameof(Profile));
            }
        }   

        string _best { get; set; }
        public string Best { get { return _best; } set { _best = value; SetPropertyChanged(nameof(Best)); } }

        string _worst { get; set; }
        public string Worst { get { return _worst; } set { _worst = value; SetPropertyChanged(nameof(Worst)); } }

        string _detail { get; set; }
        public string Detail { get { return _detail; } set { _detail = value; SetPropertyChanged(nameof(Detail)); } }

        string _consistently { get; set; }
        public string Consistently { get { return _consistently; } set { _consistently = value; SetPropertyChanged(nameof(Consistently)); } }

        ObservableCollection<StrengthModel> _strengths { get; set; }
        public ObservableCollection<StrengthModel> Strengths { get { return _strengths; } set { _strengths = value; SetPropertyChanged(nameof(Strengths)); } }

        ObservableCollection<string> _strengthDetail { get; set; }
        public ObservableCollection<string> StrengthDetail { get { return _strengthDetail; } set { _strengthDetail = value; SetPropertyChanged(nameof(StrengthDetail)); } }

        //public ObservableCollection<StrengthModel> Strengths { get {
        //        if (_strengths == null)
        //            return new ObservableCollection<StrengthModel>();
        //        else
        //            return _strengths;
        //    } set { _strengths = value; SetPropertyChanged(nameof(Strengths)); } }

        public string UserType { get; set; }

        public string IsDelete { get; set; }

        bool _isCheckedUser { get; set; }
        public bool IsCheckedUser { get { return _isCheckedUser; } set { _isCheckedUser = value; SetPropertyChanged(nameof(IsCheckedUser)); } }

        bool _isUserAvailable { get; set; }
        public bool IsUserAvailable { get { return _isUserAvailable; } set { _isUserAvailable = value; SetPropertyChanged(nameof(IsUserAvailable)); } }

        private bool _isshoutvisible { get; set; } = true;
        public bool IsShoutVisible
        {
            get { return _isshoutvisible; } set { _isshoutvisible = value; SetPropertyChanged(nameof(IsShoutVisible)); }
        }

        string _notificationtype { get; set; } = "Weekly";
        public string NotificationType
        {
            get { return _notificationtype; }
            set 
            { 
                _notificationtype = value; 
                SetPropertyChanged(nameof(NotificationType));
                if (!string.IsNullOrEmpty(value))
                    SetPropertyChanged(nameof(UpcomingNotificationDate));
            }
        }

        public int NotificationID { get; set; }

        int _connection360id { get; set; }
        public int Connection360ID  { get { return _connection360id; } set { _connection360id = value; SetPropertyChanged(nameof(Connection360ID)); }}

        private int _inviteuser { get; set; }
        public int InviteUser 
        {
            get { return _inviteuser; } set { _inviteuser = value; SetPropertyChanged(nameof(InviteUser)); }
        }


        private string _nextNotificationDate { get; set; }
        //public string NextNotificationDate
        //{
        //    get { return _nextNotificationDate; }
        //    set { _nextNotificationDate = value;

        //        SetPropertyChanged(nameof(NextNotificationDate));

        //        if (!string.IsNullOrEmpty(value))
        //        {
        //            UpcomingNotificationDate = DateTime.ParseExact(NextNotificationDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        //            if (NotificationType == "Weekly")
        //            {
        //                UpcomingNotificationDate = UpcomingNotificationDate.AddDays(07);
        //            }
        //            else if (NotificationType == "Bi-Weekly")
        //            {
        //                UpcomingNotificationDate = UpcomingNotificationDate.AddDays(15);
        //            }
        //            else if (NotificationType == "Monthly")
        //            {
        //                UpcomingNotificationDate = UpcomingNotificationDate.AddMonths(1);
        //            }
        //        }
        //        else
        //        {
        //            UpcomingNotificationDate = DateTime.Now.Date;
        //        }
        //    }
        //}

        private DateTime _upcomingNotificationDate { get; set; }
        public DateTime UpcomingNotificationDate
        {
            get
            {
                return _upcomingNotificationDate;
            }
            set
            {
                _upcomingNotificationDate = value;
                SetPropertyChanged(nameof(UpcomingNotificationDate));

            }
        }

        public string Document { get; set; }

        string _documentVisible { get; set; }
        public string DocumentVisible 
        {
            get { return _documentVisible; }
            set { _documentVisible = value;SetPropertyChanged(nameof(DocumentVisible)); SetPropertyChanged(nameof(IsVisibletoUserSource)); } 
        }

        public string IsVisibletoUserSource
        {
            get 
            {
                if (!string.IsNullOrEmpty(DocumentVisible))
                {
                    if (DocumentVisible == "1")
                        return "Check2";
                    else
                        return "Uncheck";
                }
                else 
                {
                    return "Uncheck";
                }
            }
        }
    }

    public class LoginInputModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceType { get; set; }
        public string DeviceToken { get; set; }
    }

    public class UpdateUserInputModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class GetUserInputModel
    {
        public string UserID { get; set; }
    }

    public class UserNetworkInputModel
    {
        public string NetworkID { get; set; }
    }
    public class UserNotificationDeleteInput
    {
        public int ID { get; set; }
    }

}
