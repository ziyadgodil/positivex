﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;


namespace HRApp.Models
{
    public class InviteUserModel : BaseModel
    {

    }

    public class InviteUserInputModel
    {
        public string Email { get; set; }
        public string Type { get; set; }
        public string NotificationType { get; set; }
    }
}
