﻿using System;
namespace HRApp.Models
{
    public class StrengthModel : BaseModel
    {
        public int DomainID { get; set; }
        public string DomainName { get; set; }
        public int StrengthID { get; set; }

        int _strengthOrder { get; set; }
        public int StrengthOrder { get { return _strengthOrder; } set { _strengthOrder = value; SetPropertyChanged(nameof(StrengthOrder)); } }

        public string StrengthName { get; set; }
        public string StrengthDetail { get; set; }

        bool _strengthSelected { get; set; }
        public bool StrengthSelected { get { return _strengthSelected; } set { _strengthSelected = value; SetPropertyChanged(nameof(StrengthSelected)); } }

        string _strengthComment { get; set; }
        public string StrengthComment { get { return _strengthComment; } set { _strengthComment = value; SetPropertyChanged(nameof(StrengthComment)); } }
    }
}
