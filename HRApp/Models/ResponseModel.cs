﻿using System;
using System.Collections.ObjectModel;

namespace HRApp.Models
{
    public class CommonResponse
    {
        public bool status { get; set; }
        public string msg { get; set; }

    }
    public class ResponseModel<T>
    {
        public ResponseModel()
        {
            items = new ObservableCollection<T>();
        }
        public ObservableCollection<T> items { get; set; }
    }

    public class ListResponseModel<T> : CommonResponse
    {
        public ListResponseModel()
        {
            data = new ResponseModel<T>();
        }
        public ResponseModel<T> data { get; set; }
    }

    public class NullListResponseModel<T> : CommonResponse
    {
        public NullListResponseModel()
        {
            data = new ObservableCollection<T>();
        }
        public ObservableCollection<T> data { get; set; }
    }

    public class SimpleResponse<T> : CommonResponse
    {
        public T data { get; set; }
    }
}
