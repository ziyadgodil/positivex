﻿using System;
using Akavache;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Services;
using HRApp.Views;
using HRApp.Views.MasterMenu;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HRApp
{
    public partial class App : Application
    {
        static App _instance;

        public static App Instance { get { return _instance; } }
        public static ServiceManager HRServiceManager { get; private set; }
        public static DatabaseManager HRLocalDataManager { get; private set; }
        
        public static UserModel CurrentUser { get; set; }
        
        //for FCM Token
        public static string FCMToken { get; set; }

        public App()
        {
            _instance = this;
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(ServiceConfiguration.SyncfusionLicensingKey);

            InitializeComponent();
            HRServiceManager = new ServiceManager(new RestService());
            HRLocalDataManager = new DatabaseManager(new LocalDatabase());
            //MainPage = new LoginPage();
            
            if (string.IsNullOrEmpty(Settings.UserToken))
            {
                MainPage = new LoginPage();
            }
            else
            {
                LoadData();
                MainPage = new SliderMenuPage();
            }





            //MainPage = new MainPage();
        }


        public async void LoadData()
        {
            var data = await App.HRLocalDataManager.GetAllStrengths();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
