﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using HRApp.Helpers.BaseData;
using HRApp.Models;

namespace HRApp.Services
{
    public class DatabaseManager
    {
        ILocalDatabase _localDatabase;
        public DatabaseManager(ILocalDatabase localDatabase)
        {
            _localDatabase = localDatabase;
        }

        #region ==> Database Table Key
        public static string UserDataKey = "UserDataKey";
        public static string StrengthsKey = "StrengthsKey";
        public static string GivenStrengthsKey = "GivenStrengthsKey";
        public static string ReceivedStrengthsKey = "ReceivedStrengthsKey";
        public static string UserNetworksKey = "UserNetworksKey";

        #endregion

        #region ==> UserData Methods

        public async Task<bool> SaveUserData(UserModel userModel)
        {
            try
            {
                await _localDatabase.InsertObjectAsync(UserDataKey, userModel, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<UserModel> GetUserData()
        {
            UserModel result = new UserModel();
            try
            {
                var UserData = await _localDatabase.GetObjectAsync<UserModel>(UserDataKey);
                if (UserData != null)
                    result = UserData;
                else
                    result = null;
            }
            catch
            {
                result = null;
            }
            return result;
        }

        public async Task<ObservableCollection<StrengthModel>> GetUserAllStrengths()
        {
            ObservableCollection<StrengthModel> result = new ObservableCollection<StrengthModel>();
            try
            {
                var UserData = await _localDatabase.GetObjectAsync<UserModel>(UserDataKey);
                if (UserData != null)
                    result = UserData.Strengths;
                else
                    result = null;
            }
            catch
            {
                result = null;
            }
            return result;
        }


        #endregion

        #region ==> Strengths Methods
        public async Task<bool> SaveStrengthsData(ObservableCollection<StrengthModel> Strengths)
        {
            try
            {
                await _localDatabase.InsertObjectAsync(StrengthsKey, Strengths, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<ObservableCollection<StrengthModel>> GetAllStrengths()
        {
            ObservableCollection<StrengthModel> result;
            try
            {
                return await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                //var AllStrengthsData = await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                //return new ObservableCollection<StrengthModel>(AllStrengthsData.OrderByDescending(x => x.TitleDate));

            }
            catch
            {
                result = new ObservableCollection<StrengthModel>();
            }
            return result = new ObservableCollection<StrengthModel>();
        }

        public async Task<StrengthModel> GetStrengthDetailById(int id)
        {
            StrengthModel result;
            try
            {
                var lstStrengths = await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                return lstStrengths.FirstOrDefault(x => x.StrengthID == id);

            }
            catch
            {
                result = new StrengthModel();
            }
            return result = new StrengthModel();
        }

        #endregion

        #region ==> Given Strengths Methods
        public async Task<bool> SaveGivenStrengthsData(ObservableCollection<GivenAndReceivedStrengthsDetail> StrengthsDetails)
        {
            try
            {
                await _localDatabase.InsertObjectAsync(GivenStrengthsKey, StrengthsDetails, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<ObservableCollection<GivenAndReceivedStrengthsDetail>> GetAllGivenStrengths()
        {
            ObservableCollection<GivenAndReceivedStrengthsDetail> result;
            try
            {
                return await _localDatabase.GetObjectAsync<ObservableCollection<GivenAndReceivedStrengthsDetail>>(GivenStrengthsKey);
                //var AllStrengthsData = await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                //return new ObservableCollection<StrengthModel>(AllStrengthsData.OrderByDescending(x => x.TitleDate));

            }
            catch
            {
                result = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
            }
            return result = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
        }

        #endregion

        #region ==> Received Strengths Methods

        public async Task<bool> SaveReceivedStrengthsData(ObservableCollection<GivenAndReceivedStrengthsDetail> StrengthsDetails)
        {
            try
            {
                await _localDatabase.InsertObjectAsync(ReceivedStrengthsKey, StrengthsDetails, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<ObservableCollection<GivenAndReceivedStrengthsDetail>> GetAllReceivedStrengths()
        {
            ObservableCollection<GivenAndReceivedStrengthsDetail> result;
            try
            {
                return await _localDatabase.GetObjectAsync<ObservableCollection<GivenAndReceivedStrengthsDetail>>(ReceivedStrengthsKey);
                //var AllStrengthsData = await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                //return new ObservableCollection<StrengthModel>(AllStrengthsData.OrderByDescending(x => x.TitleDate));

            }
            catch
            {
                result = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
            }
            return result = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
        }

        public async Task<ObservableCollection<GivenAndReceivedStrengthsDetail>> GetAllGivenAndReceivedStrengths()
        {
            ObservableCollection<GivenAndReceivedStrengthsDetail> result;
            try
            {
                var received  = await _localDatabase.GetObjectAsync<ObservableCollection<GivenAndReceivedStrengthsDetail>>(ReceivedStrengthsKey);
                var given = await _localDatabase.GetObjectAsync<ObservableCollection<GivenAndReceivedStrengthsDetail>>(GivenStrengthsKey);
                return new ObservableCollection<GivenAndReceivedStrengthsDetail>(received.Concat(given));
                //var AllStrengthsData = await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                //return new ObservableCollection<StrengthModel>(AllStrengthsData.OrderByDescending(x => x.TitleDate));

            }
            catch
            {
                result = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
            }
            return result = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
        }


        

        #endregion

        #region ==> Network Users Methods

        public async Task<bool> SaveNetworkUsers(ObservableCollection<UserModel> userModels)
        {
            try
            {
                await _localDatabase.InsertObjectAsync(UserNetworksKey, userModels, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<ObservableCollection<UserModel>> GetAllNetworkUsers()
        {
            //ObservableCollection<UserModel> result;
            try
            {
                var allNetworkUser = await _localDatabase.GetObjectAsync<ObservableCollection<UserModel>>(UserNetworksKey);
                return new ObservableCollection<UserModel>(allNetworkUser.Where(x => x != null && x.UserID != Settings.UserToken));
                //var AllStrengthsData = await _localDatabase.GetObjectAsync<ObservableCollection<StrengthModel>>(StrengthsKey);
                //return new ObservableCollection<StrengthModel>(AllStrengthsData.OrderByDescending(x => x.TitleDate));

            }
            catch
            {
                return new ObservableCollection<UserModel>();
            }
        }
        #endregion
    }
}
