﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using HRApp.Models;
using Plugin.FilePicker.Abstractions;
using Plugin.Media.Abstractions;

namespace HRApp.Services
{
    public class ServiceManager
    {
        IRestService restService;
        public ServiceManager(IRestService service)
        {
            restService = service;
        }


        public Task<SimpleResponse<UserModel>> LoginUser(LoginInputModel loginInputModel)
        {
            return restService.LoginUser(loginInputModel);
        }

        public Task<ListResponseModel<UserModel>> GetAllNetworkUsers()
        {
            return restService.GetAllNetworkUser();
        }

        public Task<NullListResponseModel<ShoutModel>> AddShout(ShoutInputModel shoutInputModel)
        {
            return restService.SendShout(shoutInputModel);
        }

        public Task<SimpleResponse<UserModel>> UpdateProfile(UpdateUserInputModel updateUserInputModel)
        {
            return restService.UpdateUserProfile(updateUserInputModel);
        }

        public Task<SimpleResponse<HomeModel>> SetHomeDashboard()
        {
            return restService.HomeDashboard();
        }

        public Task<ListResponseModel<StrengthModel>> GetAllStrengths()
        {
            return restService.GetAllStrengths();
        }

        public Task<SimpleResponse<GivenAndReceivedModel>> GetUserAllGivenAndReceivedStrengths()
        {
            return restService.GetUserAllGivenAndReceivedStrength();
        }
        public Task<SimpleResponse<GivenAndReceivedModel>> GetOtherUserAllGivenAndReceivedStrengths(string UserId)
        {
            return restService.GetOtherUserAllGivenAndReceivedStrength(UserId);
        }

        public Task<ListResponseModel<AimModel>> GetUsersAllAims()
        {
            return restService.GetAllAims();
        }
        public Task<NullListResponseModel<AimModel>> SaveAIM(AimModel aimModel)
        {
            return restService.SaveAIM(aimModel);
        }
        public Task<SimpleResponse<UserModel>> GetUserByID(string Id)
        {
            var getUserInputModel = new GetUserInputModel();
            getUserInputModel.UserID = Id;
            return restService.GetuserByID(getUserInputModel);
        }


        public Task<NullListResponseModel<Connection360Model>> GetAllUsersConnection()
        {
            return restService.GetAllUsersConnection();
        }

        public Task<NullListResponseModel<UserModel>> GetAllUsers()
        {
            return restService.GetAllUsers();
        }

        public Task<SimpleResponse<Connection360Model>> AddUserConnection(Connection360InputModel connectionInputModel)
        {
            return restService.AddUserConnection(connectionInputModel);
        }

        public Task<NullListResponseModel<UserModel>> AddUserNetwork(UserNetworkInputModel userNetworkInputModel)
        {
            return restService.AddUserNetwork(userNetworkInputModel);
        }

        public Task<NullListResponseModel<bool>> RemoveAim(RemoveAimInputModel removeAimInputModel)
        {
            return restService.RemoveAim(removeAimInputModel);
        }

        public Task<SimpleResponse<UserModel>> UpdateUserImage(MediaFile mediaFile)
        {
            return restService.UpdateUserImage(mediaFile);
        }

        public Task<NullListResponseModel<bool>> InviteUser(InviteUserInputModel inviteUserInputModel)
        {
            return restService.InviteUser(inviteUserInputModel);
        }

        public Task<NullListResponseModel<bool>> RemoveNotification(UserNotificationDeleteInput userNotificationDeleteInput)
        {
            return restService.RemoveNotification(userNotificationDeleteInput);
        }

        public Task<NullListResponseModel<bool>> RemoveConnection360(RemoveConnection360Model RemoveConnection) 
        {
            return restService.RemoveConnection360(RemoveConnection);
        }

        public Task<SimpleResponse<UserModel>> UpdateUserDocument(FileData mediaFile) 
        {
            return restService.UpdateUserDocument(mediaFile);
        }

        public Task<NullListResponseModel<UserModel>> RemoveUserfromNetwork(UserNetworkInputModel userNetworkInputModel) 
        {
            return restService.RemoveUserfromNetwork(userNetworkInputModel);
        }

        public Task<SimpleResponse<bool>> ShareWithManager(ShareWithManagerInputModel inputModel) 
        {
            return restService.ShareWithManager(inputModel);
        }
    }
}
