﻿using System;
namespace HRApp.Services
{
    public static class ServiceConfiguration
    {
        //public static string BaseURL = "http://ongymand.com/positivx_api/";
        public static string BaseURL = "https://positivx.com/app/";

        public static string LoginURL = "login.php";
        public static string UserNetworkURL = "getUserNetworks.php";
        public static string AddShoutURL = "addShout.php";
        public static string UserUpdateURL = "userUpdateProfile.php";
        public static string GetAllStrengthsURL = "getAllStrengths.php";
        public static string HomeDashboardURL = "home.php";
        public static string GetUserAllGivenAndReceivedStrengthsURL = "UserAllGivenAndReceivedStrengths.php";
        public static string GetAllAimsURL = "getAllUserAim.php";
        public static string AddEditAIMURL = "aim.php";
        public static string GetUserByIdURL = "getUserDetailById.php";

        public static string GetAllUserConnectionURL = "getUserConnection.php";
        public static string AddUserConnectionURL = "addUserConnection.php";
        public static string GetAllUsersURL = "getAllUsers.php";
        public static string DeleteAimURL = "deleteAim.php";

        public static string AddUserNetworlURL = "AddUserNetwork.php";
        public static string InviteUserURL = "userInvite.php";

        public static string RemoveNotificationURL = "deleteNotification.php";
        public static string RemoveConnection360URL = "deleteConnection.php";

        public static string RemoveNetworkUserURL = "deleteUserNetwork.php";

        public static string ShareWithManagerURl = "shareWithManager.php";

        public static string CommonErrorMessage = "Something went wrong please try again";

        public static string SyncfusionLicensingKey = "MTkwNjkyQDMxMzcyZTM0MmUzMEM4QlNqczVpU3JCSWhTR2FLQmRXcTNKZ0dCZzN3OS94WitDNE1lT3Z3VXc9";
    }
}
