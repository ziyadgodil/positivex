﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HRApp.Models;

namespace HRApp.Services
{
    public interface ILocalDatabase
    {
        //Task<bool> SavePortfolipAsync(PortfolioModel instance);
        //Task<PortfolioModel> GetPortfolioByIdAsync(int id);
        //Task<PortfolioModel> GetPortfolioDataAsync();

        Task InsertObjectAsync<T>(string key, T entity, DateTimeOffset? expiry);
        Task<T> GetObjectAsync<T>(string key);
        Task<IEnumerable<T>> GetAllObjects<T>();
        Task<bool> ClearAllAsync();
        Task<bool> RemoveObjectAsync(string key);

        Task<bool> RemoveAllObjectsOfTypeAsync<T>();

    }
}
