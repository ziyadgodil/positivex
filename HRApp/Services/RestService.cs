﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HRApp;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using Newtonsoft.Json;
using Plugin.FilePicker.Abstractions;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HRApp.Services
{
    public class RestService : IRestService
    {
        HttpClient _client;
        public RestService()
        {
            _client = new HttpClient();
            if(!string.IsNullOrEmpty(Settings.UserToken))
            {
                _client.DefaultRequestHeaders.Add("Token", Settings.UserToken);
            }
        }

        public async Task<SimpleResponse<UserModel>> LoginUser(LoginInputModel loginInputModel)
        {
            _client = new HttpClient();
            var result = new SimpleResponse<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.LoginURL;

                var json = JsonConvert.SerializeObject(loginInputModel);

                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        _client.DefaultRequestHeaders.Add("Token", result.data.UserID);
                        Settings.UserToken = result.data.UserID;
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<HomeModel>> HomeDashboard()
        {
            var result = new SimpleResponse<HomeModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.HomeDashboardURL;
                var response = await _client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<HomeModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch(Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<ListResponseModel<UserModel>> GetAllNetworkUser()
        {
            var result = new ListResponseModel<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.UserNetworkURL;

                var response = await _client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ListResponseModel<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        ObservableCollection<UserModel> users = result.data.items;
                        for (int i = users.Count - 1; i >= 0; i--)
                        {
                            if (users[i] == null)
                                users.RemoveAt(i);
                        }

                        result.data.items = users;
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<ShoutModel>> SendShout(ShoutInputModel shoutInputModel)
        {
            var result = new NullListResponseModel<ShoutModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.AddShoutURL;

                var json = JsonConvert.SerializeObject(shoutInputModel);

                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<ShoutModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        //_client.DefaultRequestHeaders.Add("Token", result.data.UserID);
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<UserModel>> UpdateUserProfile(UpdateUserInputModel updateUserInputModel)
        {
            var result = new SimpleResponse<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.UserUpdateURL;


                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Key", updateUserInputModel.Key),
                    new KeyValuePair<string, string>("Value", updateUserInputModel.Value)
                });
                var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = formContent };
                //var response = await _client.PostAsync(url, formContent);
                var response = await _client.SendAsync(req);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        //_client.DefaultRequestHeaders.Add("Token", result.data.UserID);
                        await App.HRLocalDataManager.SaveUserData(result.data);
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<ListResponseModel<StrengthModel>> GetAllStrengths()
        {
            var result = new ListResponseModel<StrengthModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetAllStrengthsURL;

                var response = await _client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ListResponseModel<StrengthModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<GivenAndReceivedModel>> GetUserAllGivenAndReceivedStrength()
        {
            var result = new SimpleResponse<GivenAndReceivedModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetUserAllGivenAndReceivedStrengthsURL;

                var response = await _client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<GivenAndReceivedModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<GivenAndReceivedModel>> GetOtherUserAllGivenAndReceivedStrength(string UserId)
        {
            var result = new SimpleResponse<GivenAndReceivedModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetUserAllGivenAndReceivedStrengthsURL;

                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Token", UserId);
                var response = await httpClient.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<GivenAndReceivedModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<ListResponseModel<AimModel>> GetAllAims()
        {
            var result = new ListResponseModel<AimModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetAllAimsURL;

                var response = await _client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ListResponseModel<AimModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<AimModel>> SaveAIM(AimModel aimModel)
        {
            var result = new NullListResponseModel<AimModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.AddEditAIMURL;

                var json = JsonConvert.SerializeObject(aimModel);

                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<AimModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        //_client.DefaultRequestHeaders.Add("Token", result.data.UserID);
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<UserModel>> GetuserByID(GetUserInputModel getUserInputModel)
        {
            var result = new SimpleResponse<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetUserByIdURL;

                var json = JsonConvert.SerializeObject(getUserInputModel);

                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<Connection360Model>> AddUserConnection(Connection360InputModel connectionInputModel)
        {
            var result = new SimpleResponse<Connection360Model>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.AddUserConnectionURL;

                var json = JsonConvert.SerializeObject(connectionInputModel);

                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<Connection360Model>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<Connection360Model>> GetAllUsersConnection()
        {
            var result = new NullListResponseModel<Connection360Model>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetAllUserConnectionURL;
                var response = await _client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<Connection360Model>>(contents);
                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<UserModel>> GetAllUsers()
        {
            var result = new NullListResponseModel<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.GetAllUsersURL;

                var response = await _client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<UserModel>> AddUserNetwork(UserNetworkInputModel userNetworkInputModel)
        {
            var result = new NullListResponseModel<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.AddUserNetworlURL;
                var json = JsonConvert.SerializeObject(userNetworkInputModel);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception e)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<bool>> RemoveAim(RemoveAimInputModel removeAimInputModel)
        {
            var result = new NullListResponseModel<bool>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.DeleteAimURL;
                var json = JsonConvert.SerializeObject(removeAimInputModel);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<bool>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<UserModel>> UpdateUserImage(MediaFile mediaFile)
        {
            var result = new SimpleResponse<UserModel>();
            try
            {
                HttpClient httpclient = new HttpClient();
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.UserUpdateURL;
                httpclient.DefaultRequestHeaders.Add("Token", Settings.UserToken);
                var content = new MultipartFormDataContent();
                content.Headers.ContentType.MediaType = "multipart/form-data";
                content.Add(new StringContent("Profile"),"Key");
                content.Add(new StreamContent(mediaFile.GetStream()), "Value", mediaFile.Path);
                var response = await httpclient.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        await App.HRLocalDataManager.SaveUserData(result.data);
                        MessagingCenter.Send(new MessagingCenterModel { }, "ProfileImageUpdate");
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<bool>> InviteUser(InviteUserInputModel inviteUserInputModel)
        {
            var result = new NullListResponseModel<bool>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.InviteUserURL;
                var json = JsonConvert.SerializeObject(inviteUserInputModel);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<bool>>(contents);
                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception Ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<bool>> RemoveNotification(UserNotificationDeleteInput userNotificationDeleteInput)
        {
            var result = new NullListResponseModel<bool>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.RemoveNotificationURL;
                var json = JsonConvert.SerializeObject(userNotificationDeleteInput);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<bool>>(contents);
                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<bool>> RemoveConnection360(RemoveConnection360Model RemoveConnection) 
        {
            var result = new NullListResponseModel<bool>();
            try 
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.RemoveConnection360URL;
                var json = JsonConvert.SerializeObject(RemoveConnection);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<bool>>(contents);
                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception ex) 
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<UserModel>> UpdateUserDocument(FileData mediaFile)
        {
            var result = new SimpleResponse<UserModel>();
            try
            {
                HttpClient httpclient = new HttpClient();
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.UserUpdateURL;
                httpclient.DefaultRequestHeaders.Add("Token", Settings.UserToken);
                var content = new MultipartFormDataContent();
                content.Headers.ContentType.MediaType = "multipart/form-data";
                content.Add(new StringContent("Document"), "Key");
                content.Add(new StreamContent(mediaFile.GetStream()), "Value", mediaFile.FilePath);
                var response = await httpclient.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        await App.HRLocalDataManager.SaveUserData(result.data);
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NullListResponseModel<UserModel>> RemoveUserfromNetwork(UserNetworkInputModel userNetworkInputModel)
        {
            var result = new NullListResponseModel<UserModel>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.RemoveNetworkUserURL;
                var json = JsonConvert.SerializeObject(userNetworkInputModel);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<NullListResponseModel<UserModel>>(contents);

                    if (result != null && result.status && result.data != null)
                    {
                        return result;
                    }
                    else if (!string.IsNullOrEmpty(result.msg))
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", result.msg, "OK");
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
            }
            catch (Exception e)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SimpleResponse<bool>> ShareWithManager(ShareWithManagerInputModel inputModel)
        {
            var result = new SimpleResponse<bool>();
            try
            {
                var url = ServiceConfiguration.BaseURL + ServiceConfiguration.ShareWithManagerURl;
                var json = JsonConvert.SerializeObject(inputModel);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var contents = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<SimpleResponse<bool>>(contents);
                    if (result != null && result.status)
                    {
                        return result;
                    }
                    else if (result != null && !result.status)
                    {
                        result.status = false;
                        result.msg = result.msg;
                        return result;
                    }
                    else
                    {
                        result.status = false;
                        result.msg = ServiceConfiguration.CommonErrorMessage;
                        return result;
                    }
                }
                else 
                {
                    result.status = false;
                    result.msg = ServiceConfiguration.CommonErrorMessage;
                    return result;
                }
                   
            }
            catch (Exception ex)
            {
                result.status = false;
                result.msg = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        } 
    }

}
