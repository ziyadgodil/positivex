﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Akavache;
using Akavache.Sqlite3;
using HRApp.Models;

namespace HRApp.Services
{
    //public static class LinkerPreserve
    //{
    //    static LinkerPreserve()
    //    {
    //        var persistentName = typeof(SQLitePersistentBlobCache).FullName;
    //        var encryptedName = typeof(SQLiteEncryptedBlobCache).FullName;
    //    }
    //}

    public class LocalDatabase : ILocalDatabase
    {
        static IBlobCache _dbInstance => BlobCache.UserAccount;

        public LocalDatabase()
        {
            try
            {
                BlobCache.ApplicationName = "HRApp";
                BlobCache.EnsureInitialized();
            }
            catch (Exception ex)
            {
            }
        }

        public async Task InsertObjectAsync<T>(string key, T entity, DateTimeOffset? expiry)
        {
            try
            {
                DateTime startDate = DateTime.Now; 
                expiry = startDate.AddDays(30); 

                await _dbInstance.InsertObject(key, entity, expiry);
            }
            catch (Exception ex)
            {
                ShowDebugErrorMessage(ex, entity);
            }
        }

        public async Task<T> GetObjectAsync<T>(string key)
        {
            var entity = default(T);

            try
            {
                entity = await _dbInstance.GetObject<T>(key);
            }
            catch (Exception ex)
            {
                ShowDebugErrorMessage(ex, entity);
            }

            return entity;
        }

        public async Task<IEnumerable<T>> GetAllObjects<T>()
        {
            var result = default(IEnumerable<T>);

            try
            {
                result = await _dbInstance.GetAllObjects<T>();
            }
            catch (Exception ex)
            {
                ShowDebugErrorMessage(ex, result);
            }

            return result;
        }

        public async Task<bool> ClearAllAsync()
        {
            try
            {
                await _dbInstance.InvalidateAll();
                return true;
            }
            catch (Exception ex)
            {
                ShowDebugErrorMessage(ex, null);
                return false;
            }
        }

        public async Task<bool> RemoveObjectAsync(string key)
        {
            try
            {
                await _dbInstance.Invalidate(key);
                return true;
            }
            catch (Exception ex)
            {
                ShowDebugErrorMessage(ex, null);
                return false;
            }
        }

        public async Task<bool> RemoveAllObjectsOfTypeAsync<T>()
        {
            try
            {
                await _dbInstance.InvalidateAllObjects<T>();
                return true;
            }
            catch (Exception ex)
            {
                ShowDebugErrorMessage(ex, null);
                return false;
            }
        }

        void ShowDebugErrorMessage(Exception ex, object entity, [CallerMemberName] string caller = "")
        {
            Debug.WriteLine($"Database error. {caller}.{entity}: {ex.Message}");
        }

        public void Dispose()
        {
            BlobCache.Shutdown().Wait();
        }

    }
}
