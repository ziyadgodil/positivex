﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using HRApp.Models;
using Plugin.FilePicker.Abstractions;
using Plugin.Media.Abstractions;

namespace HRApp.Services
{
    public interface IRestService
    {
        Task<SimpleResponse<UserModel>> LoginUser(LoginInputModel loginInputModel);

        Task<ListResponseModel<UserModel>> GetAllNetworkUser();

        Task<NullListResponseModel<ShoutModel>> SendShout(ShoutInputModel shoutInputModel);

        Task<SimpleResponse<UserModel>> UpdateUserProfile(UpdateUserInputModel updateUserInputModel);

        Task<ListResponseModel<StrengthModel>> GetAllStrengths();

        Task<SimpleResponse<HomeModel>> HomeDashboard();

        Task<SimpleResponse<GivenAndReceivedModel>> GetUserAllGivenAndReceivedStrength();
        Task<SimpleResponse<GivenAndReceivedModel>> GetOtherUserAllGivenAndReceivedStrength(string UserId);

        Task<ListResponseModel<AimModel>> GetAllAims();

        Task<NullListResponseModel<AimModel>> SaveAIM(AimModel aimModel);
        Task<SimpleResponse<UserModel>> GetuserByID(GetUserInputModel getUserInputModel);


        Task<NullListResponseModel<UserModel>> GetAllUsers();
        Task<NullListResponseModel<Connection360Model>> GetAllUsersConnection();
        Task<SimpleResponse<Connection360Model>> AddUserConnection(Connection360InputModel connectionInputModel);

        Task<NullListResponseModel<UserModel>> AddUserNetwork(UserNetworkInputModel userNetworkInputModel);

        Task<NullListResponseModel<bool>> RemoveAim(RemoveAimInputModel removeAimInputModel);

        Task<SimpleResponse<UserModel>> UpdateUserImage(MediaFile mediaFile);

        Task<NullListResponseModel<bool>> InviteUser(InviteUserInputModel inviteUserInputModel);

        Task<NullListResponseModel<bool>> RemoveNotification(UserNotificationDeleteInput userNotificationDeleteInput);

        Task<NullListResponseModel<bool>> RemoveConnection360(RemoveConnection360Model RemoveConnection);

        Task<SimpleResponse<UserModel>> UpdateUserDocument(FileData mediaFile);

        Task<NullListResponseModel<UserModel>> RemoveUserfromNetwork(UserNetworkInputModel userNetworkInputModel);

        Task<SimpleResponse<bool>> ShareWithManager(ShareWithManagerInputModel inputModel);

    }
}
