﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class AIMPageViewModel : BaseViewModel
    {
        bool _isListVisible { get; set; } = true;
        public bool IsListVisible { get { return _isListVisible; } set { _isListVisible = value; SetPropertyChanged(nameof(IsListVisible)); } }

        bool _isEditVisible { get; set; } = false;
        public bool IsEditVisible { get { return _isEditVisible; } set { _isEditVisible = value; SetPropertyChanged(nameof(IsEditVisible)); } }

        bool _isAddVisible { get; set; } = false;
        public bool IsAddVisible { get { return _isAddVisible; } set { _isAddVisible = value; SetPropertyChanged(nameof(IsAddVisible)); } }

        bool _isComesFromEdit { get; set; } = true;
        public bool IsComesFromEdit { get { return _isComesFromEdit; } set { _isComesFromEdit = value; SetPropertyChanged(nameof(IsComesFromEdit)); } }

        AimModel _addEditAimDetail { get; set; }
        public AimModel AddEditAimDetail { get { return _addEditAimDetail; } set { _addEditAimDetail = value; SetPropertyChanged(nameof(AddEditAimDetail)); } }

        ObservableCollection<AimModel> _aimList { get; set; } 
        public ObservableCollection<AimModel> AimList { get { return _aimList; } set { _aimList = value; SetPropertyChanged(nameof(AimList)); } }

        AimDetail _selectedAim { get; set; }
        public AimDetail SelectedAim
        {
            get
            {
                return _selectedAim;
            }
            set
            {
                _selectedAim = value;
                SetPropertyChanged(nameof(SelectedAim));
                SelectAimChanged();
            }
        }

        string _updateActivity { get; set; }
        public string UpdateActivity { get { return _updateActivity; } set { _updateActivity = value; SetPropertyChanged(nameof(UpdateActivity)); } }
        

        ObservableCollection<StrengthModel> _allStrengths { get; set; }
        public ObservableCollection<StrengthModel> AllStrengths { get { return _allStrengths; } set { _allStrengths = value; SetPropertyChanged(nameof(AllStrengths)); } }

        public ICommand OpenEditCommand { get; }
        public ICommand EditAimCommand { get; }
        public ICommand UpdateAimCommand { get; }
        public ICommand CancelUpdateAimCommand { get; }
        public ICommand AddTappedCommand { get; }
        public ICommand SaveAimCommand { get; }
        public ICommand CancelAimCommand { get; }
        public ICommand SelectUnselectStrengthsCommand { get; }
        public ICommand DeleteAimCommand { get; }
        public ICommand ShareWithManagerCommand { get; }


        public AIMPageViewModel()
        {
            LoadData();
            OpenEditCommand = new Command<AimModel>(OpenEdit);
            UpdateAimCommand = new Command(UpdateAim);
            CancelUpdateAimCommand = new Command(CancelUpdateAim);
            AddTappedCommand = new Command(OpenAddAim);
            EditAimCommand = new Command(EditAim);
            SaveAimCommand = new Command(SaveAim);
            CancelAimCommand = new Command(CancelAim);
            SelectUnselectStrengthsCommand = new Command<StrengthModel>(SelectUnselectStrengths);
            DeleteAimCommand = new Command<AimModel>(DeleteAim);
            ShareWithManagerCommand = new Command(ShareAimWithManager);
        }
        public async void LoadData()
        {
            //using (UserDialogs.Instance.Loading("Loading.."))
            //{
                var result = await App.HRServiceManager.GetUsersAllAims();
                if (result.status && result.data != null && result.data.items != null)
                {
                    AimList = result.data.items;
                }

                AllStrengths = await App.HRLocalDataManager.GetUserAllStrengths();
            //}

        }

        
        private async void SaveAim()
        {
            if (!string.IsNullOrEmpty(AddEditAimDetail.AimTitle))
            {
                using (UserDialogs.Instance.Loading("Saving.."))
                {
                    AddEditAimDetail.UserID = Settings.UserToken;
                    AddEditAimDetail.IsShareWithManager = "No";
                    if(AddEditAimDetail.AimActivity == null)
                        AddEditAimDetail.AimActivity = new ObservableCollection<AimActivity>();

                    var result = await App.HRServiceManager.SaveAIM(AddEditAimDetail);
                    if (result.status && result.data != null)
                    {
                        LoadData();
                        IsAddVisible = false;
                        IsListVisible = true;
                    }
                }
            }
            else
            {
                //await App.Instance.MainPage.DisplayAlert("Alert", "Please enter AIM title.","OK");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Please enter AIM title.", false));
            }
        }
        private async void CancelAim()
        {
            LoadData();
            IsAddVisible = false;
            IsListVisible = true;
        }

        private async void SelectUnselectStrengths(StrengthModel strengthModel)
        {
            
            if (AddEditAimDetail.AimDetail == null)
                AddEditAimDetail.AimDetail = new ObservableCollection<AimDetail>();

            if (!strengthModel.StrengthSelected)
            {
                var aimDetail = new AimDetail();
                aimDetail.StrengthId = strengthModel.StrengthID.ToString();
                aimDetail.StrengthName = strengthModel.StrengthName;
                SelectAimStrengthsPopup selectAimStrengthsPopup = new SelectAimStrengthsPopup(aimDetail);

                selectAimStrengthsPopup.SelectedStrength += async (object sender1, AimDetail ActionPlan) =>
                {
                    await App.Instance.MainPage.Navigation.PopAllPopupAsync();
                    if (ActionPlan != null)
                    {
                        AddEditAimDetail.AimDetail.Insert(0,ActionPlan);
                        strengthModel.StrengthSelected = true;

                    }
                };

                await App.Instance.MainPage.Navigation.PushPopupAsync(selectAimStrengthsPopup);
            }
            else
            {
                strengthModel.StrengthSelected = false;
                AddEditAimDetail.AimDetail.Remove(AddEditAimDetail.AimDetail.FirstOrDefault(x=>x.StrengthId == strengthModel.StrengthID.ToString()));
            }
        }


        private async void OpenAddAim()
        {
            AddEditAimDetail = new AimModel();
            IsAddVisible = true;
            IsEditVisible = false;
            IsListVisible = false;
        }

        private async void UpdateAim()
        {
            IsEditVisible = false;
            IsListVisible = true;
            if (AddEditAimDetail.AimActivity == null)
                AddEditAimDetail.AimActivity = new ObservableCollection<AimActivity>();
            if(!string.IsNullOrEmpty(UpdateActivity))
                AddEditAimDetail.AimActivity.Add(new AimActivity() { Text = UpdateActivity, TextDate = DateTime.Now.ToString("M/d/yyyy h:mm:ss tt") }) ;
            UpdateActivity = "";
            SaveAim();
            //EditAimDetail = aimModel;
        }

        private async void CancelUpdateAim()
        {
            IsEditVisible = false;
            IsListVisible = true;
            UpdateActivity = "";
        }

        private async void OpenEdit(AimModel aimModel)
        {
            IsEditVisible = true;
            IsListVisible = false;
            AddEditAimDetail = aimModel;
            AddEditAimDetail.AimActivity = new ObservableCollection<AimActivity>(AddEditAimDetail.AimActivity.OrderByDescending(s => s.DisplayDate));
            foreach (var item in AllStrengths)
            {
                if (aimModel.AimDetail.Any(x => x.StrengthId == item.StrengthID.ToString()))
                {
                    item.StrengthSelected = true;
                }
            }
        }

        private async void EditAim()
        {
            IsEditVisible = false;
            IsComesFromEdit = true;
            IsAddVisible = true;
        }

        private async void SelectAimChanged()
        {
            //await App.Instance.MainPage.DisplayAlert(SelectedAim.StrengthName,SelectedAim.ActionPlan,"Ok");
            await PopupNavigation.Instance.PushAsync(new AlertPopup(SelectedAim.StrengthName, SelectedAim.ActionPlan, false));
            SelectedAim = null;
        }

        private async void DeleteAim(AimModel aimModel)
        {
            RemoveAimInputModel removeAimInputModel = new RemoveAimInputModel();
            //var confirm = await App.Instance.MainPage.DisplayAlert("Alert", "Are you sure you want to delete this Aim?", "Ok", "Cancel");
            AlertPopup alert = new AlertPopup("Alert", "Are you sure you want to delete this Aim?", true);
            await PopupNavigation.Instance.PushAsync(alert);
            alert.AcceptAlertPopup += async (object sender1, bool confirm) => 
            {
                if (confirm)
                {
                    using (UserDialogs.Instance.Loading("Deleting.."))
                    {
                        removeAimInputModel.ID = aimModel.ID;
                        var res = await App.HRServiceManager.RemoveAim(removeAimInputModel);
                        if (res.status)
                        {
                            //await App.Instance.MainPage.DisplayAlert("Result", "Aim Deleted", "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "Aim Deleted", false));
                            AimList.Remove(aimModel);
                        }
                        else
                        {
                            //await App.Instance.MainPage.DisplayAlert("Error", res.msg, "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", res.msg, false));
                        }
                    }
                }
            }; 
        }

        private async void ShareAimWithManager() 
        {
            AimNotePopup aimNotePopup = new AimNotePopup();
            aimNotePopup.AimID = AddEditAimDetail.ID;
            await PopupNavigation.Instance.PushAsync(aimNotePopup);
        }
    }
}
