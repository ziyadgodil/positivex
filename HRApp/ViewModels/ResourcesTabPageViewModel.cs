﻿using HRApp.Helpers.BaseData;
using System;
using Acr.UserDialogs;
using HRApp.Models;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using Rg.Plugins.Popup.Services;
using HRApp.Views.PopupViews;

namespace HRApp.ViewModels
{
    public class ResourcesTabPageViewModel : BaseViewModel
    {
        private ObservableCollection<StrengthModel> _strengthdata { get; set; }

        public ObservableCollection<StrengthModel> StrengthData
        {
            get { return _strengthdata; }
            set { _strengthdata = value; SetPropertyChanged(nameof(StrengthData)); }
        }

        private ObservableCollection<StrengthModel> _filteredList { get; set; }

        public ObservableCollection<StrengthModel> FilteredList
        {
            get { return _filteredList; }
            set { _filteredList = value; SetPropertyChanged(nameof(FilteredList)); }
        }

        private string _selectedTab { get; set; } = "0";

        public string SelectedTab
        {
            get { return _selectedTab; }
            set { _selectedTab = value; SetPropertyChanged(nameof(SelectedTab)); }
        }

        string _searchText { get; set; } = "";
        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                FilterData(SelectedTab, SearchText);
            }
        }

        public ICommand TabChangedCommand { get; }
        public ICommand TappedResourceCommand { get; }

        public ResourcesTabPageViewModel()
        {
            LoadData();
            TabChangedCommand = new Command<string>(TabChanged);
            TappedResourceCommand = new Command<StrengthModel>(TappedResource);
        }
        public async void TappedResource(StrengthModel strengthModel)
        {
            //App.Instance.MainPage.DisplayAlert(strengthModel.StrengthName, strengthModel.StrengthDetail, "Ok");
            await PopupNavigation.Instance.PushAsync(new AlertPopup(strengthModel.StrengthName, strengthModel.StrengthDetail, false));
        }

        public void TabChanged(string domainId)
        {
            FilterData(domainId, SearchText);
        }

        public async void LoadData()
        {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true, null))
            {
                var temp = await App.HRLocalDataManager.GetAllStrengths();
                StrengthData = temp;
                FilterData("0","");
            }
        }

        public void FilterData(string domainId, string SearchText)
        {
            if (StrengthData == null)
                return;

            SelectedTab = domainId;
            if (domainId == "0")
            {
                FilteredList = new ObservableCollection<StrengthModel>(StrengthData.Where(x => x.StrengthName.Contains(SearchText)).OrderBy(x => x.StrengthName));
            }
            else if (domainId == "1")
            {
                FilteredList = new ObservableCollection<StrengthModel>(StrengthData.Where(x => x.DomainID == Convert.ToInt32(domainId) && x.StrengthName.Contains(SearchText)).OrderBy(x => x.StrengthName));
            }
            else if (domainId == "2")
            {
                FilteredList = new ObservableCollection<StrengthModel>(StrengthData.Where(x => x.DomainID == Convert.ToInt32(domainId) && x.StrengthName.Contains(SearchText)).OrderBy(x => x.StrengthName));
            }
            else if (domainId == "3")
            {
                FilteredList = new ObservableCollection<StrengthModel>(StrengthData.Where(x => x.DomainID == Convert.ToInt32(domainId) && x.StrengthName.Contains(SearchText)).OrderBy(x => x.StrengthName));
            }
            else if (domainId == "4")
            {
                FilteredList = new ObservableCollection<StrengthModel>(StrengthData.Where(x => x.DomainID == Convert.ToInt32(domainId) && x.StrengthName.Contains(SearchText)).OrderBy(x => x.StrengthName));
            }
        }
    }
}
