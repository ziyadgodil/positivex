﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Services;
using HRApp.Views;
using HRApp.Views.MasterMenu;
using HRApp.Views.PopupViews;
using HRApp.Views.SubPages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class ShoutOutPageViewModel : BaseViewModel
    {
        ObservableCollection<UserModel> _allNetworkUsers { get; set; }
        public ObservableCollection<UserModel> AllNetworkUsers
        {
            get { return _allNetworkUsers; }
            set { _allNetworkUsers = value; SetPropertyChanged(nameof(AllNetworkUsers)); }
        }

        ObservableCollection<UserModel> _networkUsers { get; set; }
        public ObservableCollection<UserModel> NetworkUsers { get { return _networkUsers; } set { _networkUsers = value; SetPropertyChanged(nameof(NetworkUsers)); } }

        ObservableCollection<StrengthModel> _userStrengths { get; set; } = new ObservableCollection<StrengthModel>();
        public ObservableCollection<StrengthModel> UserStrengths { get { return _userStrengths; } set { _userStrengths = value; SetPropertyChanged(nameof(UserStrengths)); } }

        StrengthModel _selectedStrength { get; set; }
        public StrengthModel SelectedStrength { get { return _selectedStrength; } set { _selectedStrength = value; SetPropertyChanged(nameof(SelectedStrength)); SelectedIndexChanged(SelectedStrength); } }

        bool _isUserListVisible { get; set; }
        public bool IsUserListVisible { get { return _isUserListVisible; } set { _isUserListVisible = value; SetPropertyChanged(nameof(IsUserListVisible)); } }

        string _commentText { get; set; }
        public string CommentText { get { return _commentText; } set { _commentText = value; SetPropertyChanged(nameof(CommentText)); } }

        string _selectedUserName { get; set; } = "Colleague Name";
        public string SelectedUserName
        {
            get
            {
                return _selectedUserName;
            }
            set
            {
                _selectedUserName = value;
                SetPropertyChanged(nameof(SelectedUserName));
            }
        }

        string _searchText { get; set; }
        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                UserSearch(SearchText);
            }
        }

        UserModel _selectedUser { get; set; }
        public UserModel SelectedUser
        {
            get
            {
                return _selectedUser;
            }
            set
            {
                _selectedUser = value;
                SetPropertyChanged(nameof(SelectedUser));
                SelectUserChanged(SelectedUser);
            }
        }

        private bool _ispickerenable { get; set; } = false;
        public bool IsPickerEnable
        {
            get { return _ispickerenable; }
            set { _ispickerenable = value; SetPropertyChanged(nameof(IsPickerEnable)); }
        }


        public ICommand SendShoutCommand { get; }

        public ICommand UserProfileCommand { get; }

        public override void OnAppearing()
        {
            LoadData();
            //SelectedUser = new UserModel();
            IsUserListVisible = false;
        }

        public ShoutOutPageViewModel()
        {
            SendShoutCommand = new Command(SendShout);
            UserProfileCommand = new Command<UserModel>(UserProfile);
        }

        public async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    var res = await App.HRServiceManager.GetAllNetworkUsers();
                    if (res != null && res.status && res.data != null)
                    {
                        var find = res.data.items.FirstOrDefault(x => x.UserID == Settings.UserToken);
                        res.data.items.Remove(find);
                        AllNetworkUsers = res.data.items;
                        UserSearch("");
                    }
                    else
                    {
                        AllNetworkUsers = new ObservableCollection<UserModel>();
                    }
                }
            }
            catch (Exception e)
            {
                //await App.Instance.MainPage.DisplayAlert("Alert", ServiceConfiguration.CommonErrorMessage, "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", ServiceConfiguration.CommonErrorMessage, false));
            }
        }

        public async void UserSearch(String SearchText)
        {
            if (!string.IsNullOrEmpty(SearchText) && NetworkUsers != null)
                NetworkUsers = new ObservableCollection<UserModel>(AllNetworkUsers.Where(x=>x.FullName.ToLower().Contains(SearchText.ToLower())));
            else
                NetworkUsers = AllNetworkUsers;
        }

        public void SelectUserChanged (UserModel userModel)
        {
            if (userModel != null)
            {
                SearchText = userModel.FullName;
                SelectedUserName = userModel.FullName;
                IsUserListVisible = false;
                UserStrengths = (userModel.Strengths != null ? new ObservableCollection<StrengthModel>(userModel.Strengths?.ToList()) : new ObservableCollection<StrengthModel>());
                IsPickerEnable = true;
            }
        }

        public async void SendShout()
        {
            try
            {
                if (SelectedUser != null && !string.IsNullOrEmpty(CommentText) && SelectedStrength != null)
                {
                    ShoutInputModel shoutInputModel = new ShoutInputModel();
                    shoutInputModel.NetworkID = SelectedUser.UserID;
                    shoutInputModel.Comment = CommentText;
                    shoutInputModel.StrengthID = SelectedStrength.StrengthID;
                    using (UserDialogs.Instance.Loading("Loading.."))
                    {
                        var res = await App.HRServiceManager.AddShout(shoutInputModel);
                        if (res != null && res.status)
                        {
                            //await App.Instance.MainPage.DisplayAlert("Success", "Shout added successfully...", "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "Shout added successfully...", false));
                            ClearData();
                            if(((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).CurrentPage is ShoutUserPage)
                            {
                                await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PopAsync();
                            }
                        }
                        else
                        {
                            //await App.Instance.MainPage.DisplayAlert("Alert", ServiceConfiguration.CommonErrorMessage, "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", ServiceConfiguration.CommonErrorMessage, false));
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

        }
        public void ClearData()
        {
            //SelectUserChanged(new UserModel());
            UserStrengths = new ObservableCollection<StrengthModel>(UserStrengths);
            CommentText = "";
        }

        public void SelectedIndexChanged(StrengthModel strengthModel)
        {
            if (strengthModel != null)
            {
                //App.Instance.MainPage.DisplayAlert(strengthModel.StrengthName, strengthModel.StrengthDetail, "Ok");
                PopupNavigation.Instance.PushAsync(new AlertPopup(strengthModel.StrengthName, strengthModel.StrengthDetail, false));
            }
        }

        private async void UserProfile(UserModel userModel)
        {
            if (userModel != null)
            {
                var page = new ProfilePage();
                page.ViewModel.UserDetail = userModel;
                await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
            }
        }
    }
}
