﻿using HRApp.Helpers.BaseData;
using HRApp.Helpers.Resources;
using HRApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class AnalyzeTab1ViewModel : BaseViewModel
    {
        private ObservableCollection<ChartDataPoint> _data { get; set; }
        public ObservableCollection<ChartDataPoint> DomainData
        {
            get => _data;
            set { _data = value; SetPropertyChanged(nameof(DomainData)); }
        }

        private ObservableCollection<UserModel> _listFilterUsers { get; set; }
        public ObservableCollection<UserModel> ListFilterUsers
        {
            get => _listFilterUsers;
            set { _listFilterUsers = value; SetPropertyChanged(nameof(ListFilterUsers)); }
        }

        private ObservableCollection<Color> _SeriesBrushes { get; set; }
        public ObservableCollection<Color> SeriesBrushes
        {
            get => _SeriesBrushes;
            set { _SeriesBrushes = value; SetPropertyChanged(nameof(SeriesBrushes)); }
        }

        private ObservableCollection<ChartDataPoint> _strengthsData { get; set; }
        public ObservableCollection<ChartDataPoint> StrengthsData
        {
            get => _strengthsData;
            set { _strengthsData = value; SetPropertyChanged(nameof(StrengthsData)); }
        }

        private ObservableCollection<Color> _strengthsBrushes { get; set; }
        public ObservableCollection<Color> StrengthsBrushes
        {
            get => _strengthsBrushes;
            set { _strengthsBrushes = value; SetPropertyChanged(nameof(StrengthsBrushes)); }
        }

        private string _selectedItemName { get; set; }
        public string SelectedItemName
        {
            get => _selectedItemName;
            set { _selectedItemName = value; SetPropertyChanged(nameof(SelectedItemName)); }
        }

        private int _selectedItemCount { get; set; }
        public int SelectedItemCount
        {
            get { return _selectedItemCount; }
            set { _selectedItemCount = value; SetPropertyChanged(nameof(SelectedItemCount)); }
        }
        private int _selectedIndex { get; set; }
        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                _selectedIndex = value;

                if (_selectedIndex == -1 || DomainData == null || DomainData.Count == 0) return;

                SelectedItemName = DomainData[_selectedIndex].DomainName;
                SelectedItemCount = DomainData[_selectedIndex].Value;

                SetPropertyChanged(nameof(SelectedIndex));
            }
        }

        private string _strengthName { get; set; }
        public string StrengthName
        {
            get => _strengthName;
            set { _strengthName = value; SetPropertyChanged(nameof(StrengthName)); }
        }

        private int _strengthCount { get; set; }
        public int StrengthCount
        {
            get { return _strengthCount; }
            set { _strengthCount = value; SetPropertyChanged(nameof(StrengthCount)); }
        }

        private string _strengthPercentage { get; set; }
        public string StrengthPercentage
        {
            get { return _strengthPercentage; }
            set { _strengthPercentage = value; SetPropertyChanged(nameof(StrengthPercentage)); }
        }

        private int _strengthIndex { get; set; }
        public int StrengthIndex
        {
            get { return _strengthIndex; }
            set
            {
                _strengthIndex = value;
                if (_strengthIndex == -1 || StrengthsData == null || StrengthsData.Count == 0) return;

                StrengthName = StrengthsData[_strengthIndex].DomainName;
                StrengthCount = StrengthsData[_strengthIndex].Value;
                var total = StrengthsData.Sum(x => x.Value);
                var ad = ((double)StrengthsData[_strengthIndex].Value) * 100 / StrengthsData.Sum(x => x.Value);

                StrengthPercentage = ad.ToString("#.##");
                SetPropertyChanged(nameof(StrengthIndex));
            }
        }

        private string _selectedFilter { get; set; } = "Top 5";
        public string SelectedFilter
        {
            get { return _selectedFilter; }
            set 
            { 
                _selectedFilter = value; 
                SetPropertyChanged(nameof(SelectedFilter)); 
                BindStrengthData(SelectedFilter); 
                BindDomainData(SelectedFilter); 
            }
        }

        public AnalyzeTab1ViewModel()
        {

        }

        public override void OnAppearing()
        {
            base.OnAppearing();

            //var data = ((AnalysisPageViewModel)this.BindingContext).ListFilterUsers;

            #region Domain Bind data

            //var ExecutingDomain = 0;
            //var InfluencingDomain = 0;
            //var RelationshipDomain = 0;
            //var StrategicDomain = 0;

            //foreach (var item in ListFilterUsers)
            //{
            //    if (item.Strengths == null)
            //    {
            //        item.Strengths = new ObservableCollection<StrengthModel>();
            //    }
            //    ExecutingDomain += item.Strengths.Where(x => x.DomainID == 1).Count();
            //    InfluencingDomain += item.Strengths.Where(x => x.DomainID == 2).Count();
            //    RelationshipDomain += item.Strengths.Where(x => x.DomainID == 3).Count();
            //    StrategicDomain += item.Strengths.Where(x => x.DomainID == 4).Count();
            //}
            //DomainData = new ObservableCollection<ChartDataPoint>()
            //{
            //    new ChartDataPoint("Executing", ExecutingDomain),
            //    new ChartDataPoint("Influencing", InfluencingDomain),
            //    new ChartDataPoint("Relationship Building", RelationshipDomain),
            //    new ChartDataPoint("Strategic Thinking", StrategicDomain)
            //};
            //SelectedIndex = 0;
            //SeriesBrushes = new ObservableCollection<Color>()
            //{
            //    Colors.ExecutingDomain,
            //    Colors.InfluencingDomain,
            //    Colors.RelationshipDomain,
            //    Colors.StrategicDomain
            //};

            //DomainBrushes.CustomBrushes = SeriesBrushes;
            //DomainChart.ItemsSource = Data;

            #endregion

            #region Strengths Bind data
            //var d= 
            //var Strengthsresults = ListFilterUsers
            //.SelectMany(e => e.Strengths)
            //.GroupBy(d => d.StrengthID);

            //StrengthsData = new ObservableCollection<ChartDataPoint>();
            //StrengthsBrushes = new ObservableCollection<Color>();

            //foreach (var item in Strengthsresults)
            //{
            //    StrengthsData.Add(new ChartDataPoint(item.First().StrengthName, item.Count()));
            //    if (item.First().DomainID == 1)
            //        StrengthsBrushes.Add(Colors.ExecutingDomain);
            //    if (item.First().DomainID == 2)
            //        StrengthsBrushes.Add(Colors.InfluencingDomain);
            //    if (item.First().DomainID == 3)
            //        StrengthsBrushes.Add(Colors.RelationshipDomain);
            //    if (item.First().DomainID == 4)
            //        StrengthsBrushes.Add(Colors.StrategicDomain);
            //}
            //StrengthIndex = 0;
            //StrengthsCustomBrushes.CustomBrushes = StrengthsBrushes;
            //StrengthsChart.ItemsSource = StrengthsData;

            #endregion

            BindDomainData(SelectedFilter);
            BindStrengthData(SelectedFilter);
        }

        public void BindStrengthData(string SelectedFilter)
        {
            if (ListFilterUsers != null && ListFilterUsers.Count > 0)
            {
                if (SelectedFilter == "Top 5")
                {
                    var Strengthsresults = ListFilterUsers.SelectMany(e => e.Strengths.Take(5)).GroupBy(d => d.StrengthID);

                    StrengthsData = new ObservableCollection<ChartDataPoint>();
                    StrengthsBrushes = new ObservableCollection<Color>();

                    foreach (var item in Strengthsresults)
                    {
                        StrengthsData.Add(new ChartDataPoint(item.First().StrengthName, item.Count()));
                        if (item.First().DomainID == 1)
                            StrengthsBrushes.Add(Colors.ExecutingDomain);
                        if (item.First().DomainID == 2)
                            StrengthsBrushes.Add(Colors.InfluencingDomain);
                        if (item.First().DomainID == 3)
                            StrengthsBrushes.Add(Colors.RelationshipDomain);
                        if (item.First().DomainID == 4)
                            StrengthsBrushes.Add(Colors.StrategicDomain);
                    }
                    StrengthIndex = 0;
                }
                else if (SelectedFilter == "Top 10")
                {
                    var Strengthsresults = ListFilterUsers.SelectMany(e => e.Strengths.Take(10)).GroupBy(d => d.StrengthID);

                    StrengthsData = new ObservableCollection<ChartDataPoint>();
                    StrengthsBrushes = new ObservableCollection<Color>();

                    foreach (var item in Strengthsresults)
                    {
                        StrengthsData.Add(new ChartDataPoint(item.First().StrengthName, item.Count()));
                        if (item.First().DomainID == 1)
                            StrengthsBrushes.Add(Colors.ExecutingDomain);
                        if (item.First().DomainID == 2)
                            StrengthsBrushes.Add(Colors.InfluencingDomain);
                        if (item.First().DomainID == 3)
                            StrengthsBrushes.Add(Colors.RelationshipDomain);
                        if (item.First().DomainID == 4)
                            StrengthsBrushes.Add(Colors.StrategicDomain);
                    }
                    StrengthIndex = 0;
                }
                else
                {
                    var Strengthsresults = ListFilterUsers.SelectMany(e => e.Strengths).GroupBy(d => d.StrengthID);

                    StrengthsData = new ObservableCollection<ChartDataPoint>();
                    StrengthsBrushes = new ObservableCollection<Color>();

                    foreach (var item in Strengthsresults)
                    {
                        StrengthsData.Add(new ChartDataPoint(item.First().StrengthName, item.Count()));
                        if (item.First().DomainID == 1)
                            StrengthsBrushes.Add(Colors.ExecutingDomain);
                        if (item.First().DomainID == 2)
                            StrengthsBrushes.Add(Colors.InfluencingDomain);
                        if (item.First().DomainID == 3)
                            StrengthsBrushes.Add(Colors.RelationshipDomain);
                        if (item.First().DomainID == 4)
                            StrengthsBrushes.Add(Colors.StrategicDomain);
                    }
                    StrengthIndex = 0;
                }
            }

        }

        public void BindDomainData(string SelectedFilter)
        {
            var ExecutingDomain = 0;
            var InfluencingDomain = 0;
            var RelationshipDomain = 0;
            var StrategicDomain = 0;
            if (ListFilterUsers != null && ListFilterUsers.Count > 0)
            {
                if (SelectedFilter == "Top 5")
                {
                    foreach (var item in ListFilterUsers)
                    {
                        if (item.Strengths == null)
                        {
                            item.Strengths = new ObservableCollection<StrengthModel>();
                        }
                        ExecutingDomain += item.Strengths.Take(5).Where(x => x.DomainID == 1).Count();
                        InfluencingDomain += item.Strengths.Take(5).Where(x => x.DomainID == 2).Count();
                        RelationshipDomain += item.Strengths.Take(5).Where(x => x.DomainID == 3).Count();
                        StrategicDomain += item.Strengths.Take(5).Where(x => x.DomainID == 4).Count();
                    }
                    DomainData = new ObservableCollection<ChartDataPoint>()
            {
                new ChartDataPoint("Executing", ExecutingDomain),
                new ChartDataPoint("Influencing", InfluencingDomain),
                new ChartDataPoint("Relationship Building", RelationshipDomain),
                new ChartDataPoint("Strategic Thinking", StrategicDomain)
            };
                    SelectedIndex = 0;
                    SeriesBrushes = new ObservableCollection<Color>()
            {
                Colors.ExecutingDomain,
                Colors.InfluencingDomain,
                Colors.RelationshipDomain,
                Colors.StrategicDomain
            };
                }
                else if (SelectedFilter == "Top 10")
                {
                    foreach (var item in ListFilterUsers)
                    {
                        if (item.Strengths == null)
                        {
                            item.Strengths = new ObservableCollection<StrengthModel>();
                        }
                        ExecutingDomain += item.Strengths.Take(10).Where(x => x.DomainID == 1).Count();
                        InfluencingDomain += item.Strengths.Take(10).Where(x => x.DomainID == 2).Count();
                        RelationshipDomain += item.Strengths.Take(10).Where(x => x.DomainID == 3).Count();
                        StrategicDomain += item.Strengths.Take(10).Where(x => x.DomainID == 4).Count();
                    }
                    DomainData = new ObservableCollection<ChartDataPoint>()
            {
                new ChartDataPoint("Executing", ExecutingDomain),
                new ChartDataPoint("Influencing", InfluencingDomain),
                new ChartDataPoint("Relationship Building", RelationshipDomain),
                new ChartDataPoint("Strategic Thinking", StrategicDomain)
            };
                    SelectedIndex = 0;
                    SeriesBrushes = new ObservableCollection<Color>()
            {
                Colors.ExecutingDomain,
                Colors.InfluencingDomain,
                Colors.RelationshipDomain,
                Colors.StrategicDomain
            };
                }
                else
                {
                    foreach (var item in ListFilterUsers)
                    {
                        if (item.Strengths == null)
                        {
                            item.Strengths = new ObservableCollection<StrengthModel>();
                        }
                        ExecutingDomain += item.Strengths.Where(x => x.DomainID == 1).Count();
                        InfluencingDomain += item.Strengths.Where(x => x.DomainID == 2).Count();
                        RelationshipDomain += item.Strengths.Where(x => x.DomainID == 3).Count();
                        StrategicDomain += item.Strengths.Where(x => x.DomainID == 4).Count();
                    }
                    DomainData = new ObservableCollection<ChartDataPoint>()
            {
                new ChartDataPoint("Executing", ExecutingDomain),
                new ChartDataPoint("Influencing", InfluencingDomain),
                new ChartDataPoint("Relationship Building", RelationshipDomain),
                new ChartDataPoint("Strategic Thinking", StrategicDomain)
            };
                    SelectedIndex = 0;
                    SeriesBrushes = new ObservableCollection<Color>()
            {
                Colors.ExecutingDomain,
                Colors.InfluencingDomain,
                Colors.RelationshipDomain,
                Colors.StrategicDomain
            };
                }

            }
        }
    }
}