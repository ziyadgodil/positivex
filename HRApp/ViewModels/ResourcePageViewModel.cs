﻿using HRApp.Helpers.BaseData;
using System;
using HRApp.Views;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using HRApp.Views.MasterMenu;
using Acr.UserDialogs;
using HRApp.Services;
using Rg.Plugins.Popup.Services;
using HRApp.Views.PopupViews;

namespace HRApp.ViewModels
{
    public class ResourcePageViewModel : BaseViewModel
    {
        bool _isNormalUser { get; set; }
        public bool IsNormalUser { get { return _isNormalUser; } set { _isNormalUser = value; SetPropertyChanged(nameof(IsNormalUser));} }

        IDownloader downloader;

        public ICommand ShowDefinition { get; }

        public ICommand EmailCoachCommand { get; }

        public ICommand DownloadDocumentCommand { get; }

        public ResourcePageViewModel()
        {
            ShowDefinition = new Command(NavigateShowDefinition);
            EmailCoachCommand = new Command(EmailCoach);
            DownloadDocumentCommand = new Command<string>(DownloadDocument);
            //downloader = DependencyService.Get<IDownloader>();
            //downloader.OnFileDownloaded += OnFileDownloaded;
            LoadData();
        }

        public async void NavigateShowDefinition()
        {
            var page = new ResourcesTabPage();
            await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);

            //await App.Instance.MainPage.Navigation.PushAsync(new ResourcesTabPage());
        }
        public async void LoadData()
        {
            var userData = await App.HRLocalDataManager.GetUserData();
            IsNormalUser = !string.IsNullOrEmpty(userData.CoachID);
        }

        public async void EmailCoach()
        {
            using (UserDialogs.Instance.Loading("Loading.."))
            {
                var userData = await App.HRLocalDataManager.GetUserData();
                var res = await App.HRServiceManager.GetUserByID(userData.CoachID);
                Device.OpenUri(new Uri("mailto:" + res.data.Email)); //+ "?subject=" + ViewModel.HoldingDetail.name));
            }
        }

        private void OnFileDownloaded(object sender, DownloadEventArgs e)
        {
            if (e.FileSaved)
            {
                //App.Instance.MainPage.DisplayAlert("Success", "File Saved Successfully.\nPlease check PositivX_Downloads folder.", "Close");
                PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "File Saved Successfully.\nPlease check PositivX_Downloads folder.", false));
            }
            else
            {
                //App.Instance.MainPage.DisplayAlert("Error", "Error while saving the file", "Close");
                PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Error while saving the file", false));
            }
        }

        public async void DownloadDocument(string arg)
        {
            //using (UserDialogs.Instance.Loading("Loading.."))
            //{
            if (arg == "1")
                Device.OpenUri(new Uri("http://positivx.com/resourcedocument/Balcony%20Basement%20Strengths-Handout2.pdf"));
            else if (arg == "2")
                Device.OpenUri(new Uri("http://positivx.com/resourcedocument/name-it-aim-it-claim-it.pdf"));
            else if (arg == "3")
                Device.OpenUri(new Uri("http://positivx.com/resourcedocument/POWER%20OF%20TWO%20Handout_4%20Questions.pdf"));
            //}
        }
        
    }
}