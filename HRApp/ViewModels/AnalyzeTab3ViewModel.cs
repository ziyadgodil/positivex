﻿using HRApp.Helpers.BaseData;
using HRApp.Helpers.Resources;
using HRApp.Models;
using HRApp.Views.MasterMenu;
using HRApp.Views.PopupViews;
using HRApp.Views.SubPages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class AnalyzeTab3ViewModel : BaseViewModel
    {
        private ObservableCollection<ChartDataPoint> _data { get; set; }
        public ObservableCollection<ChartDataPoint> DomainData
        {
            get => _data;
            set { _data = value; SetPropertyChanged(nameof(DomainData)); }
        }

        private ObservableCollection<UserModel> _listFilterUsers { get; set; }

        public ObservableCollection<UserModel> ListFilterUsers
        {
            get => _listFilterUsers;
            set { _listFilterUsers = value; SetPropertyChanged(nameof(ListFilterUsers)); }
        }

        private ObservableCollection<UserModel> _listAllFilterUsers { get; set; }

        public ObservableCollection<UserModel> ListAllFilterUsers
        {
            get => _listAllFilterUsers;
            set { _listAllFilterUsers = value; SetPropertyChanged(nameof(ListAllFilterUsers)); }
        }

        private int _executingDomain { get; set; } = 0;
        public int ExecutingDomain 
        { 
            get { return _executingDomain; }
            set { _executingDomain = value; SetPropertyChanged(nameof(ExecutingDomain)); }
        }

        private int _influencingDomain { get; set; } = 0;
        public int InfluencingDomain
        {
            get { return _influencingDomain; }
            set { _influencingDomain = value; SetPropertyChanged(nameof(InfluencingDomain)); }
        }

        private int _relationshipDomain { get; set; } = 0;
        public int RelationshipDomain
        {
            get { return _relationshipDomain; }
            set { _relationshipDomain = value; SetPropertyChanged(nameof(RelationshipDomain)); }
        }

        private int _strategicDomain { get; set; } = 0;
        public int StrategicDomain
        {
            get { return _strategicDomain; }
            set { _strategicDomain = value; SetPropertyChanged(nameof(StrategicDomain)); }
        }

        private string _pickervalue { get; set; } = "Top 5";
        public string PickerValue
        {
            get { return _pickervalue; }
            set 
            { 
                _pickervalue = value; 
                SetPropertyChanged(nameof(PickerValue)); 
                BindDomainData(PickerValue); 
            }
        }

        public ICommand ShowDomainDetailCommand { get; }
        public ICommand UserDetailCommand { get; }

        public AnalyzeTab3ViewModel()
        {
            ShowDomainDetailCommand = new Command<string>(ShowDomainDetail);
            UserDetailCommand = new Command<UserModel>(UserDetail);
        }

        public override void OnAppearing()
        {
            base.OnAppearing();

            BindDomainData(PickerValue);
        }
        public void BindDomainData(string SelectedFilter)
        {
            ListFilterUsers = new ObservableCollection<UserModel>();

            if (ListAllFilterUsers != null && ListAllFilterUsers.Count > 0)
            {
                if (SelectedFilter == "Top 5")
                {
                    foreach (var item in ListAllFilterUsers)
                    {
                        var temp = new UserModel();
                        temp.FullName = item.FullName;
                        if (item.Strengths == null)
                            temp.Strengths = new ObservableCollection<StrengthModel>();
                        else
                            temp.Strengths = new ObservableCollection<StrengthModel>(item.Strengths.Take(5));

                        ListFilterUsers.Add(temp);

                    }
                }
                else if (SelectedFilter == "Top 10")
                {
                    foreach (var item in ListAllFilterUsers)
                    {
                        var temp = new UserModel();
                        temp.FullName = item.FullName;
                        if (item.Strengths == null)
                            temp.Strengths = new ObservableCollection<StrengthModel>();
                        else
                            temp.Strengths = new ObservableCollection<StrengthModel>(item.Strengths.Take(10));

                        ListFilterUsers.Add(temp);
                    }
                }
                else
                {
                    foreach (var item in ListAllFilterUsers)
                    {

                        var temp = new UserModel();
                        temp.FullName = item.FullName;
                        if (item.Strengths == null)
                            temp.Strengths = new ObservableCollection<StrengthModel>();
                        else
                            temp.Strengths = item.Strengths;


                        ListFilterUsers.Add(temp);
                    }
                }
            }
        }
        public async void ShowDomainDetail(string DoaminId)
        {
            if (DoaminId == "1")
                //await App.Instance.MainPage.DisplayAlert("EXECUTING", "People with dominant Executing themes know how to make things happen.", "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("EXECUTING", "People with dominant Executing themes know how to make things happen.", false));
            else if (DoaminId == "2")
                //await App.Instance.MainPage.DisplayAlert("INFLUENCING", "People with dominant Influencing themes know how to take charge, speak up, and make sure the team is heard.", "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("INFLUENCING", "People with dominant Influencing themes know how to take charge, speak up, and make sure the team is heard.", false));
            else if (DoaminId == "3")
                //await App.Instance.MainPage.DisplayAlert("RELATIONSHIP BUILDING", "People with dominant Relationship Building themes have the ability to build strong relationships that can hold a team together and make the team greater than the sum of its parts.", "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("RELATIONSHIP BUILDING", "People with dominant Relationship Building themes have the ability to build strong relationships that can hold a team together and make the team greater than the sum of its parts.", false));
            else if (DoaminId == "4")
                //await App.Instance.MainPage.DisplayAlert("STRATEGIC THINKING", "People with dominant Strategic Thinking themes help teams consider what could be. They absorb and analyze information that can inform better decisions.", "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("STRATEGIC THINKING", "People with dominant Strategic Thinking themes help teams consider what could be. They absorb and analyze information that can inform better decisions.", false));

        }

        public async void UserDetail(UserModel userModel)
        {
            var page = new ProfilePage();
            page.ViewModel.UserDetail = userModel;
            await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }
    }
}