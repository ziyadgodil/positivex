﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Views;
using HRApp.Views.MasterMenu;
using HRApp.Views.PopupViews;
using HRApp.Views.SubPages;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class AnalysisPageViewModel : BaseViewModel
    {

        private bool _isFilterVisible { get; set; }

        public bool IsFilterVisible
        {
            get => _isFilterVisible;
            set { _isFilterVisible = value; SetPropertyChanged(nameof(IsFilterVisible)); }
        }

        private ObservableCollection<GivenAndReceivedStrengthsDetail> _listReceivedStrengthsDetail { get; set; }

        public ObservableCollection<GivenAndReceivedStrengthsDetail> ListReceivedStrengthsDetail
        {
            get => _listReceivedStrengthsDetail;
            set { _listReceivedStrengthsDetail = value; SetPropertyChanged(nameof(ListReceivedStrengthsDetail)); }
        }


        private ObservableCollection<GivenAndReceivedStrengthsDetail> _allReceivedStrengthsDetail { get; set; }

        public ObservableCollection<GivenAndReceivedStrengthsDetail> AllReceivedStrengthsDetail
        {
            get => _allReceivedStrengthsDetail;
            set { _allReceivedStrengthsDetail = value; SetPropertyChanged(nameof(AllReceivedStrengthsDetail)); }
        }


        private ObservableCollection<GivenAndReceivedStrengthsDetail> _listGivenStrengthsDetail { get; set; }

        public ObservableCollection<GivenAndReceivedStrengthsDetail> ListGivenStrengthsDetail
        {
            get => _listGivenStrengthsDetail;
            set { _listGivenStrengthsDetail = value; SetPropertyChanged(nameof(ListGivenStrengthsDetail)); }
        }

        ObservableCollection<GivenAndReceivedStrengthsDetail> _allGivenStrengthsDetail { get; set; }
        public ObservableCollection<GivenAndReceivedStrengthsDetail> AllGivenStrengthsDetail
        {
            get => _allGivenStrengthsDetail;
            set { _allGivenStrengthsDetail = value; SetPropertyChanged(nameof(AllGivenStrengthsDetail)); }
        }

        private ObservableCollection<UserModel> _listFilterUsers { get; set; }

        public ObservableCollection<UserModel> ListFilterUsers
        {
            get => _listFilterUsers;
            set { _listFilterUsers = value; SetPropertyChanged(nameof(ListFilterUsers)); }
        }

        public ICommand AnalysisFilterCommand { get; }
        public ICommand AnalyzeCommand { get; }
        public ICommand ShoutCommand { get; }
        public ICommand UserProfileCommand { get; }
        public ICommand ResetCommand { get; }
        public ICommand ShowDetailCommand { get; }
        



        public AnalysisPageViewModel()
        {
            AnalysisFilterCommand = new Command(AnalysisFilter);
            ShoutCommand = new Command<UserModel>(ShoutUser);
            UserProfileCommand = new Command<UserModel>(UserProfile);
            AnalyzeCommand = new Command(Analyze);
            ResetCommand = new Command(Reset);
            ShowDetailCommand = new Command<GivenAndReceivedStrengthsDetail>(ShowDetail);
            //LoadData();
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        public async void LoadData()
        {
            using (UserDialogs.Instance.Loading("Loading..."))
            {
                var AllReceivedStrengths = await App.HRServiceManager.GetUserAllGivenAndReceivedStrengths();
                if (AllReceivedStrengths != null && AllReceivedStrengths.data != null)
                {
                    //AllReceivedStrengthsDetail = AllReceivedStrengths.data.received;
                    //AllGivenStrengthsDetail = AllReceivedStrengths.data.given;

                    AllReceivedStrengthsDetail = new ObservableCollection<GivenAndReceivedStrengthsDetail>(AllReceivedStrengths.data.received.OrderByDescending(y => y.ShoutDate));
                    AllGivenStrengthsDetail = new ObservableCollection<GivenAndReceivedStrengthsDetail>(AllReceivedStrengths.data.given.OrderByDescending(x => x.ShoutDate));
                }
                else
                {
                    AllReceivedStrengthsDetail = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
                    AllGivenStrengthsDetail = new ObservableCollection<GivenAndReceivedStrengthsDetail>();
                }

                #region OLD CODE FROM LOCAL DATABASE
                //ListReceivedStrengthsDetail = await App.HRLocalDataManager.GetAllReceivedStrengths();
                //ListGivenStrengthsDetail = await App.HRLocalDataManager.GetAllGivenStrengths();

                //AllReceivedStrengthsDetail = await App.HRLocalDataManager.GetAllReceivedStrengths();
                //AllGivenStrengthsDetail = await App.HRLocalDataManager.GetAllGivenStrengths();
                #endregion

                ListReceivedStrengthsDetail = AllReceivedStrengthsDetail;
                ListGivenStrengthsDetail = AllGivenStrengthsDetail;
            }
        }

        private async void ShowDetail(GivenAndReceivedStrengthsDetail givenAndReceivedStrengthsDetail)
        {
            //await App.Instance.MainPage.DisplayAlert(givenAndReceivedStrengthsDetail.StrengthsDetail.StrengthName,givenAndReceivedStrengthsDetail.Comment,"Ok");
            await PopupNavigation.Instance.PushAsync(new AlertPopup(givenAndReceivedStrengthsDetail.StrengthsDetail.StrengthName, givenAndReceivedStrengthsDetail.Comment, false));
        }

        private async void Analyze()
        {

            IsFilterVisible = false;
            var page = new AnalyzeTabPage(this);
            await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }

        private async void AnalysisFilter()
        {

            AnalysisFilterPopup analysisFilterPopup = new AnalysisFilterPopup();


            analysisFilterPopup.AddedFilter += async (object sender1, ObservableCollection<UserModel> listFilterUsers) =>
            {
                await App.Instance.MainPage.Navigation.PopAllPopupAsync();
                if (listFilterUsers != null && listFilterUsers.Count > 0)
                {
                    listFilterUsers.Where(p => p.UserID == Settings.UserToken).ToList().ForEach(c =>c.IsShoutVisible = false);
                    ListFilterUsers = listFilterUsers;
                    IsFilterVisible = true;
                    //ListAllUsers.Where(p => ListAllUsersConnection.Any(p2 => p2.UserID == p.UserID)).ToList().ForEach(a => a.IsUserAvailable = true);
                    ListReceivedStrengthsDetail = new ObservableCollection<GivenAndReceivedStrengthsDetail>(AllReceivedStrengthsDetail.Where(x => x.UserDetail.IsDelete != "1" && listFilterUsers.Any(y => y.UserID == x.UserDetail.UserID)));
                    ListGivenStrengthsDetail = new ObservableCollection<GivenAndReceivedStrengthsDetail>(AllGivenStrengthsDetail.Where(x => x.UserDetail.IsDelete != "1" && listFilterUsers.Any(y => y.UserID == x.UserDetail.UserID)));
                }
            };

            await App.Instance.MainPage.Navigation.PushPopupAsync(analysisFilterPopup);

        }

        private async void ShoutUser(UserModel userModel)
        {
            var page = new ShoutUserPage();
            page.ViewModel.SelectedUser = userModel;
            await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }

        private async void UserProfile(UserModel userModel)
        {
            var page = new ProfilePage();
            page.ViewModel.UserDetail = userModel;
            await ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).PushAsync(page);
        }

        private async void Reset()
        {
            IsFilterVisible = false;
            ListReceivedStrengthsDetail = AllReceivedStrengthsDetail;
            ListGivenStrengthsDetail = AllGivenStrengthsDetail;
        }

        

    }
}
