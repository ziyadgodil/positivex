﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Views.PopupViews;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class UserProfilePageViewModel : BaseViewModel
    {
        private MediaFile file;
        UserModel _userDetail { get; set; }
        public UserModel UserDetail { get { return _userDetail; } set { _userDetail = value; SetPropertyChanged(nameof(UserDetail)); } }

        bool _isBestVisible { get; set; } = false;
        public bool IsBestVisible { get { return _isBestVisible; } set { _isBestVisible = value; SetPropertyChanged(nameof(IsBestVisible)); } }

        string _isVisibleSource { get; set; } = "Uncheck";
        public string IsVisibleSource { get { return _isVisibleSource; } set { _isVisibleSource = value; SetPropertyChanged(nameof(IsVisibleSource)); } }

        private string _pdffilename { get; set; } = "No file uploaded";
        public string PdfFileName
        {
            get { return _pdffilename; }
            set { _pdffilename = value; SetPropertyChanged(nameof(PdfFileName)); }
        }
        //public ICommand OnEditBestCommand { get; }

        public ICommand OnSaveStrengthsCommand { get; }

        public ICommand OpenDetailCommand { get; }
        public ICommand UploadImageCommand { get; }
        public ICommand ShowImageCommand { get; }
        public ICommand UploadDocumentCommand { get; }
        public ICommand ViewPdfCommand { get; }
        public ICommand DownloadPdfCommand { get; }
        public ICommand IsVisibletoUserCommand { get; }

        public ICommand RemoveUserDocumentCommand { get; }
        public UserProfilePageViewModel()
        {
            LoadData();
            OnSaveStrengthsCommand = new Command(SaveStrengths);
            OpenDetailCommand = new Command<StrengthModel>(OpenDetailStrength);
            UploadImageCommand = new Command(UploadImage);
            ShowImageCommand = new Command<string>(ShowImage);
            UploadDocumentCommand = new Command(UploadDocument);
            ViewPdfCommand = new Command(ViewPdf);
            DownloadPdfCommand = new Command(DownloadPdf);
            IsVisibletoUserCommand = new Command(IsVisibletoUserFunction);
            RemoveUserDocumentCommand = new Command(RemoveUserDocument);
        }
        public async void LoadData()
        {
            var res = await App.HRLocalDataManager.GetUserData();
            if (res != null && res.Strengths != null)
            {
                var order = 1;
                foreach (var item in res.Strengths)
                {
                    item.StrengthOrder = order;
                    if (res.StrengthDetail != null)
                    {
                        item.StrengthComment = res.StrengthDetail[order - 1];
                    }
                    order++;
                }
            }
            if (!string.IsNullOrEmpty(res.Document))
            {
                PdfFileName = Path.GetFileName(res.Document);
            }
            else
            {
                PdfFileName = "No file uploaded";
            }
            UserDetail = res;
        }

        public async void SaveStrengths()
        {
            if (UserDetail.Strengths.Count() >= 5)
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
                    updateUserInputModel.Key = "Strengths";
                    updateUserInputModel.Value = string.Join(",", UserDetail.Strengths.Select(x => x.StrengthID));
                    var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);


                    updateUserInputModel = new UpdateUserInputModel();
                    updateUserInputModel.Key = "StrengthDetail";
                    updateUserInputModel.Value = string.Join("$$", UserDetail.Strengths.Select(x => x.StrengthComment));
                    res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);

                    //await App.Instance.MainPage.DisplayAlert("Success", "Strengths saved!", "Ok");
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "Strengths saved!", false));
                    LoadData();
                }
            }
            else
            {
                //await App.Instance.MainPage.DisplayAlert("Alert", "You must add atleast 5 strengths.", "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "You must add atleast 5 strengths.", false));
            }
        }
        public async void UpdateBest()
        {
            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
            updateUserInputModel.Key = "Best";
            updateUserInputModel.Value = UserDetail.Best;
            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
        }
        public async void UpdateWrost()
        {
            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
            updateUserInputModel.Key = "Worst";
            updateUserInputModel.Value = UserDetail.Worst;
            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
        }
        public async void UpdateDetail()
        {
            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
            updateUserInputModel.Key = "Detail";
            updateUserInputModel.Value = UserDetail.Detail;
            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
        }
        public async void UpdateConsistently()
        {
            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
            updateUserInputModel.Key = "Consistently";
            updateUserInputModel.Value = UserDetail.Consistently;
            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
        }
        public async void UpdateFullName()
        {
            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
            updateUserInputModel.Key = "FullName";
            updateUserInputModel.Value = UserDetail.FullName;
            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
        }

        public async void RemoveUserDocument()
        {
            if (!string.IsNullOrEmpty(UserDetail.Document))
            {
                AlertPopup alert = new AlertPopup("Confirm", "Do you want to remove report?", true);
                await PopupNavigation.Instance.PushAsync(alert);
                alert.AcceptAlertPopup += async (object sender1, bool e1) =>
                {
                    if (e1 == true)
                    {
                        using (UserDialogs.Instance.Loading())
                        {
                            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
                            updateUserInputModel.Key = "Document";
                            updateUserInputModel.Value = "";
                            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
                            if (res != null && res.status && res.data != null)
                            {
                                await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "Document Removed!", false));
                                LoadData();
                            }
                        }
                    }
                };
            }
        }

        public async void UpdatedDocumentVisibility()
        {
            UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
            updateUserInputModel.Key = "DocumentVisible";
            updateUserInputModel.Value = UserDetail.DocumentVisible;
            var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
            if (res != null && res.status && res.data != null)
            {
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "Visibility Modified!", false));
                LoadData();
            }
        }

        public void OpenDetailStrength(StrengthModel strengthModel)
        {
            //App.Instance.MainPage.DisplayAlert(strengthModel.StrengthName, strengthModel.StrengthComment, "Ok");
            PopupNavigation.Instance.PushAsync(new AlertPopup(strengthModel.StrengthName, strengthModel.StrengthDetail, false));
        }

        public async void ShowImage(string source)
        {
            if (!string.IsNullOrEmpty(source))
            {
                ProfileImagePopup profileImagePopup = new ProfileImagePopup(source);

                await App.Instance.MainPage.Navigation.PushPopupAsync(profileImagePopup);
            }
        }

        public async void UploadImage()
        {
            string action = await App.Instance.MainPage.DisplayActionSheet("", "Cancel", null, "Take Photo", "Choose Photo");

            if (action == "Take Photo")
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    //await App.Instance.MainPage.DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("No Camera", ":( No camera avaialble.", false));
                    return;
                }

                try
                {
                    var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                    var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                        cameraStatus = results[Permission.Camera];
                        storageStatus = results[Permission.Storage];
                    }

                    if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                    {
                        file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            Directory = "Sample",
                            Name = "test.jpg",
                            DefaultCamera = CameraDevice.Front,
                            PhotoSize = PhotoSize.Small,
                            CompressionQuality = 50,
                            AllowCropping = true
                        });

                        if (file == null)
                            return;

                    }
                    else
                    {
                        await App.Instance.MainPage.DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");

                        CrossPermissions.Current.OpenAppSettings();
                    }
                }
                catch (Exception error)
                {
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", error.ToString(), false));
                }

                using (UserDialogs.Instance.Loading("Saving..."))
                {
                    var res = await App.HRServiceManager.UpdateUserImage(file);

                    if (res != null && res.status && res.data != null)
                    {
                        //await App.Instance.MainPage.DisplayAlert("Success", "Profile image updated", "ok");
                        await PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "Profile image updated", false));
                        LoadData();
                    }
                    else if (res.status == false)
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", res.msg, "ok");
                        await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", res.msg, false));
                    }
                }
            }

            else if (action == "Choose Photo")
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsPickPhotoSupported)
                {
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Photos Not Supported", ":( Permission not granted to photos.", false));
                    return;
                }


                try
                {
                    var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (storageStatus != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage });
                        storageStatus = results[Permission.Storage];
                    }

                    if (storageStatus == PermissionStatus.Granted)
                    {
                        file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                        {
                            PhotoSize = PhotoSize.Small,
                            CompressionQuality = 50
                        });


                        if (file == null)
                            return;
                        var FileImage = new Image();

                        FileImage.Source = ImageSource.FromStream(() =>
                        {
                            return file.GetStream();
                        });


                    }
                    else
                    {
                        await App.Instance.MainPage.DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");

                        CrossPermissions.Current.OpenAppSettings();
                    }
                }
                catch (Exception error)
                {
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", error.ToString(), false));
                }

                using (UserDialogs.Instance.Loading("Saving..."))
                {
                    var res = await App.HRServiceManager.UpdateUserImage(file);

                    if (res != null && res.status && res.data != null)
                    {
                        //await App.Instance.MainPage.DisplayAlert("Success", "Profile image updated", "ok");
                        await PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "Profile image updated", false));
                        LoadData();
                    }
                    else if (res.status == false)
                    {
                        //await App.Instance.MainPage.DisplayAlert("Alert", res.msg, "ok");
                        await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", res.msg, false));
                    }
                }
            }
        }

        public async void UploadDocument()
        {
            FileData pickedfile = new FileData();
            try
            {
                if (PdfFileName != "No file uploaded")
                {
                    AlertPopup alert = new AlertPopup("Confirm", "Upload new document will relpace existing one.", true);
                    await PopupNavigation.Instance.PushAsync(alert);

                    alert.AcceptAlertPopup += async (object sender1, bool e1) =>
                    {
                        if (e1 == true)
                        {
                            await PopupNavigation.Instance.PopAsync();
                            pickedfile = await CrossFilePicker.Current.PickFile();
                            if (pickedfile != null)
                            {
                                if (pickedfile.FileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                                {
                                    PdfFileName = pickedfile.FileName;
                                    using (UserDialogs.Instance.Loading())
                                    {
                                        var res = await App.HRServiceManager.UpdateUserDocument(pickedfile);
                                        if (res != null && res.status && res.data != null)
                                        {
                                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "Document updated", false));
                                            LoadData();
                                        }
                                    }
                                }
                                else
                                {
                                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Only PDFs are supported \n Please select valid pdf file.", false));
                                }
                            }

                        }
                    };
                }
                else
                {
                    pickedfile = await CrossFilePicker.Current.PickFile();
                    if (pickedfile != null)
                    {
                        if (pickedfile.FileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                        {
                            PdfFileName = pickedfile.FileName;
                            using (UserDialogs.Instance.Loading())
                            {
                                var res = await App.HRServiceManager.UpdateUserDocument(pickedfile);
                                if (res != null && res.status && res.data != null)
                                {
                                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Success", "Document updated", false));
                                    LoadData();
                                }
                            }
                        }
                        else
                        {
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Error", "Only PDFs are supported \n Please select valid pdf file.", false));
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }

        public async void ViewPdf()
        {
            if (PdfFileName != "No file uploaded" && UserDetail.Document != null)
            {
                using (UserDialogs.Instance.Loading("loading.."))
                {
                    ViewPdfPopup viewPdfPopup = new ViewPdfPopup(UserDetail.Document);
                    await App.Instance.MainPage.Navigation.PushPopupAsync(viewPdfPopup);
                }
            }
        }

        public void DownloadPdf()
        {
            if (PdfFileName != "No file uploaded")
            {
                using (UserDialogs.Instance.Loading("loading.."))
                {
                    Launcher.OpenAsync(new Uri(UserDetail.Document));
                }
            }
        }

        public async void IsVisibletoUserFunction()
        {
            if (PdfFileName != "No file uploaded")
            {
                if (UserDetail.DocumentVisible == "0")
                {
                    AlertPopup alert = new AlertPopup("Confirm", "Make your report visible to other users?", true);
                    await PopupNavigation.Instance.PushAsync(alert);
                    alert.AcceptAlertPopup += (object sender1, bool e1) =>
                    {
                        if (e1 == true)
                        {
                            using (UserDialogs.Instance.Loading("Updating..."))
                            {
                                UserDetail.DocumentVisible = "1";
                                UpdatedDocumentVisibility();
                            }
                        }
                    };
                }
                else if (UserDetail.DocumentVisible == "1")
                {
                    AlertPopup alert = new AlertPopup("Confirm", "Make your report in-visible to other users?", true);
                    await PopupNavigation.Instance.PushAsync(alert);
                    alert.AcceptAlertPopup += (object sender1, bool e1) =>
                    {
                        if (e1 == true)
                        {
                            using (UserDialogs.Instance.Loading("Updating..."))
                            {
                                UserDetail.DocumentVisible = "0";
                                UpdatedDocumentVisibility();
                            }
                        }
                    };
                }

            }
        }
    }
}
