﻿using System;
using System.Windows.Input;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Views.MasterMenu;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {

        string _userName { get; set; }
        public string UserName { get { return _userName; } set { _userName = value; SetPropertyChanged(nameof(UserName)); } }

        string _userPassword { get; set; }
        public string UserPassword { get { return _userPassword; } set { _userPassword = value; SetPropertyChanged(nameof(UserPassword)); } }


        public ICommand LoginCommand { get; }
        public LoginPageViewModel()
        {
            LoginCommand = new Command(DoLogin);
            if (!string.IsNullOrEmpty(Settings.UserName) && !string.IsNullOrEmpty(Settings.UserPassword))
            {
                UserName = Settings.UserName;
                UserPassword = Settings.UserPassword;
            }
        }


        private async void DoLogin()
        {
            //UserName = "user";
            //UserPassword = "123456";

            if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(UserPassword))
            {
                var LoginData = new LoginInputModel();
                
                LoginData.UserName = UserName;
                LoginData.Password = UserPassword;
                LoginData.DeviceToken = App.FCMToken;
                if (Device.RuntimePlatform.ToLower() == "android")
                {
                    LoginData.DeviceType = "android";
                }
                else 
                {
                    LoginData.DeviceType = "ios";
                }
                
                SimpleResponse<UserModel> LoginRes;
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    LoginRes = await App.HRServiceManager.LoginUser(LoginData);

                    if(LoginRes!=null)
                    {
                        if (LoginRes.status && LoginRes.data != null)
                        {
                            //App.CurrentUser = await App.HRLocalDataManager.GetUserData();

                            Settings.UserName = UserName;
                            Settings.UserPassword = UserPassword;

                            await App.HRLocalDataManager.SaveUserData(LoginRes.data);


                            var AllStrengths = await App.HRServiceManager.GetAllStrengths();
                            await App.HRLocalDataManager.SaveStrengthsData(AllStrengths.data.items);

                            //var AllReceivedStrengths = await App.HRServiceManager.GetUserAllGivenAndReceivedStrengths();
                            //if (AllReceivedStrengths != null && AllReceivedStrengths.data != null)
                            //{
                            //    await App.HRLocalDataManager.SaveGivenStrengthsData(AllReceivedStrengths.data.given);
                            //    await App.HRLocalDataManager.SaveReceivedStrengthsData(AllReceivedStrengths.data.received);
                            //}

                            //var AllNetworkUsers = await App.HRServiceManager.GetAllNetworkUsers();
                            //await App.HRLocalDataManager.SaveNetworkUsers(AllNetworkUsers.data.items);

                            App.Instance.MainPage = new SliderMenuPage();
                        }
                        else
                        {
                            //await App.Instance.MainPage.DisplayAlert("Alert", LoginRes.msg, "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", LoginRes.msg, false));
                        }
                    }
                }

            }
            else
            {
                //await App.Instance.MainPage.DisplayAlert("Alert", "Invalid username and password please try again.", "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Invalid username and password please try again.", false));
            }
        }
    }
}
