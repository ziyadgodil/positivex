﻿using HRApp.Helpers.BaseData;
using System;
using System.Collections.Generic;
using System.Text;
using HRApp.Views.PopupViews;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Rg.Plugins.Popup.Extensions;
using HRApp.Models;
using System.Collections.ObjectModel;
using Acr.UserDialogs;
using HRApp.Services;
using Rg.Plugins.Popup.Services;

namespace HRApp.ViewModels
{
    public class User360PageViewModel : BaseViewModel
    {
        ObservableCollection<Connection360Model> _listAllUsersConnection { get; set; }

        public ObservableCollection<Connection360Model> ListAllUsersConnection
        {
            get { return _listAllUsersConnection; }
            set { _listAllUsersConnection = value; SetPropertyChanged(nameof(ListAllUsersConnection)); }
        }

        ObservableCollection<UserModel> _listAllUsers { get; set; }

        public ObservableCollection<UserModel> ListAllUsers
        {
            get { return _listAllUsers; }
            set { _listAllUsers = value; SetPropertyChanged(nameof(ListAllUsers)); }
        }

        ObservableCollection<UserModel> _listofnetworkusers { get; set; }

        public ObservableCollection<UserModel> ListofNetworkUsers
        {
            get { return _listofnetworkusers; }
            set { _listofnetworkusers = value; SetPropertyChanged(nameof(ListofNetworkUsers)); }
        }

        private ObservableCollection<Connection360Model> _listFilterUsers { get; set; }

        public ObservableCollection<Connection360Model> ListFilterUsers
        {
            get => _listFilterUsers;
            set { _listFilterUsers = value; SetPropertyChanged(nameof(ListFilterUsers)); }
        }

        private ObservableCollection<ConnectionShout> _connectionShouts { get; set; }
        public ObservableCollection<ConnectionShout> ConnectionShouts
        {
            get { return _connectionShouts; }
            set { _connectionShouts = value;SetPropertyChanged(nameof(ConnectionShouts)); }
        }

        private int _superior { get; set; }

        public int Superior
        {
            get => _superior;
            set { _superior = value; SetPropertyChanged(nameof(Superior)); }
        }


        private int _colleague { get; set; }

        public int Colleague
        {
            get => _colleague;
            set { _colleague = value; SetPropertyChanged(nameof(Colleague)); }
        }

        private int _junior { get; set; }

        public int Junior
        {
            get => _junior;
            set { _junior = value; SetPropertyChanged(nameof(Junior)); }
        }


        private string _selectedType { get; set; }

        public string SelectedType
        {
            get => _selectedType;
            set { _selectedType = value; SetPropertyChanged(nameof(SelectedType)); FilterShout(value); }
        }

        private Color _shoutBGColor { get; set; } = Color.FromHex("#FFFFFF");

        public Color ShoutBGColor
        {
            get => _shoutBGColor;
            set { _shoutBGColor = value; SetPropertyChanged(nameof(ShoutBGColor)); }
        }




        private UserModel _currentUser { get; set; }

        public UserModel CurrentUser
        {
            get => _currentUser;
            set { _currentUser = value; SetPropertyChanged(nameof(CurrentUser)); }
        }

        public ICommand AddPopupCommand { get; }

        public User360PageViewModel()
        {
            AddPopupCommand = new Command<string>(ShowAddUserPopup);
        }

        public async void ShowAddUserPopup(string parm)
        {
            AddUserPopup addUserPopup = new AddUserPopup();

            ListAllUsers.ToList().ForEach(z => { z.IsCheckedUser = false; z.NotificationType = "Weekly"; });

            var filter = ListAllUsersConnection.Where(x => x.ConnectionType == parm).Select(x => x.UserDetail);

            var outuser = new ObservableCollection<UserModel>(ListAllUsersConnection.Where(x => x.InviteUser == 1).Select(x => x.UserDetail));
            //var outofnetworkuser = new ObservableCollection<UserModel>(filter.Where(c => c.UserID == null));

            if (outuser.Count > 0)
            {
                foreach (var item in outuser)
                {
                    ListAllUsers.Add(item);
                }
            }

            //foreach (var item in ListofNetworkUsers) 
            //{
            //    var RemovalItem = ListAllUsers.FirstOrDefault(x => x.UserID == item.UserID);
            //    if (RemovalItem != null)
            //        ListAllUsers.Remove(RemovalItem);
            //}

            foreach (var item in filter) 
            {
                var finditem = ListofNetworkUsers.FirstOrDefault(x => x.UserID == item.UserID);
                if (finditem == null) 
                {
                    ListofNetworkUsers.Add(item);
                }
            }

            addUserPopup.UserConnectionList = ListofNetworkUsers;
            addUserPopup.OutUserConnectionList = ListAllUsers;
            addUserPopup.ConnectionType = parm;

            addUserPopup.UserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList().ForEach(z => z.IsCheckedUser = true);

            addUserPopup.OutUserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList().ForEach(z => z.IsCheckedUser = true);

            addUserPopup.UserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList()
                .ForEach(z => z.NotificationType = filter.First(p => p.Email == z.Email).NotificationType);

            addUserPopup.OutUserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList()
                .ForEach(z => z.NotificationType = filter.First(p => p.Email == z.Email).NotificationType);

            addUserPopup.UserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList()
                .ForEach(z => z.Connection360ID = filter.First(p => p.Email == z.Email).Connection360ID);

            addUserPopup.OutUserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList()
                .ForEach(z => z.Connection360ID = filter.First(p => p.Email == z.Email).Connection360ID);

            addUserPopup.UserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList()
                .ForEach(z => z.UpcomingNotificationDate = filter.First(p => p.Email == z.Email).UpcomingNotificationDate);

            addUserPopup.OutUserConnectionList.Where(x => filter.Any(y => y.Email == x.Email)).ToList()
                .ForEach(z => z.UpcomingNotificationDate = filter.First(p => p.Email == z.Email).UpcomingNotificationDate);

            //addUserPopup.AddedFilter += async (object sender1, ObservableCollection<UserModel> filteredusers) =>
            //{
            //    await App.Instance.MainPage.Navigation.PopAllPopupAsync();
            //    if (filteredusers != null && filteredusers.Count > 0)
            //    {
            //        using (UserDialogs.Instance.Loading("Loading.."))
            //        {
            //            foreach (var item in filteredusers)
            //            {
            //                Connection360InputModel connectionInputModel = new Connection360InputModel();
            //                connectionInputModel.ConnectionID = item.UserID;
            //                connectionInputModel.ConnectionType = parm;
            //                connectionInputModel.NotificationType = item.NotificationType;
            //                await App.HRServiceManager.AddUserConnection(connectionInputModel);
            //            }
            //            GetAllUsersConnection();
            //        }
            //        //ListFilterUsers = filteredusers;
            //    }
            //};

            addUserPopup.AddedFilter += OnSuccessFunction;

            await App.Instance.MainPage.Navigation.PushPopupAsync(addUserPopup);
        }

        public void OnSuccessFunction(object sender, EventArgs e)
        {
            GetAllUsersConnection();
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            GetAllUsersConnection();
        }

        public async void GetAllUsersConnection()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    CurrentUser = await App.HRLocalDataManager.GetUserData();
                    var connectionUser = await App.HRServiceManager.GetAllUsersConnection();

                    ListAllUsersConnection = connectionUser.data;

                    if (ListAllUsersConnection != null)
                    {
                        Superior = ListAllUsersConnection.Count(x => x.ConnectionType == "1");
                        Colleague = ListAllUsersConnection.Count(x => x.ConnectionType == "2");
                        Junior = ListAllUsersConnection.Count(x => x.ConnectionType == "3");
                        ConnectionShouts = new ObservableCollection<ConnectionShout>(ListAllUsersConnection.Where(z => z.connectionShouts != null).SelectMany(x => x.connectionShouts).ToList().OrderByDescending(s => s.DisplayDateTime));
                        //var dat = ListAllUsersConnection.Where(z=>z.connectionShouts!=null).Select(x => x.connectionShouts);
                        //ListAllUsersConnection.Where(z => z.connectionShouts != null).Select(i => i.connectionShouts).Aggregate());
                    }
                    var res = await App.HRServiceManager.GetAllUsers();
                    if (res != null && res.data != null)
                    {
                        ListAllUsers = new ObservableCollection<UserModel>(res.data.Where(x=>x.UserID != Settings.UserToken));
                    }
                    else
                    {
                        ListAllUsers = new ObservableCollection<UserModel>();
                    }

                    var networklist = await App.HRServiceManager.GetAllNetworkUsers();
                    if (networklist != null)
                    {
                        ListofNetworkUsers = new ObservableCollection<UserModel>(networklist.data.items.Where(x => x.UserID != Settings.UserToken));
                    }
                    else
                    {
                        ListofNetworkUsers = new ObservableCollection<UserModel>();
                    }
                }
            }
            catch (Exception ex)
            {
                //await App.Instance.MainPage.DisplayAlert("Alert", ServiceConfiguration.CommonErrorMessage, "Ok");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", ServiceConfiguration.CommonErrorMessage, false));
            }
        }

        public void FilterShout(string SelectedType)
        {
            if(SelectedType== "Managers/Mentors")
            {
                ShoutBGColor = Color.FromHex("#e8e3e3");
                ConnectionShouts = new ObservableCollection<ConnectionShout>(ListAllUsersConnection.Where(z => z.ConnectionType == "1" && z.connectionShouts != null).SelectMany(x => x.connectionShouts).ToList());
            }
            else if (SelectedType == "Peers")
            {
                ShoutBGColor = Color.FromHex("#4CB5C4");
                ConnectionShouts = new ObservableCollection<ConnectionShout>(ListAllUsersConnection.Where(z => z.ConnectionType == "2" && z.connectionShouts != null).SelectMany(x => x.connectionShouts).ToList());
            }
            else if (SelectedType == "Direct Reports")
            {
                ShoutBGColor = Color.FromHex("#bfbfbf");
                ConnectionShouts = new ObservableCollection<ConnectionShout>(ListAllUsersConnection.Where(z => z.ConnectionType == "3" && z.connectionShouts != null).SelectMany(x => x.connectionShouts).ToList());
            }

        }
    }
}
