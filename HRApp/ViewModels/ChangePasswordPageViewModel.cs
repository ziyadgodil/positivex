﻿using HRApp.Helpers.BaseData;
using System;
using System.Collections.Generic;
using System.Text;
using Acr.UserDialogs;
using System.Windows.Input;
using Xamarin.Forms;
using HRApp.Models;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Services;

namespace HRApp.ViewModels
{
    public class ChangePasswordPageViewModel : BaseViewModel
    {
        string _password { get; set; }
        public string Password { get { return _password; } set { _password = value; SetPropertyChanged(nameof(Password)); } }

        string _confirmPassword { get; set; }
        public string ConfirmPassword { get { return _confirmPassword; } set { _confirmPassword = value; SetPropertyChanged(nameof(ConfirmPassword)); } }

        public ICommand SaveCommand { get; }
        public ChangePasswordPageViewModel()
        {
            SaveCommand = new Command(ChangePassword);
        }

        public async void ChangePassword()
        {
            if (!string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(ConfirmPassword))
            {
                if (Password == ConfirmPassword)
                {
                    using (UserDialogs.Instance.Loading("Saving..."))
                    {
                        UpdateUserInputModel updateUserInputModel = new UpdateUserInputModel();
                        updateUserInputModel.Key = "Password";
                        updateUserInputModel.Value = Password;
                        var res = await App.HRServiceManager.UpdateProfile(updateUserInputModel);
                        if (res.status)
                        {
                            //await App.Instance.MainPage.DisplayAlert("Result", "Password changed successfully!", "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", "Password changed successfully!", false));
                            ClearEditors();
                        }
                        else
                        {
                            //await App.Instance.MainPage.DisplayAlert("Result", res.msg, "Ok");
                            await PopupNavigation.Instance.PushAsync(new AlertPopup("Result", res.msg, false));
                        }
                    }
                }
                else
                {
                    //await App.Instance.MainPage.DisplayAlert("Alert", "Password and confirm password does not match!", "OK");
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Password and confirm password does not match!", false));
                    ClearEditors();
                }
            }
            else
            {
                //await App.Instance.MainPage.DisplayAlert("Alert", "Please enter the valid details", "OK");
                await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "Please enter the valid details", false));
            }
        }

        void ClearEditors()
        {
            Password = "";
            ConfirmPassword = "";
        }
    }
}