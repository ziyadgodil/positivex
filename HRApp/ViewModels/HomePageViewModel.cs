﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Acr.UserDialogs;
using HRApp.Helpers.BaseData;
using HRApp.Helpers.Resources;
using HRApp.Models;
using HRApp.Views;
using HRApp.Views.MasterMenu;
using HRApp.Views.NavBar;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        private HomeModel _homemodel { get; set; }

        public HomeModel HomeModel
        {
            get => _homemodel;
            set { _homemodel = value;SetPropertyChanged(nameof(HomeModel)); }
        }

        private ObservableCollection<ChartDataPoint> _data { get; set; }

        public ObservableCollection<ChartDataPoint> Data
        {
            get => _data;
            set { _data = value; SetPropertyChanged(nameof(Data)); }
        }

        public ObservableCollection<StrengthModel> AllStrengths { get; set; }

        private string _moreLessLable { get; set; } = "+ more";

        public string MoreLessLable
        {
            get => _moreLessLable;
            set { _moreLessLable = value; SetPropertyChanged(nameof(MoreLessLable)); }
        }

        private ObservableCollection<Color> _SeriesBrushes { get; set; }

        public ObservableCollection<Color> SeriesBrushes
        {
            get => _SeriesBrushes;
            set { _SeriesBrushes = value; SetPropertyChanged(nameof(SeriesBrushes)); }
        }


        private string _selectedItemName { get; set; }

        public string SelectedItemName
        {
            get => _selectedItemName;
            set { _selectedItemName = value; SetPropertyChanged(nameof(SelectedItemName)); }
        }

        private int _selectedIndex { get; set; } = -1;

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                _selectedIndex = value;

                if (_selectedIndex == -1 || Data == null) return;

                SetPropertyChanged(nameof(SelectedIndex));

                SelectDomainName();
            }
        }
        private ObservableCollection<NotificationModel> _notificationlist { get; set; }
        public ObservableCollection<NotificationModel> NotificationList
        {
            get { return _notificationlist; }
            set { _notificationlist = value; SetPropertyChanged(nameof(NotificationList)); }
        }

        private string _notificationlistcount { get; set; }
        public string NotificationListCount
        {
            get { return _notificationlistcount; }
            set { _notificationlistcount = value; SetPropertyChanged(nameof(NotificationListCount)); }
        }

        private bool _isCountFrameVisible { get; set; } = false;
        public bool IsCountFrameVisible 
        {
            get { return _isCountFrameVisible; }
            set { _isCountFrameVisible = value; SetPropertyChanged(nameof(IsCountFrameVisible)); }
        }
        public ICommand MoreLessCommand { get; }

        public ICommand TappedStrengthsCommand { get; }

        public ICommand ShoutCommentCommand { get; }

        public ICommand AddNetworkCommand { get; }

        public ICommand ShowNotificationCommand { get; }

        public ICommand GotoAnlysisCommand { get; }

        public ICommand OpenSliderMenuCommand { get; }

        public HomePageViewModel()
        {
            MoreLessCommand = new Command(DoMoreLess);
            TappedStrengthsCommand = new Command<StrengthModel>(TappedStrength);
            ShoutCommentCommand = new Command<Activity>(ShoutComment);
            AddNetworkCommand = new Command(AddNetwork);
            GotoAnlysisCommand = new Command(GotoAnlysis);
            ShowNotificationCommand = new Command(ShowNotificationPopup);
            OpenSliderMenuCommand = new Command(OpenSliderMenu);
            CheckUserExist();

            MessagingCenter.Subscribe<MessagingCenterModel>(this, "NotifyUpdate", (sender) => 
            {
                LoadHomePage();
            });

        }

        public async void CheckUserExist()
        {
            try
            {
                var checkuserdetails = await App.HRServiceManager.GetUserByID(Settings.UserToken);
                if (checkuserdetails != null && checkuserdetails.status == false && checkuserdetails.data == null)
                {
                    Settings.UserName = "";
                    Settings.UserPassword = "";
                    App.Instance.MainPage = new LoginPage();
                }
            }
            catch(Exception ex)
            { }
        }

        public void TappedStrength(StrengthModel strengthModel)
        {
            //App.Instance.MainPage.DisplayAlert(strengthModel.StrengthName, strengthModel.StrengthDetail, "Ok");
            PopupNavigation.Instance.PushAsync(new AlertPopup(strengthModel.StrengthName, strengthModel.StrengthDetail, false));
        }

        public void ShoutComment(Activity activitymodel)
        {
            if(!string.IsNullOrEmpty(activitymodel.strengthName) && !string.IsNullOrEmpty(activitymodel.shoutComment))
                //App.Instance.MainPage.DisplayAlert(activitymodel.strengthName, activitymodel.shoutComment, "OK");
                PopupNavigation.Instance.PushAsync(new AlertPopup(activitymodel.strengthName, activitymodel.shoutComment, false));
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadHomePage();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<MessagingCenterModel>(this, "NotifyUpdate");
        }

        private async void AddNetwork()
        {
            try
            {
                ObservableCollection<UserModel> ListAllUsers = new ObservableCollection<UserModel>();
                ObservableCollection<UserModel> ListAllUsersConnection = new ObservableCollection<UserModel>();
                ObservableCollection<UserModel> FilteredList = new ObservableCollection<UserModel>();

                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    var AllUserslist = await App.HRServiceManager.GetAllUsers();
                    if (AllUserslist != null)
                    {
                        ListAllUsers = new ObservableCollection<UserModel>(AllUserslist.data.Where(x => x.UserID != Settings.UserToken));
                    }
                    else
                    {
                        ListAllUsers = new ObservableCollection<UserModel>();
                    }

                    var connectionusers = await App.HRServiceManager.GetAllNetworkUsers();
                    if (connectionusers != null)
                    {
                        ListAllUsersConnection = new ObservableCollection<UserModel>(connectionusers.data.items.Where(x => x.UserID != Settings.UserToken));
                    }
                    else
                    {
                        ListAllUsersConnection = new ObservableCollection<UserModel>();
                    }
                }
                
                
                ListAllUsers.Where(p => ListAllUsersConnection.Any(p2 => p2.UserID == p.UserID)).ToList().ForEach(a => a.IsUserAvailable = true);

                ListAllUsersConnection.ToList().ForEach(z => z.IsCheckedUser = true);

                //foreach (UserModel item in ListAllUsersConnection)
                //{
                //    var finditem = ListAllUsers.FirstOrDefault(x => x.UserID == item.UserID);
                //    if(finditem!=null)
                //        ListAllUsers.Remove(finditem);
                //}

                AddNetworkPopup networkPopup = new AddNetworkPopup();
                networkPopup.UserConnectionList = ListAllUsers;
                networkPopup.Networkuserslist = ListAllUsersConnection;
                //networkPopup.AddedUsers += async (object sender1, ObservableCollection<UserModel> filteredusers) =>
                //{
                //    await App.Instance.MainPage.Navigation.PopAllPopupAsync();
                //    if (filteredusers != null && filteredusers.Count > 0)
                //    {
                //        using (UserDialogs.Instance.Loading("Loading.."))
                //        {
                //            foreach (var item in filteredusers)
                //            {
                //                UserNetworkInputModel userNetworkInput = new UserNetworkInputModel();
                //                userNetworkInput.NetworkID = item.UserID;
                //                await App.HRServiceManager.AddUserNetwork(userNetworkInput);
                //            }
                //        }
                //    }
                //};
                await App.Instance.MainPage.Navigation.PushPopupAsync(networkPopup);
            }
            catch (Exception ex)
            {

            }
        }

        private async void DoMoreLess()
        {
            if (MoreLessLable == "+ more")
            {
                MoreLessLable = "- less";
                HomeModel.Strengths = AllStrengths;
            }
            else
            {
                MoreLessLable = "+ more";
                HomeModel.Strengths = new ObservableCollection<StrengthModel>(AllStrengths.Take(5));
            }
        }

        public async void LoadHomePage()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    
                        var res = await App.HRServiceManager.SetHomeDashboard();
                        if (res != null)
                        {
                            if (res.status && res.data != null)
                            {
                                if (res.data.Strengths == null)
                                    res.data.Strengths = new ObservableCollection<StrengthModel>();

                                if (res.data.Strengths != null)
                                {
                                    var order = 1;
                                    foreach (var item in res.data.Strengths)
                                    {
                                        item.StrengthOrder = order;
                                        order++;
                                    }
                                    AllStrengths = res.data.Strengths;
                                    res.data.Strengths = new ObservableCollection<StrengthModel>(AllStrengths.Take(5));

                                    Data = new ObservableCollection<ChartDataPoint>();
                                    SeriesBrushes = new ObservableCollection<Color>();

                                    if (AllStrengths.Where(x => x.DomainID == 1).Count() > 0)
                                    {
                                        Data.Add(new ChartDataPoint("Executing", AllStrengths.Where(x => x.DomainID == 1).Count()));
                                        SeriesBrushes.Add(Colors.ExecutingDomain);
                                    }
                                    if (AllStrengths.Where(x => x.DomainID == 2).Count() > 0)
                                    {
                                        Data.Add(new ChartDataPoint("Influencing", AllStrengths.Where(x => x.DomainID == 2).Count()));
                                        SeriesBrushes.Add(Colors.InfluencingDomain);
                                    }
                                    if (AllStrengths.Where(x => x.DomainID == 3).Count() > 0)
                                    {
                                        Data.Add(new ChartDataPoint("Relationship Building", AllStrengths.Where(x => x.DomainID == 3).Count()));
                                        SeriesBrushes.Add(Colors.RelationshipDomain);
                                    }
                                    if (AllStrengths.Where(x => x.DomainID == 4).Count() > 0)
                                    {
                                        Data.Add(new ChartDataPoint("Strategic Thinking", AllStrengths.Where(x => x.DomainID == 4).Count()));
                                        SeriesBrushes.Add(Colors.StrategicDomain);
                                    }
                                    //SelectedIndex = 0;
                                    MoreLessLable = "+ more";
                                }

                                HomeModel = res.data;
                                if (res.data.NotificationList.Count > 0)
                                {
                                    NotificationList = res.data.NotificationList;
                                    NotificationListCount = NotificationList.Count.ToString();
                                IsCountFrameVisible = true;
                                    //MessagingCenter.Send(new MessagingCenterModel { }, "NotificationCount", NotificationList.Count());
                                }
                                else if (res.data.NotificationList.Count == 0)
                                {
                                    NotificationList = new ObservableCollection<NotificationModel>();
                                NotificationListCount = NotificationList.Count().ToString();
                                IsCountFrameVisible = false;
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void OpenSliderMenu() 
        {
            var slidermenupage = ((SliderMenuPage)App.Instance.MainPage);
            slidermenupage.IsPresented = !slidermenupage.IsPresented;
        }
        public void GotoAnlysis()
        {
            var tabbedPage = ((NavigationPage)((SliderMenuPage)App.Instance.MainPage).Detail).CurrentPage as BottomTabPage;
            tabbedPage.SwitchToAnalysis();
        }

        private async void ShowNotificationPopup()
        {
            try
            {
                if (NotificationList.Count > 0)
                {

                    NotificationPopup notificationPopup = new NotificationPopup();

                    notificationPopup.NotificationList = NotificationList;

                    notificationPopup.UpdateNotification += (object sender1 ,NotificationModel selectedItem) => 
                    {
                        if (selectedItem != null) 
                        {
                            NotificationList.Remove(selectedItem);
                            if (NotificationList.Count() > 0)
                            {
                                notificationPopup.NotificationList = NotificationList;
                                NotificationListCount = NotificationList.Count().ToString();
                                //MessagingCenter.Send(new MessagingCenterModel { }, "NotificationCount", NotificationList.Count());
                            }
                            else 
                            {
                                IsCountFrameVisible = false;
                            }
                        }
                    };

                    await App.Instance.MainPage.Navigation.PushPopupAsync(notificationPopup);

                    //ListAllNotifications = await App.HRLocalDataManager.GetAllNetworkUsers();
                    //notificationPopup.NotificationList = ListAllNotifications;

                }
                else
                {
                    //await App.Instance.MainPage.DisplayAlert("Alert", "You have no notification right now!", "Ok");
                    await PopupNavigation.Instance.PushAsync(new AlertPopup("Alert", "You have no notification right now!", false));
                }

            }
            catch (Exception ex)
            {
            }
        }


        private async void SelectDomainName()
        {
            SelectedItemName = Data[_selectedIndex].DomainName;

            if (SelectedItemName.ToLower() == "EXECUTING".ToLower())
                await PopupNavigation.Instance.PushAsync(new AlertPopup("EXECUTING", "People with dominant Executing themes know how to make things happen.", false));
            else if (SelectedItemName.ToLower() == "INFLUENCING".ToLower())
                await PopupNavigation.Instance.PushAsync(new AlertPopup("INFLUENCING", "People with dominant Influencing themes know how to take charge, speak up, and make sure the team is heard.", false));
            else if (SelectedItemName.ToLower() == "RELATIONSHIP BUILDING".ToLower())
                await PopupNavigation.Instance.PushAsync(new AlertPopup("RELATIONSHIP BUILDING", "People with dominant Relationship Building themes have the ability to build strong relationships that can hold a team together and make the team greater than the sum of its parts.", false));
            else if (SelectedItemName.ToLower() == "STRATEGIC THINKING".ToLower())
                await PopupNavigation.Instance.PushAsync(new AlertPopup("STRATEGIC THINKING", "People with dominant Strategic Thinking themes help teams consider what could be. They absorb and analyze information that can inform better decisions.", false));

        }
    }

    public class ChartDataPoint
    {
        //public Color DomainColor { get; set; }
        public string DomainName { get; set; }
        public int Value { get; set; }
        public ChartDataPoint(string _domainName, int _value)
        {
            DomainName = _domainName;
            Value = _value;
        }
        
    }
}
