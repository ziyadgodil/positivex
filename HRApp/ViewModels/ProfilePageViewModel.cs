﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using HRApp.Helpers.BaseData;
using HRApp.Models;
using HRApp.Views.PopupViews;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace HRApp.ViewModels
{
    public class ProfilePageViewModel : BaseViewModel
    {
        UserModel _userDetail { get; set; }
        public UserModel UserDetail { get { return _userDetail; } set { _userDetail = value; SetPropertyChanged(nameof(UserDetail)); OrderStrengths(); } }

        ObservableCollection<StrengthModel> _userStrengths { get; set; }
        public ObservableCollection<StrengthModel> UserStrengths { get { return _userStrengths; } set { _userStrengths = value; SetPropertyChanged(nameof(UserStrengths)); } }

        public ObservableCollection<StrengthModel> AllStrengths { get; set; }

        bool _showreport { get; set; } = false;
        public bool ShowReport 
        {
            get { return _showreport; }
            set { _showreport = value; SetPropertyChanged(nameof(ShowReport)); }
        }

        private string _pdffilename { get; set; } = "No file uploaded";
        public string PdfFileName
        {
            get { return _pdffilename; }
            set { _pdffilename = value; SetPropertyChanged(nameof(PdfFileName)); }
        }
        public ICommand TappedStrengthsCommand { get; }
        public ICommand ShowImageCommand { get; }
        public ICommand ViewPdfCommand { get; }
        public ICommand DownloadPdfCommand { get; }

        //private string _moreLessLable { get; set; } = "+ more";

        //public string MoreLessLable
        //{
        //    get => _moreLessLable;
        //    set { _moreLessLable = value; SetPropertyChanged(nameof(MoreLessLable)); }
        //}

        //public ICommand MoreLessCommand { get; }

        public ProfilePageViewModel()
        {
            //MoreLessCommand = new Command(DoMoreLess);
            TappedStrengthsCommand = new Command<StrengthModel>(TappedStrength);
            ShowImageCommand = new Command<string>(ShowImage);
            ViewPdfCommand = new Command(ViewPdf);
            DownloadPdfCommand = new Command(DownloadPdf);
        }

        public void TappedStrength(StrengthModel strengthModel)
        {
            //App.Instance.MainPage.DisplayAlert(strengthModel.StrengthName, strengthModel.StrengthDetail, "Ok");
            PopupNavigation.Instance.PushAsync(new AlertPopup(strengthModel.StrengthName, strengthModel.StrengthDetail, false));
        }

        public void OrderStrengths()
        {
            if (UserDetail.Strengths == null)
                return;
            var order = 1;
            foreach (var item in UserDetail.Strengths)
            {
                item.StrengthOrder = order;
                order++;
            }
            AllStrengths = UserDetail.Strengths;
            UserStrengths = AllStrengths; //new ObservableCollection<StrengthModel>(AllStrengths.Take(5));
        }

        public async void ShowImage(string source)
        {
            if (!string.IsNullOrEmpty(source))
            {
                ProfileImagePopup profileImagePopup = new ProfileImagePopup(source);

                await App.Instance.MainPage.Navigation.PushPopupAsync(profileImagePopup);
            }

        }
        public override void OnAppearing()
        {
            base.OnAppearing();
            if (UserDetail.DocumentVisible != "0" && !string.IsNullOrEmpty(UserDetail.Document))
            {
                ShowReport = true;
                PdfFileName = Path.GetFileName(UserDetail.Document);
            }
            else 
            {
                ShowReport = false;
            }
        }

        public async void ViewPdf()
        {
            if (PdfFileName != "No file uploaded" && UserDetail.Document != null)
            {
                ViewPdfPopup viewPdfPopup = new ViewPdfPopup(UserDetail.Document);
                await App.Instance.MainPage.Navigation.PushPopupAsync(viewPdfPopup);
            }
        }

        public void DownloadPdf()
        {
            if (PdfFileName != "No file uploaded")
                Launcher.OpenAsync(new Uri(UserDetail.Document));
        }

    }
}
