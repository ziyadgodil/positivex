﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Acr.UserDialogs;
using Plugin.CurrentActivity;
using Android.Gms.Common;
using Firebase.Iid;
using Android.Util;
using HRApp.Helpers.BaseData;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;

namespace HRApp.Droid
{
    [Activity(Label = "PositivX", Icon = "@mipmap/positivx", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        static readonly string TAG = "MainActivity";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(enableFastRenderer: true);
            base.OnCreate(savedInstanceState);

            UserDialogs.Init(this);
            Xamarin.Forms.Forms.SetFlags("CarouselView_Experimental");
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            
            LoadApplication(new App());

            IsPlayServicesAvailable();
            CreateNotificationChannel();

            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {  // UserID Key for getting user id
                    var value = Intent.Extras.GetString(key);
                    if (key == "UserID" && !string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(Settings.UserToken)) 
                    {
                        Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                        MessagingCenter.Send(new MessagingCenterModel { }, "SendShout", value);
                        break;
                    }
                    
                }
            }

            Log.Debug(TAG, "InstanceID token: " + FirebaseInstanceId.Instance.Token);

            if (string.IsNullOrWhiteSpace(App.FCMToken))
            {
                App.FCMToken = FirebaseInstanceId.Instance.Token;
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            //BadgeDrawable.SetBadgeCount(this, menu.GetItem(0), 3);
            return base.OnPrepareOptionsMenu(menu);
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            return base.OnCreateOptionsMenu(menu);
        }
        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {

                return false;
            }
            else
            {
                //msgText.Text = "Google Play Services is available.";
                return true;
            }
        }

        public async override void OnBackPressed()
        {
            if (Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed))
            {
                // Do something if there are some pages in the `PopupStack`
                await PopupNavigation.Instance.PopAllAsync();
            }
            else
            {
                // Do something if there are not any pages in the `PopupStack`
            }
        }

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID, "Notifications", NotificationImportance.Max)
            {
                Description = "PositivX Notification Channel",
                Importance = NotificationImportance.Max,
                LockscreenVisibility = NotificationVisibility.Public
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }
    }
}