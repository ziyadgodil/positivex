﻿using System;
using Android.Content;
using Android.Views;
using HRApp.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(BorderlessPicker))]
namespace HRApp.Droid.CustomRenderers
{
    [Obsolete]
    public class BorderlessPicker : PickerRenderer
    {
        public static void Init() { }
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                Control.Background = null;

                var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                layoutParams.SetMargins(0, 0, 0, 0);
                LayoutParameters = layoutParams;
                Control.LayoutParameters = layoutParams;
                Control.Gravity = GravityFlags.Center;
                Control.SetPadding(0, 0, 0, 0);
                SetPadding(0, 0, 0, 0);
                SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }
    }
}
