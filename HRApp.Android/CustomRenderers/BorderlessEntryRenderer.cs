﻿using System;
using Android.Content;
using HRApp.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Editor), typeof(BorderlessEntryRenderer))]
namespace HRApp.Droid.CustomRenderers
{
    public class BorderlessEntryRenderer : EditorRenderer
    {
        public BorderlessEntryRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);
        }
        //protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        //{
        //    base.OnElementChanged(e);
        //    Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);
        //}
    }
}
