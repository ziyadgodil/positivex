﻿using Android.Content;
using Android.Graphics;
using Android.Support.Design.Internal;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using HRApp.Droid.CustomRenderers;
using HRApp.Views.NavBar;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

[assembly: ExportRenderer(typeof(BottomTabPage), typeof(CustomTabbedPageRenderer))]
namespace HRApp.Droid.CustomRenderers
{
    public class CustomTabbedPageRenderer : TabbedPageRenderer
    {
        Xamarin.Forms.TabbedPage tabbedPage;
        BottomNavigationView bottomNavigationView;
        Android.Views.IMenuItem lastItemSelected;
        int lastItemId = -1;

        public CustomTabbedPageRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TabbedPage> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                tabbedPage = e.NewElement as BottomTabPage;
                bottomNavigationView = (GetChildAt(0) as Android.Widget.RelativeLayout).GetChildAt(1) as BottomNavigationView;
                var height = bottomNavigationView.LayoutParameters.Height;
                //bottomNavigationView.NavigationItemSelected += BottomNavigationView_NavigationItemSelected;

                //Call to change the font
                ChangeFont();
            }

            if (e.OldElement != null)
            {
                //bottomNavigationView.NavigationItemSelected -= BottomNavigationView_NavigationItemSelected;
            }

        }

        //Change Tab font
        void ChangeFont()
        {
           // var fontFace = Typeface.CreateFromAsset(Context.Assets, "Roboto-Light.ttf");
            var bottomNavMenuView = bottomNavigationView.GetChildAt(0) as BottomNavigationMenuView;
            bottomNavMenuView.SetMinimumHeight(110);
            bottomNavigationView.SetPadding(0, 9, 0, 35);
            bottomNavigationView.LabelVisibilityMode = 1;
            //for (int i = 0; i < bottomNavMenuView.ChildCount; i++)
            //{
            //    var item = bottomNavMenuView.GetChildAt(i) as BottomNavigationItemView;
            //    item.SetIconSize(100);
            //    var itemTitle = item.GetChildAt(1);

            //    var smallTextView = ((TextView)((BaselineLayout)itemTitle).GetChildAt(0));
            //    var largeTextView = ((TextView)((BaselineLayout)itemTitle).GetChildAt(1));

            //    lastItemId = bottomNavMenuView.SelectedItemId;

            //    //smallTextView.SetTypeface(fontFace, TypefaceStyle.Bold);
            //    //largeTextView.SetTypeface(fontFace, TypefaceStyle.Bold);

            //    //Set text color
            //    var textColor = (item.Id == bottomNavMenuView.SelectedItemId) ? tabbedPage.On<Xamarin.Forms.PlatformConfiguration.Android>().GetBarSelectedItemColor().ToAndroid() : tabbedPage.On<Xamarin.Forms.PlatformConfiguration.Android>().GetBarItemColor().ToAndroid();
            //    smallTextView.SetTextColor(textColor);
            //    largeTextView.SetTextColor(textColor);
            //}
        }

        void BottomNavigationView_NavigationItemSelected(object sender, BottomNavigationView.NavigationItemSelectedEventArgs e)
        {
            var normalColor = tabbedPage.On<Xamarin.Forms.PlatformConfiguration.Android>().GetBarItemColor().ToAndroid();
            var selectedColor = tabbedPage.On<Xamarin.Forms.PlatformConfiguration.Android>().GetBarSelectedItemColor().ToAndroid();
            var bottomNavMenuView = bottomNavigationView.GetChildAt(0) as BottomNavigationMenuView;

            if (lastItemId != -1)
            {
                SetTabItemTextColor(bottomNavMenuView.GetChildAt(lastItemId) as BottomNavigationItemView, normalColor);
            }

            SetTabItemTextColor(bottomNavMenuView.GetChildAt(e.Item.ItemId) as BottomNavigationItemView, selectedColor);

            this.OnNavigationItemSelected(e.Item);
            lastItemId = e.Item.ItemId;

            //if (e.Item.ItemId == 4)
            //{
            //    bottomNavMenuView.Visibility = ViewStates.Gone;
            //}
            //else
            //{
            //    bottomNavMenuView.Visibility = ViewStates.Visible;
            //}
        }

        void SetTabItemTextColor(BottomNavigationItemView bottomNavigationItemView, Android.Graphics.Color textColor)
        {
            var itemTitle = bottomNavigationItemView.GetChildAt(1);
            var smallTextView = ((TextView)((BaselineLayout)itemTitle).GetChildAt(0));
            var largeTextView = ((TextView)((BaselineLayout)itemTitle).GetChildAt(1));

            smallTextView.SetTextColor(textColor);
            largeTextView.SetTextColor(textColor);
        }
    }
}
