﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserNotifications;
using Firebase.CloudMessaging;  
using Foundation;
using Naxam.Controls.Platform.iOS;
using Syncfusion.ListView.XForms.iOS;
using Syncfusion.XForms.iOS.EffectsView;
using UIKit;
using Xamarin.Forms;
using HRApp.Helpers.BaseData;

namespace HRApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public void DidRefreshRegistrationToken(Messaging messaging, string fcmToken)
        {
            System.Diagnostics.Debug.WriteLine($" FCM Token: { fcmToken } ");
            if (string.IsNullOrEmpty(App.FCMToken))
            {
                App.FCMToken = fcmToken;
            }
        }
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            UIApplication.SharedApplication.RegisterForRemoteNotifications();
            Firebase.Core.App.Configure();
            TopTabbedRenderer.Init();
            Rg.Plugins.Popup.Popup.Init();
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
            Syncfusion.SfChart.XForms.iOS.Renderers.SfChartRenderer.Init();
            SfListViewRenderer.Init();
            SfEffectsViewRenderer.Init();
            Xamarin.Forms.Forms.SetFlags("CarouselView_Experimental");
            Xamarin.Forms.Forms.SetFlags("CollectionView_Experimental");
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) => {
                    Console.WriteLine(granted);
                });

                UNUserNotificationCenter.Current.Delegate = new MyNotificationCenterDelegate();

                Messaging.SharedInstance.Delegate = this;
            }
            else
            {
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
                
            }
            return base.FinishedLaunching(app, options);
        }

        [Export("messaging:didReceiveMessage:")]
        public void DidReceiveMessage(Messaging messaging, RemoteMessage remoteMessage)
        {
            // Handle Data messages for iOS 10 and above.
            //HandleMessage(remoteMessage.AppData);
            //LogInformation(nameof(DidReceiveMessage), remoteMessage.AppData);
        }

        public override void OnActivated(UIApplication uiApplication)
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            base.OnActivated(uiApplication);
        }

        public class MyNotificationCenterDelegate : UNUserNotificationCenterDelegate
        {
            public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
            {
                completionHandler();
            }

            public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
            {
                completionHandler(UNNotificationPresentationOptions.Sound | UNNotificationPresentationOptions.Alert | UNNotificationPresentationOptions.Badge);
                MessagingCenter.Send(new MessagingCenterModel { }, "NotifyUpdate");
            }
        }


        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            try
            {
                if (userInfo != null)
                {
                    var userid = userInfo["gcm.notification.UserID"];
                    if (!string.IsNullOrEmpty(userid.ToString()) && !string.IsNullOrEmpty(Settings.UserToken))
                    {
                        MessagingCenter.Send(new MessagingCenterModel { }, "SendShout", userid.ToString());
                    }
                }

                completionHandler(UIBackgroundFetchResult.NewData);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
