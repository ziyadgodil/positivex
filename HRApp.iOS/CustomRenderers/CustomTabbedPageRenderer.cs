﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using HRApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

//[assembly: ExportRenderer(typeof(TabbedPage), typeof(CustomTabbedPageRenderer))]
namespace HRApp.iOS.CustomRenderers
{
    public class CustomTabbedPageRenderer : TabbedRenderer
    {
        private bool disposed;
        private int TabBarHeight = 110;

        public CustomTabbedPageRenderer()
        {
            TabBar.BarTintColor = UIColor.White;
        }

        public override void ViewWillLayoutSubviews()
        {
            base.ViewWillLayoutSubviews();
            var newHeight = TabBarHeight;
            CoreGraphics.CGRect tabFrame = TabBar.Frame; //self.TabBar is IBOutlet of your TabBar
            tabFrame.Height = newHeight;
            tabFrame.Y = View.Frame.Size.Height - newHeight;

            foreach (UIViewController vc in ViewControllers)
            {
                //Adjust the title's position   
                //vc.TabBarItem.TitlePositionAdjustment = new UIOffset(0, -36);
                //Adjust the icon's position
                vc.TabBarItem.ImageInsets = new UIEdgeInsets(4, 0, 0, 0);
            }

            TabBar.Frame = tabFrame;
        }


        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                //Tabbed.PropertyChanged += Tabbed_PropertyChanged;
            }
        }

        //private void Tabbed_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == HideableTabbedPage.IsHiddenProperty.PropertyName)
        //    {
        //        if ((this.Element as HideableTabbedPage).IsHidden)
        //        {
        //            TabBarHeight = 0;
        //            ViewWillLayoutSubviews();
        //        }
        //        else
        //        {
        //            TabBarHeight = 110;
        //            ViewWillLayoutSubviews();
        //        }
        //    }
        //}
    }
}
