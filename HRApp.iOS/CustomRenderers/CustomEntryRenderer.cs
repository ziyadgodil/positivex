﻿using System;
using HRApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(CustomEntryRenderer))]
namespace HRApp.iOS.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            //Control.BorderStyle = UITextBorderStyle.None;

            var view = (Entry)Element;

            if (view != null && Control != null)
            {
                //  SetDoneButton(view);
                Control.BorderStyle = UITextBorderStyle.None;
                if (view.HorizontalTextAlignment != TextAlignment.Center)
                {
                    var paddingView = new UIView(new CoreGraphics.CGRect(0, 0, 14, 0));
                    Control.LeftView = paddingView;
                    Control.LeftViewMode = UITextFieldViewMode.Always;
                }
            }
        }
    }
}
