﻿using System;
using CoreGraphics;
using HRApp.Helpers.Controls;
using HRApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TopTabbedPage), typeof(TopTabbedPageRenderer))]
namespace HRApp.iOS.CustomRenderers
{
    public class TopTabbedPageRenderer : PageRenderer
    {
        public override void ViewWillLayoutSubviews()
        {
            base.ViewWillLayoutSubviews();

            nfloat tabSize = 44.0f;

            UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;

            if (UIInterfaceOrientation.LandscapeLeft == orientation || UIInterfaceOrientation.LandscapeRight == orientation)
            {
                tabSize = 32.0f;
            }

            CGRect rect = this.View.Frame;
            rect.Y = this.NavigationController != null ? tabSize : tabSize + 20;
            this.View.Frame = rect;

            if (TabBarController != null)
            {
                CGRect tabFrame = this.TabBarController.TabBar.Frame;
                tabFrame.Height = tabSize;
                tabFrame.Y = this.NavigationController != null ? 64 : 20;
                this.TabBarController.TabBar.Frame = tabFrame;
                this.TabBarController.TabBar.BarTintColor = UIColor.Red;
            }
        }
    }
}
